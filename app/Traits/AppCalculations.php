<?php

namespace App\Traits;

use App\Models\Bonus;
use App\Models\Deposit;
use App\Models\Referral;
use App\Models\User;

trait AppCalculations
{
    protected function checkDepositMaturity(Deposit $deposit)
    {
        $planDuration = $deposit->plan->duration;
        $depositApprovedOn = $deposit->approved_at;

        $depositMaturityDate = $depositApprovedOn->addDays($planDuration);


        return ($deposit->status === \App\Models\Deposit::STATUS_APPROVED &&
            $deposit->approved_at !== null &&
            !$depositMaturityDate->gte(now())
        );
    }

    protected function calculateRealtimeEarning(User $user)
    {

        $earnedProfit = 0;
        $user->deposits()
            ->approved()
            ->notWithdrawn()
            ->with(['plan'])
            ->get()
            ->each(function ($deposit) use (&$earnedProfit) {
                if (!$this->checkDepositMaturity($deposit)) {
                    $currentLifeSpan = $deposit->approved_at->diffInDays(now());
                    for ($i = 0; $i < $currentLifeSpan; $i++){
                        $earnedProfit += ($deposit->amount * $deposit->plan->percentage) / 100;
                    }
                } else {
                    $earnedProfit += ($deposit->amount * $deposit->plan->percentage * $deposit->plan->duration) / 100;
                }
            });
        return $earnedProfit;
    }

    public function getActiveBonuses(User $user)
    {
        $totalBonuses = 0;

        $user->bonuses()->notWithdrawn()->get()
            ->each(function ($bonus) use (&$totalBonuses) {
                if (!$bonus->getBonusExpiryStatus() && $bonus->withdrawn === Bonus::NOT_WITHDRAWN)
                    $totalBonuses += $bonus->amount;
            });

        return $totalBonuses;
    }

    protected function getUserDepositBalance(User $user)
    {
        $sumTotal = 0;

        $user->deposits()
            ->approved()
            ->notWithdrawn()
            ->get()
            ->each(function ($deposit) use (&$sumTotal) {
                if (!is_null($deposit->approved_at) && $deposit->status === Deposit::STATUS_APPROVED)
                    $sumTotal += $deposit->amount;
            });

        return $sumTotal;
    }

    protected function getUserReferrals(User $user)
    {
        $referees = [];
        $refCommission = 0;

        $user->referrals()->bonusAvailable()->get()
            ->each(function ($ref) use (&$referees, &$refCommission) {
                $referred = User::with(['deposits'])->where('id', $ref->referee_id)->first();
                if ($referred) {
                    array_push($referees, $referred);
                    if ($referred->deposits->count() > 0) {
                        $approvedRefUserDeposit = $referred->deposits->first(function ($dep) {
                            return $dep->status === \App\Models\Deposit::STATUS_APPROVED;
                        });
                        if ($approvedRefUserDeposit)
                            $refCommission += $approvedRefUserDeposit->amount * (Referral::REFERRAL_COMMISSION_PERCENTAGE / 100);
                    }
                }
            });

        return [collect($referees), $refCommission];
    }
}
