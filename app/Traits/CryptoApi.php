<?php

namespace App\Traits;

use Illuminate\Support\Facades\Http;

trait CryptoApi {
    public function getCurrencyValue($currency = 'BTC', $value = 0)
    {
        $endPoint = config('app.currency_convert_url');
        $amount = 0;

        try {
            $response = Http::get($endPoint . '?currency=' . $currency);
            if ($response->successful()) {
                $data = $response->json();
                $valueInUSD = $data['data']['rates']['USDC'] ?? 1;
                $amount = round(floatval($value / $valueInUSD), 4);
            }
            return $amount;
        } catch (\Throwable $exception) {
            return $amount;
        }
    }
}
