<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Referral extends Model
{
    use HasFactory, SoftDeletes;
    const BONUS_TAKEN = 1;
    const BONUS_NOT_TAKEN = 0;
    const REFERRAL_COMMISSION_PERCENTAGE = 5;

    protected $guarded = ['id'];

    public function scopeBonusAvailable($query)
    {
        return $query->where('bonus_taken', self::BONUS_NOT_TAKEN);
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function referee()
    {
        return $this->hasOne(User::class, 'referee_id');
    }

    public function isWithdrawable()
    {
        $yes = false;

        $referredUser = User::with(['deposits'])->where('id', $this->getAttributes(referee_id))->first();
        if ($referredUser) {
            $referredApprovedDeposit = $referredUser->deposits->first(function ($deposit) {
                return $deposit->isApproved() && $deposit->withdrawn === \App\Models\Deposit::NOT_WITHDRAWN;
            });

            if ($referredApprovedDeposit)
                $yes = true;
        }

        return $yes;
    }
}
