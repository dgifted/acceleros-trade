<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Str;
use Laravel\Sanctum\HasApiTokens;
use phpDocumentor\Reflection\Types\Self_;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable, SoftDeletes;

    const USER_ADMIN = 10;
    const USER_REGULAR = 1;

    const STATUS_ACTIVE = 1;
    const STATUS_INACTIVE = 0;

    const VERIFIED = 1;
    const UNVERIFIED = 0;

    protected $appends = ['first_name', 'avatar_path'];
    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'username',
        'referrer_id',
        'phone',
        'is_active',
        'verified',
        'balance',
        'uid',
        'avatar',
        'secret_question',
        'secret_answer',
        'ref',
        'verification_token'
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    protected static function boot()
    {
        parent::boot();

        static::deleting(function ($user) {
            $user->deposits()->get()
                ->each(function ($deposit) {
                    $deposit->delete();
                });

            $user->wallets()->get()
                ->each(function ($wallet) {
                    $wallet->delete();
                });

            $user->withdrawals()->get()
                ->each(function ($withdrawal) {
                    $withdrawal->delete();
                });

            $user->referrals()->get()
                ->each(function ($ref) {
                    $ref->delete();
                });
        });
    }

    public function getAvatarPathAttribute()
    {
        return !!$this->getAttribute('avatar')
            ? asset('avatars/' . $this->getAttribute('avatar'))
            : asset('dist/img/user1.png');
    }

    public function bonuses()
    {
        return $this->hasMany(Bonus::class, 'user_id');
    }

    public function deposits()
    {
        return $this->hasMany(Deposit::class, 'user_id');
    }

    public static function generateRef()
    {
        return Str::random('32');
    }

    public static function generateUid()
    {
        return uniqid(time(), true);
    }

    public static function generateVerificationToken()
    {
        return Str::random(64);
    }

    public function getFirstNameAttribute()
    {
        return explode(' ', $this->getAttribute('name'))[0] ?? '';
    }

    public function isActive()
    {
        return $this->getAttribute('is_active') === self::STATUS_ACTIVE;
    }

    public function scopeIsActive($query)
    {
        return $query->where([
            'is_active' => self::STATUS_ACTIVE,
            'verified' => self::VERIFIED
        ]);
    }

    public function isAdmin()
    {
        return $this->getAttribute('admin') === User::USER_ADMIN;
    }

    public function isVerified()
    {
        return $this->getAttribute('verified') === self::VERIFIED;
    }

    public function scopeFindByUid($query, $uid)
    {
        return $query->where('uid', $uid);
    }

    public function scopeNotAdmin($query)
    {
        return $query->where('admin', '!=', self::USER_ADMIN);
    }

    public function scopeAdmin($query)
    {
        return $query->where('admin', self::USER_ADMIN);
    }

    public function scopeVerified($query)
    {
        return $query->where('verified', self::VERIFIED);
    }

    public function referrals()
    {
        return $this->hasMany(Referral::class, 'user_id');
    }

    public function wallets()
    {
        return $this->hasMany(CustomerWallet::class, 'user_id');
    }

    public function withdrawals()
    {
        return $this->hasMany(Withdrawal::class, 'user_id');
    }
}
