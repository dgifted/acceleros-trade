<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Str;

class PaymentChannel extends Model
{
    use HasFactory, SoftDeletes;

    const SUPPORTED = 1;
    const UNSUPPORTED = 0;

    const IS_DEFAULT = 1;
    const NON_DEFAULT = 0;

    protected $appends = ['icon'];
    protected $guarded = ['id'];

    public function customerWallets()
    {
        return $this->hasMany(CustomerWallet::class, 'channel_id');
    }

    public function deposits()
    {
        return $this->hasMany(Deposit::class, 'channel_id');
    }

    public function getIconAttribute()
    {
        switch ($this->getAttribute('id')) {
            case 1:
                return asset('assets/images/18.gif');
            case 2:
                return asset('assets/images/48.gif');
            case 3:
                return asset('assets/images/68.gif');
            case 4:
                return asset('assets/images/69.gif');
            case 5:
                return asset('assets/images/77.gif');
            case 6:
                return asset('assets/images/79.gif');
            case 7:
                return asset('assets/images/82.gif');
            case 8:
                return asset('assets/images/92.gif');
            case 9:
                return asset('assets/images/94.gif');
        }
    }

    public static function generateRefId()
    {
        return Str::random(32);
    }

    public function isSupported()
    {
        return $this->getAttribute('supported') === PaymentChannel::SUPPORTED;
    }

    public function scopeFindByRef($query, $ref)
    {
        return $query->where('ref_id', $ref);
    }

    public function scopeSupported($query)
    {
        return $query->where('supported', self::SUPPORTED);
    }
}
