<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class GenericEmail extends Model
{
    use HasFactory;

    protected $appends = ['excerpt'];
    protected $guarded = ['id'];

    public function getExcerptAttribute()
    {
        return strip_tags(substr($this->getAttribute('content'), 0, 300)) . '...';
    }

    public static function generateReferenceId($len = 32)
    {
        return Str::random($len);
    }

    public function recipient()
    {
        return $this->belongsTo(User::class, 'recipient_id');
    }

    public function scopeFindByRefId($query, $refId)
    {
        return $query->where('ref_id', $refId);
    }
}
