<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Str;

class Withdrawal extends Model
{
    use HasFactory, SoftDeletes;

    const STATUS_PENDING = 0;
    const STATUS_APPROVED = 1;
    const STATUS_CANCELLED = 2;
    const STATUS_FAILED = 3;

    const CAN_WITHDRAW = 'xxxx-xxxx';
    const CANNOT_WITHDRAW = 'xxx-xxx';
    const FOR_BONUS = 'bonus';

    protected $guarded = ['id'];

    public function customer()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function deposit()
    {
        return $this->belongsTo(Deposit::class, 'deposit_id');
    }

    public static function generateRefId()
    {
        return Str::random(32);
    }
    public function scopeApproved($query)
    {
        return $query->where('status', Withdrawal::STATUS_APPROVED);
    }

    public function scopeFindByRef($query, $ref)
    {
        return $query->where('ref_id', $ref);
    }

    public function userWallet()
    {
        return $this->belongsTo(CustomerWallet::class, 'user_wallet_id');
    }
}
