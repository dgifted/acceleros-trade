<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Bonus extends Model
{
    use HasFactory;

    const WITHDRAWN = 1;
    const NOT_WITHDRAWN = 0;

    protected $appends = ['expiry_date'];

    protected $guarded = ['id'];

    public function customer()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public static function generateReferenceId($length = 32)
    {
        return Str::random($length);
    }

    public function getExpiryDateAttribute()
    {
        if ($this->getAttribute('expire_days') !== 0) {
            return $this->getAttribute('created_at')
                ->addDays($this->getAttribute('expire_days'));
        }

        return 0;
    }

    public function getBonusExpiryStatus()
    {
        $expired = false;

        if ($this->getAttribute('expire_days') !== 0) {
            $expired = $this->getAttribute('created_at')
                ->addDays($this->getAttribute('expire_days'))->lt(now());
        }

        return $expired;
    }

    public function isWithdrawn()
    {
        return $this->getAttribute('withdrawn') === self::WITHDRAWN;
    }

    public function scopeFindByRefId($query, $refId)
    {
        return $query->where('ref_id', $refId);
    }

    public function scopeNotWithdrawn($query)
    {
        return $query->where('withdrawn', self::NOT_WITHDRAWN);
    }

    public function scopeWithdrawn($query)
    {
        return $query->where('withdrawn', self::WITHDRAWN);
    }
}
