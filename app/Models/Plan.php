<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Str;

class Plan extends Model
{
    use HasFactory, SoftDeletes;

    protected $guarded = ['id'];

    public function deposits()
    {
        return $this->hasMany(Deposit::class, 'plan_id');
    }

    public static function generateRefId()
    {
        return Str::random(32);
    }

    public function scopeFindByRef($query, $ref)
    {
        return $query->where('ref_id', $ref);
    }
}
