<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Str;

class Deposit extends Model
{
    use HasFactory, SoftDeletes;

    const STATUS_PENDING = 0;
    const STATUS_APPROVED = 1;
    const STATUS_CANCELLED = 2;
    const STATUS_FAILED = 3;

    const WITHDRAWN = 1;
    const NOT_WITHDRAWN = 0;

    protected $casts = [
        'approved_at' => 'datetime'
    ];
    protected $guarded = ['id'];

    protected static function boot()
    {
        parent::boot();

        static::deleting(function ($deposit) {
            if ($deposit->withdrawal)
                $deposit->withdrawal->delete();
        });
    }

    public function channel()
    {
        return $this->belongsTo(PaymentChannel::class, 'channel_id');
    }

    public function customer()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public static function generateReferenceId()
    {
        return Str::random(32);
    }

    public function isApproved()
    {
        return $this->getAttribute('status') === Deposit::STATUS_APPROVED &&
            $this->getAttribute('approved_at') !== null;
    }

    public function plan()
    {
        return $this->belongsTo(Plan::class, 'plan_id');
    }

    public function scopeApproved($query)
    {
        return $query->where('status', Deposit::STATUS_APPROVED)
            ->orWhere('approved_at', '!=', null);
    }

    public function scopeFindByRef($query, $ref)
    {
        return $query->where('ref_id', $ref);
    }

    public function scopeWithdrawn($query)
    {
        return $query->where('withdrawn', self::WITHDRAWN);
    }

    public function scopeNotWithdrawn($query)
    {
        return $query->where('withdrawn', self::NOT_WITHDRAWN);
    }

    public function withdrawal()
    {
        return $this->hasOne(Withdrawal::class, 'deposit_id');
    }
}
