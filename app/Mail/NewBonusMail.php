<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class NewBonusMail extends Mailable
{
    use Queueable, SerializesModels;

    public $bonus;

    /**
     * Create a new message instance.
     *
     * @param $bonus
     */
    public function __construct($bonus)
    {
        $this->bonus = $bonus;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('New bonus')
            ->view('emails.new-bonus');
    }
}
