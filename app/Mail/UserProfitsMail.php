<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class UserProfitsMail extends Mailable
{
    use Queueable, SerializesModels;
    public $profits, $user;

    /**
     * Create a new message instance.
     *
     * @param $profits
     * @param $user
     */
    public function __construct($profits, $user)
    {
        $this->profits = $profits;
        $this->user = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Daily profits updates')
        ->view('emails.profits-updates');
    }
}
