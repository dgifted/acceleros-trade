<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class NewReferralMail extends Mailable
{
    use Queueable, SerializesModels;

    public $referee, $referred, $commission;

    /**
     * Create a new message instance.
     *
     * @param $referee
     * @param $referred
     * @param $commission
     */
    public function __construct($referee, $referred, $commission = 20)
    {
        $this->referee = $referee;
        $this->referred = $referred;
        $this->commission = $commission;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('')
            ->view('emails.new-referred-user');
    }
}
