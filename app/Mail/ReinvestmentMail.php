<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ReinvestmentMail extends Mailable
{
    use Queueable, SerializesModels;

    public $deposit, $user;

    /**
     * Create a new message instance.
     *
     * @param $deposit
     * @param $user
     */
    public function __construct($deposit, $user)
    {
        $this->deposit = $deposit;
        $this->user = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Reinvestment transaction')
        ->view('emails.re-deposit');
    }
}
