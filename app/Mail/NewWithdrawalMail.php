<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class NewWithdrawalMail extends Mailable
{
    use Queueable, SerializesModels;

    public $user, $withdrawal;

    /**
     * Create a new message instance.
     *
     * @param $user
     * @param $withdrawal
     */
    public function __construct($user, $withdrawal)
    {
        $this->user = $user;
        $this->withdrawal = $withdrawal;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Withdrawal request.')
            ->view('emails.new-withdrawal');
    }
}
