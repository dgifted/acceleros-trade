<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class GenericEmailMail extends Mailable
{
    use Queueable, SerializesModels;

    public
        $content,
        $subject,
        $recipient;

    /**
     * Create a new message instance.
     *
     * @param $content
     * @param $subject
     * @param $recipient
     */
    public function __construct($content, $subject, $recipient)
    {
        $this->content = $content;
        $this->recipient = $recipient;
        $this->subject = $subject;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this
            ->subject($this->subject)
            ->view('emails.generic-email')->with([
                'content' => $this->content,
                'recipient' => $this->recipient,
                'subject' => $this->subject,
            ]);
    }
}
