<?php

namespace App\Http\Controllers\Customer;

use App\Http\Controllers\Controller;
use App\Mail\ReinvestmentMail;
use App\Models\Deposit;
use App\Models\PaymentChannel;
use App\Models\Plan;
use App\Models\User;
use App\Traits\AppCalculations;
use App\Traits\CryptoApi;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

class ReinvestmentController extends Controller
{
    use AppCalculations, CryptoApi;

    public function showReinvestmentPage()
    {
        $user = auth()->user();
        $reqProfits = \request('profits') ?? 0;
        $profits = $this->getUserDepositBalance($user) + $this->calculateRealtimeEarning($user) + $user->balance;
        if ($reqProfits == 0 || $reqProfits > $profits)
            return redirect()->route('withdrawal.create')->with([
                'message' => 'Amount cannot exceed ' . $profits . '. Please correct the amount and try again.',
                'type' => 'warning'
            ]);

        $plans = Plan::all()->filter(function ($plan) use ($reqProfits) {
            return $plan->min <= $reqProfits;
        });
        $channels = PaymentChannel::supported()->get(['id', 'name']);

        return view('customer.re-deposit')->with([
            'amount' => $reqProfits,
            'channels' => $channels,
            'plans' => $plans,
            'profits' => $profits
        ]);
    }

    public function reInvest(Request $request)
    {
        $request->validate([
            'amount' => ['required', 'numeric'],
            'plan' => ['required'],
            'channel' => ['required',],
        ]);
        $amount = $request->get('amount');

        $user = auth()->user();
        $earnedProfit = $this->calculateRealtimeEarning($user) + $this->getUserDepositBalance($user);
        $deposits = $user->deposits()
            ->approved()
            ->notWithdrawn()
            ->get();
        $totalPendingWithdrawals = 0;
        $maturedDeposits = $deposits->filter(function ($deposit) {
            return $this->checkDepositMaturity($deposit);
        })->each(function ($deposit) use (&$totalPendingWithdrawals) {
            $totalPendingWithdrawals += $deposit->amount;
        });
        $totalPendingWithdrawals += $earnedProfit + $user->balance;

        if ($amount > $totalPendingWithdrawals) {
            return redirect()->route('withdrawal.create')->with([
                'type' => 'danger',
                'message' => 'Entered amount cannot exceed ' . $totalPendingWithdrawals . '.'
            ]);
        }
        $success = false;
        DB::transaction(function () use (&$success, &$maturedDeposits, &$user, $amount, $totalPendingWithdrawals, $request) {
            $maturedDeposits->each(function ($deposit) {
                $deposit->withdrawn = Deposit::WITHDRAWN;
                $deposit->withdrawn_at = now();
                $deposit->save();
            });

            $user->balance = ($totalPendingWithdrawals - $amount);
            $user->save();

            $channel = PaymentChannel::where('id', $request->get('channel'))->first();
            $deposit = $user->deposits()->create([
                'ref_id' => Deposit::generateReferenceId(),
                'channel_id' => $request->get('channel'),
                'plan_id' => $request->get('plan'),
                'currency_equivalent' => $this->getCurrencyValue($request->get($channel->abbreviation), $amount),
                'amount' => $amount
            ]);

            if (!config('mail.paused'))
                Mail::to($user)->send(new ReinvestmentMail($deposit, $user));
            $success = true;
        });

        if (!$success)
            return redirect()->route('withdrawal.create')->with([
                'type' => 'danger',
                'message' => 'An unexpected error occurred. Please try again later.'
            ]);

        return redirect()->route('withdrawal.create')->with([
            'type' => 'success',
            'message' => 'Operation successful.'
        ]);
    }
}
