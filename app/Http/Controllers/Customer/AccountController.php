<?php

namespace App\Http\Controllers\Customer;

use App\Http\Controllers\Controller;
use App\Models\PaymentChannel;
use App\Models\User;
use App\Traits\AppCalculations;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;

class AccountController extends Controller
{
    use AppCalculations;

    public function showAccountUpdatePAge()
    {
        $user = auth()->user();
        $wallets = $user->wallets()->with(['paymentChannel'])->get();

        $channels = PaymentChannel::supported()->get()
            ->each(function ($channel) use ($wallets) {
                foreach ($wallets as $wallet) {
                    if ($wallet->paymentChannel->id === $channel->id)
                        $channel->customerWallet = $wallet->address;
                }
            });

        return view('customer.account')->with([
            'user' => $user,
            'channels' => $channels
        ]);
    }

    public function updateAccount(Request $request)
    {
        $user = auth()->user();

        if ($request->has('name') && !is_null($request->get('name'))) {
            $user->name = $request->get('name');
            $user->save();
        }

        $userWallets = $user->wallets()->get();

        PaymentChannel::all()
            ->each(function ($channel) use ($request, $user, $userWallets) {
                if ($request->has($channel->slug) && !is_null($request->get($channel->slug))) {

                    $userChannelWallet = $userWallets->first(function ($wall) use ($channel) {
                        return $wall->channel_id === $channel->id;
                    });

                    if (!!$userChannelWallet) {
                        $wallet = $userChannelWallet;
                        $wallet->address = $request->get($channel->slug);
                        $wallet->save();
                    } else {
                        $user->wallets()->create([
                            'channel_id' => $channel->id,
                            'address' => $request->get($channel->slug)
                        ]);
                    }
                }
            });

        return back()->with([
            'message' => 'Account data updated successfully'
        ]);
    }

    public function showAccountHistoryPage()
    {
        $a_type = request()->has('a_type') && !is_null(\request('a_type')) ? \request('a_type') : 'all';
        $user = auth()->user();

        $days = [];
        for ($i = 1; $i <= 31; $i++) {
            array_push($days, $i);
        }

        $months = [
            1 => 'Jan', 2 => 'Feb', 3 => 'Mar', 4 => 'Apr', 5 => 'May', 6 => 'Jun', 7 => 'Jul', 8 => 'Aug',
            9 => 'Sep', 10 => 'Oct', 11 => 'Nov', 12 => 'Dec'
        ];

        $types = [
            'all' => 'All transactions',
            'deposit' => 'Deposits',
            'withdrawal' => 'Withdrawal',
            'earning' => 'Earning',
            'commissions' => 'Referral commission'
        ];

        return view('customer.account-history')->with([
            'aType' => $a_type,
            'days' => $days,
            'months' => $months,
            'paymentChannels' => PaymentChannel::all(['id', 'name']),
            'today' => now(),
            'types' => $types,
            'user' => $user
        ]);
    }

    public function filterAccountTransactions(Request $request)
    {
        $user = auth()->user();
        $data = $request->validate([
            'type' => ['required'],
            'month_from' => ['required'],
            'day_from' => ['required'],
            'year_from' => ['required'],
            'month_to' => ['required'],
            'day_to' => ['required'],
            'year_to' => ['required'],
        ]);
        $referralBonus = 5 / 100;
        $startDate = Carbon::createFromDate($data['year_from'], $data['month_from'], $data['day_from']);
        $endDate = Carbon::createFromDate($data['year_to'], $data['month_to'], $data['day_to']);

        switch ($data['type']) {
            case 'all':
                return back()->with([
                    'payload' => $this->getAllTransactions($startDate, $endDate)
                ]);
            case 'deposit':
                return back()->with([
                    'payload' => $user->deposits()
                        ->approved()
                        ->whereBetween('created_at', [$startDate, $endDate])
                        ->get()
                        ->map(function ($deposit) {
                            return (object)[
                                'type' => 'Deposit',
                                'amount' => $deposit->amount,
                                'date' => $deposit->created_at
                            ];
                        })
                ]);
            case 'withdrawal':
                return back()->with([
                    'payload' => $user->withdrawals()
                        ->approved()
                        ->whereBetween('created_at', [$startDate, $endDate])
                        ->get()
                        ->map(function ($withdrawal) {
                            return (object)[
                                'type' => 'Withdrawal',
                                'amount' => $withdrawal->amount,
                                'date' => $withdrawal->created_at
                            ];
                        })
                ]);
            case 'earning':
                $payload = [
                    (object)[
                        'type' => 'Earnings',
                        'amount' => $this->calculateRealtimeEarning($user),
                        'date' => now()
                    ]
                ];
                return back()->with([
                    'payload' => collect($payload)
                ]);
            case 'commissions':
                return back()->with([
                    'payload' => $this->mapReferralCommissions(
                        $referralBonus, $user->referrals()
                        ->whereBetween('created_at', [$startDate, $endDate])
                        ->get()
                    )
                ]);
        }
    }

    private function getAllTransactions($startDate, $endDate)
    {
        $user = auth()->user();
        $deposits = [];
        $withdrawals = [];
        $earnings = [];
        $referralCommissions = [];
        $referralBonus = 5 / 100;

        $user->referrals()->whereBetween('created_at', [$startDate, $endDate])->get()
            ->each(function ($ref) use (&$referralCommissions, $referralBonus) {
                $refUser = User::with('deposits')->where('id', $ref->referee_id)->first();
                if ($refUser && $refUser->deposits->count() > 0) {
                    array_push($referralCommissions, (object)[
                        'type' => 'Referral commission',
                        'amount' => $refUser->deposits->first()->amount * $referralBonus,
                        'date' => $refUser->deposits->first()->created_at
                    ]);
                }
            });

        $user->deposits()
            ->approved()
            ->whereBetween('created_at', [$startDate, $endDate])
            ->get()
            ->each(function ($deposit) use (&$deposits) {
                array_push($deposits, (object)[
                    'type' => 'Deposit',
                    'amount' => $deposit->amount,
                    'date' => $deposit->created_at,
                ]);
            });

        $user->withdrawals()
            ->approved()
            ->get()
            ->whereBetween('created_at', [$startDate, $endDate])
            ->each(function ($withdrawal) use (&$withdrawals) {
                array_push($withdrawals, (object)[
                    'type' => 'Withdrawal',
                    'amount' => $withdrawal->amount,
                    'date' => $withdrawal->created_at,
                ]);
            });
        array_push($earnings, (object)[
            'type' => 'Earnings',
            'amount' => $this->calculateRealtimeEarning($user),
            'date' => now()
        ]);

        $payload = array_merge($deposits, $withdrawals, $earnings, $referralCommissions);
        return collect($payload);
    }

    private function mapReferralCommissions($referralBonus, $commissions = [])
    {
        $localCommissions = [];

        $commissions->each(function ($ref) use (&$localCommissions, $referralBonus) {
            $refUser = User::with('deposits')->where('id', $ref->referee_id)->first();
            if ($refUser && $refUser->deposits->count() > 0) {
                array_push($localCommissions, (object)[
                    'type' => 'Referral commission',
                    'amount' => $refUser->deposits->first()->amount * $referralBonus,
                    'date' => $refUser->deposits->first()->created_at
                ]);
            }
        });

        return collect($localCommissions);
    }
}
