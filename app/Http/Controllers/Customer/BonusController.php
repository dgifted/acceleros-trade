<?php

namespace App\Http\Controllers\Customer;

use App\Http\Controllers\Controller;
use App\Mail\NewDepositMail;
use App\Models\Bonus;
use App\Models\Deposit;
use App\Models\PaymentChannel;
use App\Models\Plan;
use App\Models\Withdrawal;
use App\Traits\CryptoApi;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

class BonusController extends Controller
{
    use CryptoApi;

    public function showCustomerBonusesPage()
    {
        $user = auth()->user();

        $plans = Plan::all();
        $bonuses = $user->bonuses()->latest()->get()
            ->each(function ($bonus) use ($plans) {
                $eligiblePlans = [];

                $plans->each(function ($plan) use ($bonus, &$eligiblePlans) {
                    if ($bonus->amount >= $plan->min)
                        array_push($eligiblePlans, $plan);
                });
                $eligiblePlans = collect($eligiblePlans);
                $bonus->eligiblePlans = $eligiblePlans;
            });

        return view('customer.bonuses')->with([
            'bonuses' => $bonuses
        ]);
    }

    public function showBonusReinvestPage($refId)
    {
        $bonus = Bonus::findByRefId($refId)->firstOrFail();

        $channels = PaymentChannel::supported()->get(['id', 'name']);

        $plans = Plan::all()->filter(function ($plan) use ($bonus) {
            return $bonus->amount >= $plan->min;
        })->each(function ($plan) use ($bonus) {
            $plan->capital = $bonus->amount;
        });

        return view('customer.deposit-bonus')->with([
            'bonus' => $bonus,
            'channels' => $channels,
            'plans' => $plans
        ]);
    }

    public function showBonusWithdrawalPage($refId)
    {
        $bonus = Bonus::findByRefId($refId)->firstOrFail();

        $user = auth()->user();

        $userWallets = $user->wallets()->with(['paymentChannel'])->get();
        $channels = PaymentChannel::with(['customerWallets.customer', 'deposits'])
            ->where('supported', \App\Models\PaymentChannel::SUPPORTED)
            ->get()
            ->each(function ($channel) use ($bonus, $userWallets) {
                $channel->userWallet = null;
                $channel->availableFund = 0;
                $channel->pendingFund = 0;

                foreach ($userWallets as $wallet) {
                    if ($wallet->channel_id === $channel->id) {
                        $channel->userWallet = $wallet->address;
                        $channel->availableFund = $bonus->amount;
                        $channel->pendingFund = $bonus->amount;
                    }
                }
            });

        return view('customer.withdraw-bonus')->with([
            'bonus' => $bonus,
            'channels' => $channels,
            'userWallets' => $userWallets
        ]);
    }

    public function reInvestBonus(Request $request, $refId)
    {
        $data = $request->validate([
            'channel' => ['required'],
            'plan' => ['required']
        ]);
        unset($request);

        $user = auth()->user();
        $bonus = Bonus::findByRefId($refId)->firstOrFail();

        if ($bonus->isWithdrawn())
            return back()->with([
                'message' => 'Bonus already used.',
                'type' => 'warning'
            ]);

        if ($bonus->getBonusExpiryStatus())
            return back()->with([
                'message' => 'Bonus has expired.',
                'type' => 'warning'
            ]);

        DB::beginTransaction();
        try {
            $channel = PaymentChannel::findOrFail($data['channel']);
            $deposit = $user->deposits()->create([
                'ref_id' => Bonus::generateReferenceId(),
                'channel_id' => $data['channel'],
                'plan_id' => $data['plan'],
                'amount' => $bonus->amount,
                'currency_equivalent' => $this->getCurrencyValue($channel->abbreviation, $bonus->amount)
            ]);

            $bonus->withdrawn = Bonus::WITHDRAWN;
            $bonus->save();

            if (!config('mail.paused'))
                Mail::to($user)->send(new NewDepositMail($deposit, $user));
            $success = true;
            DB::commit();
        } catch (\Throwable $exception) {
            $success = false;
            DB::rollBack();
        }

        if (!$success)
            return back()->with([
                'message' => 'Bonus reinvestment failed. Please try again',
                'type' => 'danger'
            ]);

        return back()->with([
            'message' => 'Bonus reinvested successfully.',
            'type' => 'success'
        ]);
    }

    public function withdrawBonus(Request $request, $refId)
    {
        $request->validate([
            'wallet' => ['required'],
        ]);

        $bonus = Bonus::findByRefId($refId)->firstOrFail();

        if ($bonus->isWithdrawn())
            return back()->with([
                'message' => 'Sorry bonus has already been used.',
                'type' => 'warning'
            ]);

        if ($bonus->getBonusExpiryStatus())
            return back()->with([
                'message' => 'Sorry bonus has expired.',
                'type' => 'warning'
            ]);

        $success = false;
        $withdrawal = null;

        DB::transaction(function () use (&$bonus, &$withdrawal, $request, &$success) {
            $bonus->withdrawn = Bonus::WITHDRAWN;
            $bonus->save();

            $withdrawal = auth()->user()->withdrawals()->create([
                'user_wallet_id' => $request->get('wallet'),
                'ref_id' => Withdrawal::generateRefId(),
                'amount' => $bonus->amount,
            ]);

            $success = true;
        });

        if (!$success)
            return back()->with([
                'message' => 'Operation failed. Please try again',
                'type' => 'danger'
            ]);

        return back()->with([
            'message' => 'Bonus withdrawn.',
            'type' => 'success'
        ]);

    }

}
