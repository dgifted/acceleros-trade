<?php

namespace App\Http\Controllers\Customer;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Traits\AppCalculations;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    use AppCalculations;

    public function index()
    {
        $user = auth()->user();
        $totalDeposit = 0;
        $totalWithdrawal = 0;
        $earnedProfits = $this->calculateRealtimeEarning($user);


        $user->deposits()
            ->approved()
            ->notWithdrawn()
            ->latest()
            ->get()
            ->each(function ($deposit) use (&$totalDeposit) {
                $totalDeposit += $deposit->amount;
            });

        $lastDeposit = $user->deposits()->approved()->get()->last();
        $user->withdrawals()
            ->approved()
            ->latest()
            ->get()
            ->each(function ($withdrawal) use (&$totalWithdrawal) {
                $totalWithdrawal += $withdrawal->amount;
            });

        $lastWithdraw = $user->withdrawals()->approved()->get()->last();
        $depositBalance = $this->getUserDepositBalance($user);

        return view('customer.index')->with([
            'balance' => $depositBalance + $earnedProfits + $user->balance + $this->getUserReferrals($user)[1],
            'lastDeposit' => $lastDeposit,
            'lastWithdrawal' => $lastWithdraw,
            'profits' => $earnedProfits,
            'totalDeposit' => $totalDeposit,
            'totalWithdrawal' => $totalWithdrawal,
            'user' => $user
        ]);
    }
}
