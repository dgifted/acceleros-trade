<?php

namespace App\Http\Controllers\Customer;

use App\Http\Controllers\Controller;
use App\Mail\NewDepositMail;
use App\Models\Deposit;
use App\Models\PaymentChannel;
use App\Models\Plan;
use App\Traits\CryptoApi;
use App\Traits\AppCalculations;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

class DepositController extends Controller
{
    use CryptoApi, AppCalculations;

    public function showAllDepositsPage()
    {
        $user = auth()->user();
        $depositBalance = $this->getUserDepositBalance($user);
        $userDeposits = $user->deposits()->approved()->notWithdrawn()->get();

        $plansWithDeposits = Plan::with(['deposits'])->get()
            ->each(function ($plan) use ($user, $userDeposits) {
                foreach ($plan->deposits as $deposit) {
                    foreach ($userDeposits as $userDeposit) {
                        if ($deposit->id === $userDeposit->id) {
                            if ($userDeposit->withdrawn === \App\Models\Deposit::WITHDRAWN) {
                                $plan->subscribedTo = 1;
                                $plan->depositedAmount = $userDeposit->amount;
                                $plan->withdrawan = true;
                            } else {
                                $plan->subscribedTo = 1;
                                $plan->depositedAmount = $userDeposit->amount;
                                $plan->withdrawan = false;
                            }
                        } else {
                            $plan->subscribedTo = 0;
                            $plan->depositedAmount = $userDeposit->amount;
                            $plan->withdrawan = false;
                        }
                    }
                }
            });

        return view('customer.deposit-list')->with([
            'balance' => $depositBalance,
            'plansWithDeposit' => $plansWithDeposits,
        ]);
    }

    public function showNewDepositPage()
    {
        $plans = Plan::all();
        $channels = PaymentChannel::supported()->get(['id', 'name']);

        return view('customer.deposit')->with([
            'plans' => $plans,
            'channels' => $channels
        ]);
    }

    public function saveDeposit(Request $request)
    {

        $data = $request->validate([
            'plan' => ['required'],
            'amount' => ['required'],
            'channel' => ['required']
        ]);

        $channel = PaymentChannel::findOrFail($data['channel']);
        $plan = Plan::findOrFail($data['plan']);

        $currencyEquivalent = null;
        if ((int)$data['channel'] !== 1) {
            $currencyEquivalent = $this->getCurrencyValue($channel->abbreviation, $data['amount']);
        }

        if ((double)$data['amount'] < (double)$plan->min) {
            return back()->withErrors([
                'amount' => 'Amount is less than selected plan minimum capital.'
            ]);
        }

        $success = false;
        $deposit = null;
        DB::transaction(function () use (&$currencyEquivalent, $data, &$deposit, &$success) {
            $deposit = Deposit::create([
                'channel_id' => $data['channel'],
                'user_id' => auth()->id(),
                'plan_id' => $data['plan'],
                'amount' => $data['amount'],
                'currency_equivalent' => $currencyEquivalent,
                'ref_id' => \App\Models\Deposit::generateReferenceId()
            ]);

            $deposit->refresh([
                'channel'
            ]);

            if (!config('mail.paused'))
                Mail::to(auth()->user()->email)->send(new NewDepositMail($deposit, auth()->user()));
            $success = true;
        });

        if (!$success) {
            $plans = Plan::all();
            $channels = PaymentChannel::supported()->get(['id', 'name']);

            return redirect()->route('deposit.create')->with([
                'plans' => $plans,
                'channels' => $channels,
                'message' => 'An unexpected error occurred. Please try again later.',
                'type' => 'danger'
            ]);
        }

        return redirect()->route('deposit.details', $deposit->ref_id);
    }

    public function showDepositDetails($ref)
    {
        $deposit = Deposit::where('ref_id', $ref)->with(['channel', 'plan'])->firstOrFail();

        return view('customer.deposit-complete')->with([
            'channel' => $deposit->channel,
            'deposit' => $deposit,
            'plan' => $deposit->plan
        ]);
    }

}
