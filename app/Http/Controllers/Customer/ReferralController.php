<?php

namespace App\Http\Controllers\Customer;

use App\Http\Controllers\Controller;
use App\Models\Referral;
use App\Models\User;
use App\Models\Withdrawal;
use App\Traits\AppCalculations;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ReferralController extends Controller
{
    use AppCalculations;

    public function showAllReferralsPage()
    {
        $user = auth()->user();
        $userWallets = $user->wallets()->with(['paymentChannel'])->get();

        return view('customer.referrals-list')->with([
            'referees' => $this->getUserReferrals($user)[0],
            'commission' => $this->getUserReferrals($user)[1],
            'userWallets' => $userWallets
        ]);
    }

    public function storeReferralWithdrawal(Request $request)
    {
        $request->validate([
            'wallet' => ['required'],
            'amount' => ['required', 'numeric'],
        ]);

        $user = auth()->user();
        $referees = $this->getUserReferrals($user)[0];

        if ($referees->count() === 0)
            return back()->with([
                'message' => 'You do not have an active referred user at the moment.',
                'type' => 'warning'
            ]);

        $success = false;
        DB::transaction(function () use ($request, &$success, $user) {
            $user->withdrawals()->create([
                'user_wallet_id' => $request->get('wallet'),
                'ref_id' => Withdrawal::generateRefId(),
                'amount' => $request->get('amount')
            ]);

            $user->referrals()->bonusAvailable()->get()
                ->each(function ($ref) {
                    $ref->bonus_taken = Referral::BONUS_TAKEN;
                    $ref->save();
                });
            $success = true;
        });

        if (!$success)
            return back()->with([
                'message' => 'An unexpected error occurred. Please try again later.',
                'type' => 'danger'
            ]);

        return back()->with([
            'message' => 'Withdrawal request sent.',
            'type' => 'success'
        ]);
    }
}
