<?php

namespace App\Http\Controllers\Customer;

use App\Http\Controllers\Controller;
use App\Mail\ResendVerificationMail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use App\Models\User;

class VerificationController extends Controller
{
//    private function redirectTo()
//    {
//        return auth()->user()->isAdmin() ? '/admin' : '/dashboard';
//    }

    public function verify($token)
    {
        $user = User::where('verification_token', $token)->firstOrFail();

//        Auth::attempt([
//            'email' => $user->email,
//            'password' =>
//        ]);

        if ($user->hasVerifiedEmail()) {
            return redirect()->route('customer.home');
        }

        $user->verified = User::VERIFIED;
        $user->email_verified_at = now();
        $user->verification_token = null;

        $user->save();

        return redirect()->route('customer.home');
    }

    public function resendVerification()
    {
        $user = auth()->user();

        // Check if verification column of the user has a value

        if ($user->verification_token == null) {
            $user->verification_token = User::generateVerificationToken();
            $user->save();
        }

        //Send email to the user with the verification code
        try {
            retry(5, function () use ($user) {
                if (!config('mail.paused'))
                    Mail::to($user->email)->send(new ResendVerificationMail($user));
            }, 300);
        } catch (\Exception $e) {
            report($e);
        }

        // Return response to the user
        return response()->json([
            'success' => true,
            'alert' => 'success',
            'data' => '',
            'message' => 'Verification email sent. Please check your email.',
        ]);
    }
}
