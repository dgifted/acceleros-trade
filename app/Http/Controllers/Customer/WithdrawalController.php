<?php

namespace App\Http\Controllers\Customer;

use App\Http\Controllers\Controller;
use App\Mail\NewWithdrawalMail;
use App\Mail\ReinvestmentMail;
use App\Models\Deposit;
use App\Models\PaymentChannel;
use App\Models\Withdrawal;
use App\Traits\AppCalculations;
use App\Traits\CryptoApi;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

class WithdrawalController extends Controller
{
    use AppCalculations, CryptoApi;

    public function showWithdrawalPage()
    {
        $user = auth()->user();

        $totalPendingWithdrawals = 0;
        $earnedProfit = $this->calculateRealtimeEarning($user);
        $deposits = $user->deposits()->with(['plan', 'channel'])
            ->approved()
            ->notWithdrawn()
            ->get();

        $maturedDeposits = $deposits->filter(function ($deposit) {
            return $this->checkDepositMaturity($deposit);
        })->each(function ($deposit) use (&$totalPendingWithdrawals) {
            $totalPendingWithdrawals += $deposit->amount;
        });

        // TODO: Migrate to util class
        $canWithdraw = $maturedDeposits->count() > 0 || $user->balance > 0;

        $totalPendingWithdrawals += $earnedProfit + $user->balance;

        $totalBalance = $this->getUserDepositBalance($user) + $earnedProfit + $user->balance + $this->getUserReferrals($user)[1];
        $userWallets = $user->wallets()->with(['paymentChannel'])->get();
        $channels = PaymentChannel::with(['customerWallets.customer', 'deposits'])
            ->where('supported', \App\Models\PaymentChannel::SUPPORTED)
            ->get()
            ->each(function ($channel) use ($userWallets, $user) {
                $channel->userWallet = null;
                $channel->availableFund = 0;
                $channel->pendingFund = 0;

                foreach ($userWallets as $wallet) {
                    if ($wallet->channel_id === $channel->id) {
                        $channel->userWallet = $wallet->address;
                        $channel->availableFund = $this->calculateRealtimeEarning($user) + $user->balance;
                        $channel->pendingFund = $this->calculateRealtimeEarning($user) + $this->getUserDepositBalance($user);
                    }
                }
            });

        return view('customer.withdraw')->with([
            'balance' => $totalBalance,
            'canWithdraw' => $canWithdraw === true ? Withdrawal::CAN_WITHDRAW : Withdrawal::CANNOT_WITHDRAW,
            'channels' => $channels,
            'pendingWithdrawals' => $totalPendingWithdrawals,
            'userWallets' => $userWallets
        ]);
    }

    public function storeWithdrawal(Request $request)
    {
        $user = auth()->user();
        $totalPendingWithdrawals = 0;
        $earnedProfit = $this->calculateRealtimeEarning($user);

        $amount = (double)$request->get('withdrawalAmount');
        $canWithdraw = $request->get('can_withdraw');
        $walletId = $request->get('wallet');

        if ($amount <= 0 || $canWithdraw !== Withdrawal::CAN_WITHDRAW)
            return back()->with([
                'message' => 'You are not allowed to make this withdrawal at this time.'
            ]);

        $deposits = $user->deposits()->with(['plan', 'channel'])
            ->approved()
            ->notWithdrawn()
            ->get();

        $maturedDeposits = $deposits->filter(function ($deposit) {
            return $this->checkDepositMaturity($deposit);
        })->each(function ($deposit) use (&$totalPendingWithdrawals) {
            $totalPendingWithdrawals += $deposit->amount;
        });

        $totalPendingWithdrawals += $earnedProfit + $user->balance + $this->getUserReferrals($user)[1];

        if ($amount > $totalPendingWithdrawals)
            return back()->with([
                'message' => 'You are not allowed to make this withdrawal at this time.'
            ]);

        $success = false;
        DB::transaction(function () use (&$success, &$maturedDeposits, &$user, $amount, $walletId, $totalPendingWithdrawals) {
            $maturedDeposits->each(function ($deposit) {
                $deposit->withdrawn = Deposit::WITHDRAWN;
                $deposit->withdrawn_at = now();
                $deposit->save();
            });

            $user->referrals()->bonusAvailable()->get()
                ->each(function ($ref) {
                    $ref->bonus_taken = \App\Models\Referral::BONUS_TAKEN;
                    $ref->save();
                });

            $withdrawal = $user->withdrawals()->create([
                'ref_id' => Withdrawal::generateRefId(),
                'amount' => $amount,
                'user_wallet_id' => $walletId
            ]);

            $user->balance = ($totalPendingWithdrawals - $amount);
            $user->save();

            if (!config('mail.paused'))
                Mail::to($user->email)->send(new NewWithdrawalMail($user, $withdrawal->refresh(['userWallet.paymentChannel'])));
            $success = true;
        });

        if (!$success)
            return back()->with([
                'message' => 'Withdrawal request failed. Please try again.',
                'type' => 'error'
            ]);

        return back()->with([
            'message' => 'Withdrawal request sent.',
            'type' => 'success'
        ]);
    }
}
