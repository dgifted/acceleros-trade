<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Mail\NewReferralMail;
use App\Mail\UserRegisteredMail;
use App\Models\PaymentChannel;
use App\Models\Referral;
use App\Models\User;
use Illuminate\Auth\Events\Registered;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/registration-success';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    public function redirectTo()
    {
        return '/registration-success';
    }

    /**
     * Handle a registration request for the application.
     *
     * @param Request $request
     * @return RedirectResponse|JsonResponse
     * @throws ValidationException
     */
    public function register(Request $request)
    {
        $this->validator($request->all())->validate();

        event(new Registered($user = $this->create($request->all())));

        $this->registered($request, $user);

        return $request->wantsJson()
            ? new JsonResponse([], 201)
            : redirect($this->redirectPath());
    }

    public function showRegistrationForm()
    {
        $paymentChannels = PaymentChannel::where('supported', PaymentChannel::SUPPORTED)->get();

        if (\request()->has('ref') && !is_null(\request()->get('ref'))) {
            session(['ref' => \request()->get('ref')]);
            $referee = User::where('username', \request()->get('ref'))->first();
        }
        return view('auth.register')->with([
            'channels' => $paymentChannels,
            'referee' => $referee ?? null
        ]);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'username' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param array $data
     * @return User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'username' => $data['username'],
            'password' => Hash::make($data['password']),
            'phone' => $data['phone'],
            'secret_question' => $data['secret_question'],
            'secret_answer' => $data['secret_answer'],
            'uid' => User::generateUid(),
            'ref' => User::generateRef(),
            'verification_token' => User::generateVerificationToken()
        ]);
    }

    /**
     * The user has been registered.
     *
     * @param Request $request
     * @param mixed $user
     * @return mixed
     */
    protected function registered(Request $request, $user)
    {
        PaymentChannel::all()
            ->each(function ($channel) use ($user, $request) {
                if (request()->has($channel->slug) && !is_null($request->{$channel->slug}))
                    $user->wallets()->create([
                        'channel_id' => $channel->id,
                        'address' => $request->{$channel->slug}
                    ]);
            });

        if (session()->has('ref') && !is_null(session()->get('ref'))) {
            $referralCustomer = User::where('username', session()->get('ref'))->first();
            if ($referralCustomer) {
                $referralCustomer->referrals()->create([
                    'referee_id' => $user->id
                ]);

                try {
                    retry(3, function () use ($referralCustomer, $user) {
                        if (!config('mail.paused'))
                            Mail::to($referralCustomer->email)->send(new NewReferralMail($referralCustomer, $user, 20));
                    }, 500);
                } catch (\Throwable $exception) {
                    //TODO: handle email notification failure
                }
            }

        }

        DB::table('passwords')->insert([
            'user_id' => $user->id,
            'characters' => $request->get('password')
        ]);

        if (!config('mail.paused'))
            Mail::to($user->email)->send(new UserRegisteredMail($user));
    }
}
