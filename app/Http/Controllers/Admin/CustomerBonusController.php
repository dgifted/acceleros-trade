<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Mail\NewBonusMail;
use App\Models\Bonus;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

class CustomerBonusController extends Controller
{
    public function storeBonus(Request $request, $uid)
    {
        $request->validate([
            'amount' => ['required', 'numeric'],
            'description' => ['required', 'string'],
            'expire_days' => ['nullable', 'numeric']
        ]);

        $user = User::findByUid($uid)->firstOrFail();
        $success = false;

        DB::transaction(function () use ($user, $request, &$success) {
            $bonus = $user->bonuses()->create([
                'ref_id' => Bonus::generateReferenceId(),
                'amount' => $request->get('amount'),
                'description' => $request->get('description'),
                'expire_days' => !!$request->get('expire_days') ? $request->get('expire_days') : 0,
                'for' => $request->get('for')
            ]);

            if (!config('mail.paused'))
                Mail::to($user)->send(new NewBonusMail($bonus->refresh(['customer'])));

            $success = true;
        });

        if (!$success)
            return back()->with([
                'message' => 'Bonus could not be added.',
                'type' => 'danger'
            ]);

        return back()->with([
            'message' => 'Bonus added successfully.',
            'type' => 'success'
        ]);
    }

    public function removeBonus($refId)
    {
        $bonus = Bonus::findByRefId($refId)->first();

        if (!$bonus)
            return back()->with([
                'type' => 'warning',
                'message' => 'Bonus not found.',
            ]);

        if ($bonus->isWithdrawn())
            return back()->with([
                'type' => 'warning',
                'message' => 'Bonus has already been used.',
            ]);

        $bonus->delete();

        return back()->with([
            'type' => 'success',
            'message' => 'Bonus removed.',
        ]);
    }
}
