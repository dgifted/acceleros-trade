<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\PaymentChannel;
use App\Models\User;
use App\Traits\AppCalculations;
use Illuminate\Http\Request;

class UserController extends Controller
{
    use AppCalculations;

    public function showAllUsersPage()
    {
        $users = User::notAdmin()->get();
        return view('admin.users')->with(['users' => $users]);
    }

    public function showSingleUserPage($uid)
    {
        $user = User::where('uid', $uid)->with(['bonuses', 'deposits', 'withdrawals', 'wallets.paymentChannel'])->firstOrFail();
        $activeBonus = $this->getActiveBonuses($user);
        $depositedAmount = 0;
        $withdrawnAmount = 0;
        $transactions = [];

        $user->deposits->each(function ($deposit) use (&$depositedAmount, &$transactions) {
            $depositedAmount += $deposit->amount;
            array_push($transactions, (object)[
                'ref_id' => $deposit->ref_id,
                'type' => 'deposit',
                'amount' => $deposit->amount,
                'date' => $deposit->created_at,
                'status' => $this->getStatusText($deposit->status)
            ]);
        });

        $user->withdrawals->each(function ($withdrawal) use (&$withdrawnAmount, &$transactions) {
            $withdrawnAmount += $withdrawal->amount;
            array_push($transactions, (object)[
                'ref_id' => $withdrawal->ref_id,
                'type' => 'withdrawal',
                'amount' => $withdrawal->amount,
                'date' => $withdrawal->created_at,
                'status' => $this->getStatusText($withdrawal->status)
            ]);
        });

        $earnings = $this->calculateRealtimeEarning($user);

        $channels = PaymentChannel::supported()->get()
            ->each(function ($channel) use ($user) {
                foreach ($user->wallets as $wallet) {
                    if ($wallet->channel_id === $channel->id) {
                        $channel->address = $wallet->address;
                    }
                }
            });

        return view('admin.user-details')->with([
            'activeBonus' => $activeBonus,
            'channels' => $channels,
            'depositedAmount' => $depositedAmount,
            'earnings' => $earnings,
            'transactions' => collect($transactions),
            'user' => $user,
            'withdrawnAmount' => $withdrawnAmount,
        ]);
    }

    public function deactivateUser($uid)
    {
        $user = User::where('uid', $uid)->firstOrFail();
        $user->is_active = User::STATUS_INACTIVE;
        $user->save();


        return back()->with([
            'message' => 'Customer deactivated successfully.',
            'type' => 'success'
        ]);
    }

    public function reactivateUser($uid)
    {
        $user = User::where('uid', $uid)->firstOrFail();
        $user->is_active = User::STATUS_ACTIVE;
        $user->save();


        return back()->with([
            'message' => 'Customer activated successfully.',
            'type' => 'success'
        ]);
    }

    public function updateUserDetails(Request $request, $uid)
    {
        $user = User::where('uid', $uid)->firstOrFail();

        if ($request->has('name') && !is_null($request->get('name'))) {
            $user->name = $request->get('name');
            $user->save();
        }

        $userWallets = $user->wallets()->get();

        PaymentChannel::all()
            ->each(function ($channel) use ($request, $user, $userWallets) {
                if ($request->has($channel->slug) && !is_null($request->get($channel->slug))) {

                    $userChannelWallet = $userWallets->first(function ($wall) use ($channel) {
                        return $wall->channel_id === $channel->id;
                    });

                    if (!!$userChannelWallet) {
                        $wallet = $userChannelWallet;
                        $wallet->address = $request->get($channel->slug);
                        $wallet->save();
                    } else {
                        $user->wallets()->create([
                            'channel_id' => $channel->id,
                            'address' => $request->get($channel->slug)
                        ]);
                    }
                }
            });

        return back()->with([
            'message' => 'Account data updated successfully',
            'type' => 'success'
        ]);
    }

    public function verifyUser($uid)
    {
        $user = User::where('uid', $uid)->firstOrFail();
        $user->verified = User::VERIFIED;
        $user->email_verified_at = now();
        $user->save();

        return back()->with([
            'message' => 'Customer verified successfully.',
            'type' => 'success'
        ]);
    }

    public function deleteUser($uid)
    {
        $user = User::findByUid($uid)->firstOrFail();

        $user->delete();

        return back()->with([
            'message' => 'Customer deleted successfully.',
            'type' => 'success'
        ]);
    }

    private function getStatusText($statusCode)
    {
        switch ($statusCode) {
            case \App\Models\Deposit::STATUS_APPROVED:
                return 'approved';
            case \App\Models\Deposit::STATUS_PENDING:
                return 'pending';
            case \App\Models\Deposit::STATUS_CANCELLED:
                return 'cancelled';
            default:
                return 'failed';
        }
    }
}
