<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Mail\DepositApprovedMail;
use App\Models\Deposit;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

class DepositController extends Controller
{
    public function showAllDepositsPage()
    {
        $deposits = Deposit::with(['customer', 'plan'])->get();

        return view('admin.deposits')->with([
            'deposits' => $deposits
        ]);
    }

    public function showSingleDepositPage($ref_id)
    {
        $deposit = Deposit::findByRef($ref_id)->with(['customer', 'plan', 'channel'])->firstOrFail();

        return view('admin.deposit-details')->with([
            'deposit' => $deposit
        ]);
    }

    public function approveDeposit($ref_id)
    {
        $deposit = Deposit::findByRef($ref_id)->with(['customer'])->firstOrFail();
        $success = false;

        DB::transaction(function () use (&$deposit, &$success) {
            $deposit->status = Deposit::STATUS_APPROVED;
            $deposit->approved_at = now();
            $deposit->save();

            if (!config('mail.paused'))
                Mail::to($deposit->customer->email)->send(new DepositApprovedMail($deposit));
            $success = true;
        });

        if (!$success)
            return back()->with([
                'message' => 'Deposit approval failed. Please try again later.',
                'type' => 'danger'
            ]);

        return back()->with([
            'message' => 'Deposit approved successfully.',
            'type' => 'success'
        ]);
    }

    public function editDeposit(Request $request, $refId)
    {
        $deposit = Deposit::findByRef($refId)->firstOrFail();

        $deposit->amount = $request->get('amount');
        $deposit->approved_at = Carbon::parse($request->get('date'));
        $deposit->save();

        return $request->wantsJson()
            ? response()->json([
                'success' => true,
                'message' => 'Deposit amount updated successfully.',
                'data' => $deposit
            ])
            : redirect()->route('admin.view-deposit-details', $deposit->ref_id);
    }

    public function deleteDeposit($refId)
    {
        $deposit = Deposit::findByRef($refId)->firstOrFail();

        $deposit->delete();

        return back()->with([
            'message' => 'Deposit deleted successfully.',
            'type' => 'success'
        ]);
    }

}
