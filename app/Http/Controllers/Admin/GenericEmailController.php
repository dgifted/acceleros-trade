<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Mail\GenericEmailMail;
use App\Mail\GenericMail;
use App\Mail\UserRegisteredMail;
use App\Models\GenericEmail;
use App\Models\User;
use App\Services\EmailServices;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;

class GenericEmailController extends Controller
{
    public function index()
    {
        $emails = GenericEmail::with(['recipient'])->latest()->get();

        return view('admin.sent-emails')->with([
            'emails' => $emails
        ]);
    }

    public function deleteEmail($refId)
    {
        $email = GenericEmail::findByRefId($refId)->firstOrFail();
        $email->delete();

        return back()->with([
            'message' => 'Email entry deleted',
            'type' => 'success'
        ]);
    }

    public function createNewEmail()
    {
        $users = User::notAdmin()->verified()->get();

        return view('admin.new-email')->with(['users' => $users]);
    }

    public function storeNewEmail(Request $request)
    {
        $data = $request->validate([
            'recipients' => ['nullable'],
            'subject' => ['required'],
            'content' => ['required'],
            'option' => ['nullable']
        ]);

        if (isset($data['option'])) {
            $usersIds = User::notAdmin()->verified()->get()->pluck(['id'])->toArray();
            (new EmailServices())->sendOutEmailsToUsers($data['subject'], $data['content'], $usersIds);
        }

        if (!isset($data['option']) && count($data['recipients']) > 0) {
            (new EmailServices())->sendOutEmailsToUsers($data['subject'], $data['content'], $data['recipients']);
        }

        if (!isset($data['option']) && count($data['recipients']) === 0)
            return back()->with([
                'message' => 'Please select some recipients.',
                'type' => 'danger'
            ]);

        return back()->with([
            'message' => 'Email sent.',
            'type' => 'success'
        ]);
    }
}
