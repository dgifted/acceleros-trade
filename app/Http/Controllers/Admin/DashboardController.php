<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Deposit;
use App\Models\User;
use App\Traits\AppCalculations;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    use AppCalculations;

    public function showDashboardPage()
    {
        $totalDeposits = 0;
        $accumulatedInterest = 0;
        $dueDeposits = 0;

        Deposit::approved()->latest()->with(['customer'])->get()
            ->each(function ($deposit) use (&$totalDeposits, &$dueDeposits) {
                $totalDeposits += $deposit->amount;
                if ($this->checkDepositMaturity($deposit))
                    $dueDeposits += $deposit->amount;
            });

        $recentDeposits = Deposit::latest()->get()->take(7);
        $users = User::notAdmin()->latest()->with(['deposits', 'deposits.plan'])->get()
        ->each(function ($user) use (&$accumulatedInterest) {
            $accumulatedInterest += $this->calculateRealtimeEarning($user);
        });


        return view('admin.index')->with([
            'depositedAmount' => round($totalDeposits, 2),
            'accumulatedInterest' => round($accumulatedInterest, 2),
            'dueDeposit' => $dueDeposits,
            'usersCount' => $users->count(),
            'recentDeposits' => $recentDeposits,
            'recentCustomers' => $users->take(5),
        ]);
    }
}
