<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\PaymentChannel;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class PaymentChannelController extends Controller
{
    public function showAllPaymentChannelsPage()
    {
        $paymentChannels = PaymentChannel::all();

        return view('admin.payment-channels')->with([
            'channels' => $paymentChannels
        ]);
    }

    public function showSinglePaymentChannelPage($refId)
    {
        //Idle controller method
    }

    public function storePaymentChannel(Request $request)
    {
        $data = $request->validate([
            'name' => ['required', 'string'],
            'wallet' => ['required', 'string'],
            'abbreviation' => ['nullable', 'string'],
        ]);

        PaymentChannel::create([
            'name' => $data['name'],
            'ref_id' => PaymentChannel::generateRefId(),
            'slug' => Str::of($data['name'])->slug('_'),
            'abbreviation' => $data['abbreviation'] ?? null,
            'wallet' => $data['wallet'],
        ]);

        return back()->with([
            'message' => 'Payment channel added successfully.',
            'type' => 'success'
        ]);
    }

    public function showPaymentChannelEditPage($refId)
    {
        $channel = PaymentChannel::findByRef($refId)->firstOrFail();

        return view('admin.payment-channels-edit')->with([
            'channel' => $channel
        ]);
    }

    public function updatePaymentChannel(Request $request, $refId)
    {
        $request->validate([
            'name' => ['required'],
            'wallet' => ['required'],
        ]);

        $channel = PaymentChannel::findByRef($refId)->firstOrFail();
        $data = [
            'slug' => Str::of(strtolower($request->get('name')))->slug('_')
        ];

        foreach ($request->all() as $key => $reqEntry) {
            if (!is_null($reqEntry)) {
                $data[$key] = $reqEntry;
            }
        }

        $data['name'] = strtolower($data['name']);
        $channel->fill($data);
        $channel->save();

        return back()->with([
            'message' => 'Payment channel updated successfully.',
            'type' => 'success'
        ]);
    }

    public function deletePaymentChannel($refId)
    {
        $channel = PaymentChannel::findByRef($refId)->firstOrFail();

        $channel->delete();

        return back()->with([
            'message' => 'Payment channel deleted successfully.',
            'type' => 'success'
        ]);
    }

    public function changePaymentChannelStatus($refId)
    {
        $channel = PaymentChannel::findByRef($refId)->firstOrFail();
        $channel->supported = $channel->supported === PaymentChannel::SUPPORTED
            ? PaymentChannel::UNSUPPORTED
            : PaymentChannel::SUPPORTED;
        $channel->save();

        return back()->with([
            'message' => 'Payment channel status changed successfully.',
            'type' => 'success'
        ]);
    }
}
