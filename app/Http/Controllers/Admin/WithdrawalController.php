<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Withdrawal;
use Illuminate\Http\Request;

class WithdrawalController extends Controller
{
    public function showAllWithdrawalPage()
    {
        $withdrawals = Withdrawal::with(['customer', 'userWallet.paymentChannel'])->get();

        return view('admin.withdrawals')->with([
            'withdrawals' => $withdrawals
        ]);
    }

    public function showSingleWithdrawalPage($refId)
    {
        $withdrawal = Withdrawal::findByRef($refId)->with(['deposit.plan', 'deposit.customer'])->firstOrFail();

        return view('admin.withdrawal-details')->with([
            'withdrawal' => $withdrawal
        ]);
    }

    public function approveWithdrawal($refId)
    {
        $withdrawal = Withdrawal::findByRef($refId)->firstOrFail();
        $withdrawal->status = Withdrawal::STATUS_APPROVED;
        $withdrawal->save();

        return back()->with([
            'message' => 'Withdrawal approved successfully.',
            'type' => 'success'
        ]);
    }

    public function deleteWithdrawal($refId)
    {
        $withdrawal = Withdrawal::findByRef($refId)->firstOrFail();

        $withdrawal->delete();

        return back()->with([
            'message' => 'Withdrawal deleted successfully.',
            'type' => 'success'
        ]);
    }
}
