<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Plan;
use Illuminate\Http\Request;

class PlanController extends Controller
{
    public function showAllPlansPage()
    {
        $plans = Plan::all();

        return view('admin.plans')->with([
            'plans' => $plans
        ]);
    }

    public function showSinglePlanPage($refId)
    {
        $plan = Plan::findByRef($refId)->with(['deposits'])->firstOrFail();

        $totalDeposits = 0;
        if ($plan->deposits->count() > 0) {
            $plan->deposits->filter(function ($deposit) {
                return $deposit->isApproved();
            })->each(function ($deposit) use (&$totalDeposits) {
                $totalDeposits += $deposit->amount;
            });
        }

        return view('admin.plan-details')->with([
            'plan' => $plan,
            'totalDeposits' => $totalDeposits
        ]);
    }

    public function storePlan(Request $request)
    {

    }

    public function deletePlan($refId)
    {
        $plan = Plan::findByRef($refId)->firstOrFail();

        $plan->delete();

        return back()->with([
            'message' => 'Plan deleted successfully.',
            'type' => 'success'
        ]);
    }

    public function updatePlan(Request $request, $refId)
    {
        $data = $request->validate([
            'title' => ['required'],
            'min' => ['required'],
            'max' => ['required'],
            'percentage' => ['required'],
            'duration' => ['required'],
            'description' => ['required'],
        ]);

        $plan = Plan::findByRef($refId)->firstOrFail();
        $plan->fill($data);
        $plan->save();

        return back()->with([
            'message' => 'Plan updated successfully.',
            'type' => 'success'
        ]);
    }
}
