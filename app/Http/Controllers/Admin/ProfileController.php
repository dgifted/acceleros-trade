<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class ProfileController extends Controller
{
    public function showAdminProfilePage()
    {
        $user = auth()->user();

        return view('admin.profile')->with([
            'user' => $user
        ]);
    }

    public function updatePassword(Request $request)
    {
        $data = $request->validate([
            'password' => ['required', 'confirmed']
        ]);

        $user = auth()->user();
        $user->password = Hash::make($data['password']);
        $user->save();

        return
            response()->json([
                'success' => true,
                'alert' => 'success',
                'data' => $user,
                'message' => 'Password changed successful.'
            ], 201);
    }

    public function uploadProfilePicture(Request $request)
    {
        $request->validate([
            'file' => ['required', 'image']
        ]);

        $user = auth()->user();
        $user->avatar = $request->file('file')->store('', 'avatars');
        $user->save();

        return
            response()->json([
                'success' => true,
                'alert' => 'success',
                'data' => '',
                'message' => 'Profile picture updated successful.'
            ], 201);
    }
}
