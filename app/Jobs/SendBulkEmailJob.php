<?php

namespace App\Jobs;

use App\Mail\GenericEmailMail;
use App\Models\GenericEmail;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Mail;

class SendBulkEmailJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $content, $subject, $users;

    /**
     * Create a new job instance.
     *
     * @param $users
     * @param $subject
     * @param $content
     */
    public function __construct($users, $subject, $content)
    {
        $this->content = $content;
        $this->subject = $subject;
        $this->users = $users;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        foreach ($this->users as $user) {
            try {
                Mail::to($user)->send(new GenericEmailMail($this->content, $this->subject, $user));
                GenericEmail::create([
                    'ref_id' => GenericEmail::generateReferenceId(),
                    'recipient_id' => $user->id,
                    'subject' => $this->subject,
                    'content' => $this->content
                ]);
            } catch (\Throwable $exception) {
                report($exception);
            }
        }
    }
}
