<?php

namespace App\Services;

use App\Jobs\SendBulkEmailJob;
use App\Models\User;

class EmailServices {
    public function sendOutEmailsToUsers($subject, $content, $userIds = [])
    {
        $users = User::find($userIds);

        if ($users->count() > 0) {
            dispatch(new SendBulkEmailJob($users, $subject, $content));
        }
    }
}
