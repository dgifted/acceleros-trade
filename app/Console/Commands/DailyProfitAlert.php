<?php

namespace App\Console\Commands;

use App\Mail\UserProfitsMail;
use App\Models\Deposit;
use App\Models\User;
use App\Traits\AppCalculations;
use Illuminate\Console\Command;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Mail;

class DailyProfitAlert extends Command
{
    use AppCalculations;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'alert:profits';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send profits email notifications to customers with active deposits.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        $users = User::whereHas('deposits', function (Builder $query) {
            $query->where('withdrawn', '==', Deposit::NOT_WITHDRAWN)
                ->where('status', Deposit::STATUS_APPROVED);
        })->with(['deposits'])->get();

        if ($users->count() > 0) {
            $users->each(function ($user) {
                $profits = $this->calculateRealtimeEarning($user);
                retry(3, function () use ($profits, $user) {
                    if (!config('mail.paused'))
                        Mail::to($user->email)->send(new UserProfitsMail($profits, $user));
                });
            });
        }

    }
}
