<?php

use Illuminate\Support\Facades\Route;

//Landing page routes
Route::middleware(['guest'])->group(function () {
    Route::view('/', 'landing.home')->name('home');
    Route::view('/about-us', 'landing.about')->name('about');
    Route::view('/affiliate', 'landing.affiliate')->name('affiliate');
    Route::view('/contact-us', 'landing.contact')->name('contact');
    Route::view('/faqs', 'landing.faqs')->name('faqs');
    Route::view('/news', 'landing.news')->name('news');
    Route::view('/terms-and-condition', 'landing.terms')->name('terms-policy');
    Route::view('/registration-success', 'landing.register-success')->name('thanks');
});

Auth::routes();

Route::middleware(['admin'])->group(function () {
    //Admin routes
    Route::prefix('admin')->group(function () {
        //Dashboard
        Route::redirect('/', '/dashboard');
        Route::get('/dashboard', [\App\Http\Controllers\Admin\DashboardController::class, 'showDashboardPage'])->name('admin.home');

        //Bonus
        Route::post('/customer-bonus/{uid}', [\App\Http\Controllers\Admin\CustomerBonusController::class, 'storeBonus'])->name('admin.customer-add-bonus');
        Route::get('/remove-bonus/{refId}', [\App\Http\Controllers\Admin\CustomerBonusController::class, 'removeBonus'])->name('admin.remove-bonus');

        //Customer
        Route::delete('/customers/{uid}', [\App\Http\Controllers\Admin\UserController::class, 'deleteUser'])->name('admin.customer-delete');
        Route::get('/customers/{uid}', [\App\Http\Controllers\Admin\UserController::class, 'showSingleUserPage'])->name('admin.view-customer-details');
        Route::post('/customers/{uid}', [\App\Http\Controllers\Admin\UserController::class, 'updateUserDetails'])->name('admin.customer-update');
        Route::post('/customers/{uid}/deactivate', [\App\Http\Controllers\Admin\UserController::class, 'deactivateUser'])->name('admin.customer-deactivate');
        Route::post('/customers/{uid}/activate', [\App\Http\Controllers\Admin\UserController::class, 'reactivateUser'])->name('admin.customer-activate');
        Route::post('/customers/{uid}/verify', [\App\Http\Controllers\Admin\UserController::class, 'verifyUser'])->name('admin.customer-verify');
        Route::get('/customers', [\App\Http\Controllers\Admin\UserController::class, 'showAllUsersPage'])->name('admin.view-customers');

        //Deposit
        Route::get('/deposits/{ref}', [\App\Http\Controllers\Admin\DepositController::class, 'showSingleDepositPage'])->name('admin.view-deposit-details');
        Route::post('/deposits/{ref}', [\App\Http\Controllers\Admin\DepositController::class, 'approveDeposit'])->name('admin.deposit-approve');
        Route::delete('/deposits/{ref}/delete', [\App\Http\Controllers\Admin\DepositController::class, 'deleteDeposit'])->name('admin.deposit-delete');
        Route::post('/deposits/{ref}/edit', [\App\Http\Controllers\Admin\DepositController::class, 'editDeposit'])->name('admin.deposit-edit');
        Route::get('/deposits', [\App\Http\Controllers\Admin\DepositController::class, 'showAllDepositsPage'])->name('admin.view-deposits');

        //Email
        Route::get('/emails/new', [\App\Http\Controllers\Admin\GenericEmailController::class, 'createNewEmail'])->name('admin.send-new-email');
        Route::get('/emails/{id}', [\App\Http\Controllers\Admin\GenericEmailController::class, 'deleteEmail'])->name('admin.sent-emails-delete');
        Route::post('/emails/new', [\App\Http\Controllers\Admin\GenericEmailController::class, 'storeNewEmail']);
        Route::get('/emails', [\App\Http\Controllers\Admin\GenericEmailController::class, 'index'])->name('admin.sent-emails');

        //PaymentChannel
        Route::get('/payment-channels/{ref}', [\App\Http\Controllers\Admin\PaymentChannelController::class, 'showSinglePaymentChannelPage'])->name('admin.payment-channel-details');
        Route::delete('/payment-channels/{ref}/delete', [\App\Http\Controllers\Admin\PaymentChannelController::class, 'deletePaymentChannel'])->name('admin.payment-channel-delete');
        Route::get('/payment-channels/{ref}/update', [\App\Http\Controllers\Admin\PaymentChannelController::class, 'showPaymentChannelEditPage'])->name('admin.payment-channel-update');
        Route::patch('/payment-channels/{ref}/update', [\App\Http\Controllers\Admin\PaymentChannelController::class, 'updatePaymentChannel']);
        Route::get('/payment-channels/{ref}/toggle-status', [\App\Http\Controllers\Admin\PaymentChannelController::class, 'changePaymentChannelStatus'])->name('admin.payment-channel-toggle-status');
        Route::get('/payment-channels', [\App\Http\Controllers\Admin\PaymentChannelController::class, 'showAllPaymentChannelsPage'])->name('admin.view-payment-channels');
        Route::post('/payment-channels', [\App\Http\Controllers\Admin\PaymentChannelController::class, 'storePaymentChannel']);

        //Plan
        Route::delete('/plans/{ref}/delete', [\App\Http\Controllers\Admin\PlanController::class, 'deletePlan'])->name('admin.plan-delete');
        Route::patch('/plans/{ref}/edit', [\App\Http\Controllers\Admin\PlanController::class, 'updatePlan'])->name('admin.plan-edit');
        Route::get('/plans/{ref}', [\App\Http\Controllers\Admin\PlanController::class, 'showSinglePlanPage'])->name('admin.plan-details');
        Route::get('/plans', [\App\Http\Controllers\Admin\PlanController::class, 'showAllPlansPage'])->name('admin.view-plans');
        Route::post('/plans', [\App\Http\Controllers\Admin\PlanController::class, 'storePlan']);

        //Profile
        Route::get('/profile', [\App\Http\Controllers\Admin\ProfileController::class, 'showAdminProfilePage'])->name('admin.profile');
        Route::post('/profile/upload-avatar', [\App\Http\Controllers\Admin\ProfileController::class, 'uploadProfilePicture'])->name('admin.profile-upload-avatar');
        Route::post('/profile/update-password', [\App\Http\Controllers\Admin\ProfileController::class, 'updatePassword'])->name('admin.profile-update-password');

        //Withdrawal
        Route::get('/withdrawals/{ref}', [\App\Http\Controllers\Admin\WithdrawalController::class, 'showSingleWithdrawalPage'])->name('admin.view-withdrawal-details');
        Route::post('/withdrawals/{ref}/approve', [\App\Http\Controllers\Admin\WithdrawalController::class, 'approveWithdrawal'])->name('admin.withdrawal-approve');
        Route::delete('/withdrawals/{ref}/delete', [\App\Http\Controllers\Admin\WithdrawalController::class, 'deleteWithdrawal'])->name('admin.withdrawal-delete');
        Route::get('/withdrawals', [\App\Http\Controllers\Admin\WithdrawalController::class, 'showAllWithdrawalPage'])->name('admin.view-withdrawals');

        //Logs
        Route::get('/logs', [\Rap2hpoutre\LaravelLogViewer\LogViewerController::class, 'index'])->name('admin.view-logs');
    });
});

Route::middleware(['customer'])->group(function () {
    //Customer routes
    Route::prefix('')->group(function () {
        //Account
        Route::get('/account', [\App\Http\Controllers\Customer\AccountController::class, 'showAccountUpdatePAge'])->name('account.home');
        Route::post('/account', [\App\Http\Controllers\Customer\AccountController::class, 'updateAccount']);
        Route::get('/account-history', [\App\Http\Controllers\Customer\AccountController::class, 'showAccountHistoryPage'])->name('account.history');
        Route::post('/account-history', [\App\Http\Controllers\Customer\AccountController::class, 'filterAccountTransactions']);

        //Bonus
        Route::get('/bonuses/{ref_id}/reinvest', [\App\Http\Controllers\Customer\BonusController::class, 'showBonusReinvestPage'])->name('bonus-reinvest-show');
        Route::post('/bonuses/{ref_id}/reinvest', [\App\Http\Controllers\Customer\BonusController::class, 'reInvestBonus'])->name('bonus-reinvest');
        Route::get('/bonuses/{ref_id}/withdraw', [\App\Http\Controllers\Customer\BonusController::class, 'showBonusWithdrawalPage'])->name('bonus-withdraw-show');
        Route::post('/bonuses/{ref_id}/withdraw', [\App\Http\Controllers\Customer\BonusController::class, 'withdrawBonus'])->name('bonus-withdraw');
        Route::get('/bonuses', [\App\Http\Controllers\Customer\BonusController::class, 'showCustomerBonusesPage'])->name('bonuses.all');

        //Dashboard
        Route::get('/dashboard', [\App\Http\Controllers\Customer\DashboardController::class, 'index'])->name('customer.home');

        //Deposits
        Route::get('/deposits/create', [\App\Http\Controllers\Customer\DepositController::class, 'showNewDepositPage'])->name('deposit.create');
        Route::post('/deposits/create', [\App\Http\Controllers\Customer\DepositController::class, 'saveDeposit']);
        Route::get('/deposits/{ref}', [\App\Http\Controllers\Customer\DepositController::class, 'showDepositDetails'])->name('deposit.details');
        Route::get('/deposits', [\App\Http\Controllers\Customer\DepositController::class, 'showAllDepositsPage'])->name('deposit.all');

        //Reinvestment
        Route::get('/reinvest-profits', [\App\Http\Controllers\Customer\ReinvestmentController::class, 'showReinvestmentPage'])->name('deposit.reinvest');
        Route::post('/reinvest-profits', [\App\Http\Controllers\Customer\ReinvestmentController::class, 'reInvest'])->name('deposit.store-reinvest');

        //Referrals
        Route::post('/referrals/withdraw', [\App\Http\Controllers\Customer\ReferralController::class, 'storeReferralWithdrawal'])->name('referrals.withdraw');
        Route::get('/referrals', [\App\Http\Controllers\Customer\ReferralController::class, 'showAllReferralsPage'])->name('referrals.all');

        //Withdrawals
        Route::get('/withdrawals/create', [\App\Http\Controllers\Customer\WithdrawalController::class, 'showWithdrawalPage'])->name('withdrawal.create');
        Route::post('/withdrawals/create', [\App\Http\Controllers\Customer\WithdrawalController::class, 'storeWithdrawal']);

        //User verification
        Route::get('/verify-email/{token}', [\App\Http\Controllers\Customer\VerificationController::class, 'verify'])->name('verify.email');
        Route::get('/resend-verification', [\App\Http\Controllers\Customer\VerificationController::class, 'resendVerification'])->name('verify.resend');
    });
});
