<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Create admin credentials
        User::create([
            'name' => 'Super Admin',
            'username' => 'admin',
            'email' => 'admin@accelerostrade.com',
            'email_verified_at' => now(),
            'password' => Hash::make('admin.Pa$$word'),
            'admin' => User::USER_ADMIN,
            'uid' => User::generateUid(),
            'ref' => User::generateRef(),
            'verified' => User::VERIFIED
        ]);
    }
}
