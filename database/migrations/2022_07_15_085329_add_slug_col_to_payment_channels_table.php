<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddSlugColToPaymentChannelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('payment_channels', function (Blueprint $table) {
            $table->string('slug')->after('name');
        });

        \App\Models\PaymentChannel::all()
            ->each(function ($channel) {
                $channel->slug = \Illuminate\Support\Str::of($channel->name)->slug('_');
                $channel->save();
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('payment_channels', function (Blueprint $table) {
            $table->dropColumn('slug');
        });
    }
}
