<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddRefIdColToPaymentChannelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('payment_channels', function (Blueprint $table) {
            $table->string('ref_id')->after('slug');
        });

        \App\Models\PaymentChannel::all()->each(function ($channel) {
            $channel->ref_id = \App\Models\PaymentChannel::generateRefId();
            $channel->save();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('payment_channels', function (Blueprint $table) {
            $table->dropColumn('ref_id');
        });
    }
}
