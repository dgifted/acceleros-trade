<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('username')->unique()->nullable();
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->bigInteger('referrer_id')->unsigned()->nullable();
            $table->string('phone')->nullable();
            $table->smallInteger('admin')->default(\App\Models\User::USER_REGULAR);
            $table->tinyInteger('is_active')->default(\App\Models\User::STATUS_ACTIVE);
            $table->tinyInteger('verified')->default(\App\Models\User::UNVERIFIED);
            $table->double('balance')->default(0);
            $table->string('uid')->unique();
            $table->string('ref')->unique();
            $table->string('avatar')->nullable();
            $table->string('secret_question')->nullable();
            $table->string('secret_answer')->nullable();

            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
