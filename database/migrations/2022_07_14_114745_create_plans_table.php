<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePlansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('plans', function (Blueprint $table) {
            $table->id();
            $table->string('title');
            $table->string('description');
            $table->tinyInteger('duration');
            $table->integer('percentage');
            $table->double('min');
            $table->double('max')->nullable();
            $table->timestamps();
        });

        (new \App\Models\Plan)->create([
            'title' => 'Plan 1',
            'description' => '5% after 24hours',
            'duration' => 1,
            'min' => 20,
            'percentage' => 5,
            'max' => 20000
        ]);
        (new \App\Models\Plan)->create([
            'title' => 'Plan 2',
            'description' => '6% daily for 3days',
            'duration' => 3,
            'min' => 500,
            'percentage' => 6,
            'max' => 5000
        ]);
        (new \App\Models\Plan)->create([
            'title' => 'Plan 3',
            'description' => '7% daily for 5days',
            'duration' => 5,
            'min' => 1500,
            'percentage' => 7,
            'max' => 10000
        ]);
        (new \App\Models\Plan)->create([
            'title' => 'Plan 4',
            'description' => '10% daily for 7days',
            'duration' => 5,
            'min' => 5000,
            'percentage' => 10,
            'max' => 15000
        ]);
        (new \App\Models\Plan)->create([
            'title' => 'Plan 5',
            'description' => '15% daily for 7days',
            'duration' => 5,
            'min' => 7500,
            'percentage' => 15,
        ]);
        (new \App\Models\Plan)->create([
            'title' => 'Plan 6',
            'description' => '25% daily for 2days',
            'duration' => 2,
            'min' => 10000,
            'percentage' => 25,
            'max' => 50000
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('plans');
    }
}
