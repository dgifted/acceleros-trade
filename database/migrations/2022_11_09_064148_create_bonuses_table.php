<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBonusesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bonuses', function (Blueprint $table) {
            $table->id();
            $table->string('ref_id');
            $table->foreignId('user_id');
            $table->double('amount')->default(0.0);
            $table->tinyInteger('withdrawn')->default(\App\Models\Bonus::NOT_WITHDRAWN);
            $table->text('description');
            $table->smallInteger('expire_days')->default(0);

            $table->string('for')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bonuses');
    }
}
