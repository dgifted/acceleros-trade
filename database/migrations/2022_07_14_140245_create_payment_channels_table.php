<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePaymentChannelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payment_channels', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('abbreviation')->nullable();
            $table->string('wallet');
            $table->tinyInteger('supported')->default(\App\Models\PaymentChannel::SUPPORTED);
            $table->tinyInteger('is_default')->default(\App\Models\PaymentChannel::NON_DEFAULT);
            $table->timestamps();
        });

        $channels = [
            'perfect money' => ['U57639273', ''],
            'bitcoin' => ['1Hg9uCcQZYp53xjmTHGtm1dsjk8UaLNRE9', 'BTC'],
            'litecoin' => ['1Hg9uCcQZYp53xjmTHGtm1dsjk8UaLNRE9', 'LTC'],
            'dogecoin' => ['1Hg9uCcQZYp53xjmTHGtm1dsjk8UaLNRE9', 'DOGE'],
            'ethereum' => ['1Hg9uCcQZYp53xjmTHGtm1dsjk8UaLNRE9', 'ETC'],
            'bitcoin cash' => ['1Hg9uCcQZYp53xjmTHGtm1dsjk8UaLNRE9', 'BTC-alt'],
            'tether erc20' => ['1Hg9uCcQZYp53xjmTHGtm1dsjk8UaLNRE9', 'USDT'],
            'tether trc20' => ['1Hg9uCcQZYp53xjmTHGtm1dsjk8UaLNRE9', 'USDT'],
            'bnb' => ['1Hg9uCcQZYp53xjmTHGtm1dsjk8UaLNRE9', 'BNB']
        ];

        foreach ($channels as $name => $wallet) {
            \App\Models\PaymentChannel::create([
                'name' => $name,
                'wallet' => $wallet[0],
                'abbreviation' => $wallet[1],
                'supported' => \App\Models\PaymentChannel::SUPPORTED,
                'is_default' => $name === 'bitcoin' ? \App\Models\PaymentChannel::IS_DEFAULT : \App\Models\PaymentChannel::NON_DEFAULT
            ]);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payment_channels');
    }
}
