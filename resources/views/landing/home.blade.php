@extends('layouts.landing-master')
@section('page-title', 'Welcome')

@section('page-styles')

@stop
@section('extra-classes', '')
@section('extra-content')
    <div class="content">

        <div class="bannerwrap" style="margin-bottom: 20px">
            <div class="bannerright">
                <h1 class="bounceInDown wow">
                    SAFE &amp; SECURE<br/>
                    <span style="text-transform: uppercase">Online Trading</span>
                    PLATFORM<br/>
                </h1>
                <h2>Grow Your Capital with {{ config('app.name') }}</h2>
                <p class="bounceInDown wow">
                    {{ config('app.name') }}, one of the safest online asset investment firm, <br>was
                    established
                    to provide intelligent portfolios with its<br> expert investors, customer-priority
                    approach,<br> safe and high-tech investment tools.
                </p>
                <div class="mb-3">
                    <a href="{{ route('register') }}">Open New Account</a>
                </div>
            </div>
            <!--end bannerright-->
        </div>
        <!--end bannerrbannerwrap-->

    </div>
@stop

@section('content')
    <center>
        <div class="content">
            <div class="statwrap">
                <div class="statbox">
                    <div class="icon zoomIn wow"><img src="{{asset('assets/images/staticon1.png')}}"
                                                      class="hvr-bob" alt=""/>
                    </div>
                    <h3 class="bounceInUp wow">Registered Address</h3>
                    <p class="bounceInUp wow">{{ config('app.address') }}</p>
                </div>
                <div class="statbox light">
                    <a href="{{asset('assets/images/certificate.png')}}"
                       target="_blank">
                        <div class="icon zoomIn wow">
                            <img src="{{asset('assets/images/staticon2.png')}}" class="hvr-bob" alt=""/>
                        </div>
                        <h3 class="bounceInUp wow">Our Certificate</h3>
                        <p class="bounceInUp wow">
                            <img src="{{asset('assets/images/cert.png')}}" height="100px" width="122px;">
                        </p>
                    </a>
                </div>
            </div>
        </div>
    </center>

    <div class="investmentwrap">
        <div class="content">
            <div class="planswrap">
                <h1>our <span>investment</span> plans</h1>
                <div class="planlistings">
                    <div class="col zoomIn wow">
                        <img src="{{asset('assets/images/plan1.png')}}" alt=""/>
                        <div class="plans-top">
                            <h2>5%<span>After 1 Day</span><a href="{{ 'register' }}">Deposit</a></h2>
                            <ul>
                                <li><i class="fas fa-check-circle"></i> Plan Terms: <span>1 Day</span></li>
                                <li><i class="fas fa-check-circle"></i> Min Deposit: $20</li>
                                <li><i class="fas fa-check-circle"></i> Max Deposit: $20,000</li>
                                <li><i class="fas fa-check-circle"></i> Instant Withdrawals</li>
                                <li><i class="fas fa-check-circle"></i> Principal Return</li>
                                <li><i class="fas fa-check-circle"></i> HUMAN MANAGED</li>
                            </ul>
                        </div>
                    </div>

                    <div class="col col2 zoomIn wow">
                        <img src="{{asset('assets/images/plan2.png')}}" alt=""/>
                        <div class="plans-top">
                            <h2>6%<span>Daily For 3 Days</span><a href="{{ route('register') }}">Deposit</a></h2>
                            <ul>
                                <li><i class="fas fa-check-circle"></i> Plan Terms: <span>3 Days</span></li>
                                <li><i class="fas fa-check-circle"></i> Min Deposit: $750</li>
                                <li><i class="fas fa-check-circle"></i> Max Deposit: $5,000</li>
                                <li><i class="fas fa-check-circle"></i> Instant Withdrawals</li>
                                <li><i class="fas fa-check-circle"></i> Principal Return</li>
                                <li><i class="fas fa-check-circle"></i> HUMAN MANAGED</li>
                            </ul>
                        </div>
                    </div>

                    <div class="col zoomIn wow">
                        <img src="{{asset('assets/images/plan3.png')}}" alt=""/>
                        <div class="plans-top">
                            <h2>7%<span>Daily For 5 Days</span><a href="{{ route('register') }}">Deposit</a></h2>
                            <ul>
                                <li><i class="fas fa-check-circle"></i> Plan Terms: <span>5 Days</span></li>
                                <li><i class="fas fa-check-circle"></i> Min Deposit: $1,500</li>
                                <li><i class="fas fa-check-circle"></i> Max Deposit: $10,000</li>
                                <li><i class="fas fa-check-circle"></i> Instant Withdrawals</li>
                                <li><i class="fas fa-check-circle"></i> Principal Return</li>
                                <li><i class="fas fa-check-circle"></i> AI AND HUMAN MANAGED</li>
                            </ul>
                        </div>
                    </div>

                    <div class="col col2 zoomIn wow">
                        <img src="{{asset('assets/images/plan4.png')}}" alt=""/>
                        <div class="plans-top">
                            <h2>10%<span>Daily For 7 Days</span><a href="{{ route('register') }}">Deposit</a></h2>
                            <ul>
                                <li><i class="fas fa-check-circle"></i> Plan Terms: <span>7 Days</span></li>
                                <li><i class="fas fa-check-circle"></i> Min Deposit: $5,000</li>
                                <li><i class="fas fa-check-circle"></i> Max Deposit: $15,000</li>
                                <li><i class="fas fa-check-circle"></i> Instant Withdrawals</li>
                                <li><i class="fas fa-check-circle"></i> Principal Return</li>
                                <li><i class="fas fa-check-circle"></i> AI AND HUMAN MANAGED</li>
                            </ul>
                        </div>
                    </div>

                    <div class="col zoomIn wow">
                        <img src="{{asset('assets/images/plan5.png')}}" alt=""/>
                        <div class="plans-top">
                            <h2>15%<span>Daily For 10 Days</span><a href="#">Deposit</a></h2>
                            <ul>
                                <li>Plan Terms: <span>10 Days</span></li>
                                <li>Min Deposit: $7,500</li>
                                <li>Max Deposit: $50,000</li>
                                <li>Instant Withdrawals</li>
                                <li>Principal Return</li>
                                <li>AI AND HUMAN MANAGED</li>
                            </ul>
                        </div>
                    </div>

                    <div class="col col2 zoomIn wow">
                        <img src="{{asset('assets/images/plan6.png')}}" alt=""/>
                        <div class="plans-top">
                            <h2>25%<span>Daily For 2 Days </span><a href="#">Deposit</a></h2>
                            <ul>
                                <li>Plan Terms: <span>2 Days</span></li>
                                <li>Min Deposit: $20,000</li>
                                <li>Max Deposit: $100,000</li>
                                <li>Instant Withdrawals</li>
                                <li>Principal Return</li>
                                <li>AI AND HUMAN MANAGED</li>
                            </ul>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>

    <div class="calswrap">
        <div class="content">

            <div class="calculator-container">
                <div class="calinputs bounceInDown wow">
                    <label for="amount">Investment Amount:</label>
                    <input type="text" name="amount" id="amount" class="deposit" value="20">
                </div>
                <div class="calresult bounceInDown wow">
                    <img src="{{asset('assets/images/calc-check.png')}}" alt=""/>
                    <h4 id="profitDaily">5</h4>
                    <p>daily profit</p>

                </div>
                <div class="calresult bounceInDown wow">
                    <img src="{{asset('assets/images/calc-check.png')}}" alt=""/>
                    <h4 id="profitWeekly">5</h4>
                    <p>weekly profit</p>

                </div>
                <div class="calresult bounceInDown wow">
                    <img src="{{asset('assets/images/calc-check.png')}}" alt=""/>
                    <h4 id="profitMonthly">5</h4>
                    <p>Monthly profit</p>
                </div>
            </div>
        </div>
    </div>

    <div class="welcomewrap">
        <div class="content">
            <div class="welcomeright zoomIn wow">
                <img src="{{asset('assets/images/welcomethumb.png')}}"
                     alt=""/>
            </div>
            <div class="welcocmeleft">
                <h1 class="fadeInRight wow">WELCOME TO <span>{{ config('app.name') }}</span></h1>
                <p>
                    With years of
                    successful experience {{ config('app.name') }} has built the the safest, most
                    efficient and convenient online investing platform centered on online assets trading.

                    We offer superb business solutions to each of our clients under transparent and completely
                    clear conditions. We give opportunities and implement them based on our investment program,
                    which takes your income to a completely new level.
                </p>
                <p>
                    The
                    investment team has a unique mixture of technology and operating expertise in the
                    distributed ledger systems as well as financial and capital markets experience – this unique
                    skill set allows for sophisticated technical and valuation analysis within the portfolio
                    construction process. With team located all around the world, {{ config('app.name') }} has
                    24-hour coverage of the constantly trading digital assets.
                </p>

                <p>{{ config('app.name') }}, is an officially registered company which gives its clients all required
                    guarantees, including confidentiality of data provided by clients at the registration procedure.
                    Apart from this we guarantee accrual of requested payments in due time and full amount. The
                    company's system, including support service, works 24 hours, which means you are always welcome
                    to contact our experts in case you have any questions.</p>
            </div>

        </div>
    </div>





    <div class="referralwrap">

        <div class="content">
            <div class="features-inner">
                <h2 class="fadeIn wow">{{ config('app.name') }} <span>Features</span></h2>
                <div class="featuresbox fadeInLeft wow">
                    <div class="features-content">
                        <div class="left hvr-forward"><img src="{{asset('assets/images/feat1.png')}}" alt=""/></div>
                        <div class="right">
                            <h4>24/7 Customer Support</h4>
                            <p>We provide 24/7 customer support through e-mail. Our support representatives are
                                always available to answer any questions. </p>
                        </div>
                    </div>
                </div>
                <div class="featuresbox fadeInDown wow">
                    <div class="features-content">
                        <div class="left hvr-forward"><img src="{{asset('assets/images/feat2.png')}}" alt=""/></div>
                        <div class="right">
                            <h4>Dedicated Server</h4>
                            <p>We use a dedicated server with the highest level of DDOS protection to ensure that
                                your funds are always safe with us. </p>
                        </div>
                    </div>
                </div>
                <div class="featuresbox fadeInRight wow">
                    <div class="features-content">
                        <div class="left hvr-forward"><img src="{{asset('assets/images/feat3.png')}}" alt=""/></div>
                        <div class="right">
                            <h4>EV SSL</h4>
                            <p>Our website is secured with 256-bit encryption from Comodo with Extended Validation
                                that verifies the authenticity of our company.</p>
                        </div>
                    </div>
                </div>
                <div class="featuresbox fadeInLeft wow">
                    <div class="features-content">
                        <div class="left hvr-forward"><img src="{{asset('assets/images/feat4.png')}}" alt=""/></div>
                        <div class="right">
                            <h4>United Kingdom Company</h4>
                            <p>Our company is legally registered in the UK as "{{ config('app.name') }}".</br>
                                Our Address: Suite-26, 95 Miles Road Miles Road, Mitcham, London, England, CR4 3FH
                            </p>
                        </div>
                    </div>
                </div>
                <div class="featuresbox fadeInUp wow">
                    <div class="features-content">
                        <div class="left hvr-forward"><img src="{{asset('assets/images/feat5.png')}}" alt=""/></div>
                        <div class="right">
                            <h4>Instant Withdrawals</h4>
                            <p>Our withdrawals are all processed instantly after they are requested. Minimum
                                withdrawal is only $0.01 through perfect money and payeer while through bitcoin and
                                crypto currencies is $3.</p>
                        </div>
                    </div>
                </div>
                <div class="featuresbox fadeInRight wow">
                    <div class="features-content">
                        <div class="left hvr-forward"><img src="{{asset('assets/images/feat6.png')}}" alt=""/></div>
                        <div class="right">
                            <h4>DDoS Protected System</h4>
                            <p>We use the highest level of protection. Our website can resists attacks of any size.
                            </p>
                        </div>
                    </div>
                </div>


            </div>
        </div>

        <div class="content" style="display: flex; justify-content: center; align-items: center">
            <div style="display: flex; flex-direction: column; align-items: center">
                <img src="{{asset('assets/images/aff-left.png')}}" alt=""/>
                <div class="trading-text" style="text-align: center">
                    <h2>5% Referral Bonus</h2>
                    <p style="text-align: center">We offer 5% referral bonus for each new member you invite to our program. After he makes
                        a deposit you receive a referral commission of 5%.</p>
                </div>
            </div>
        </div>
    </div>


    <div class="processors-container">
        <div class="content">
            <h2>We accept Various payment processors</h2>
            <img src="{{asset('assets/images/footer-payments.png')}}" alt=""/>

        </div>
    </div>
@stop
