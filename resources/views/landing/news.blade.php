@extends('layouts.landing-master')
@section('page-title', 'News')

@section('page-styles')

@stop
@section('extra-classes', 'insideheaders')
@section('extra-content')
    <div class="bannerwrap">

        <div class="content">

            <h1 class="bounceInDown wow">News &amp; <span>Events</span></h1>
        </div>
    </div>
@stop

@section('content')
    <div class="inside_wrap news">
        <div class="content">
            <table cellspacing=1 cellpadding=2 border=0 width=100%>
                <tr>
                    <td colspan=3 align=center>No news found</td>
                </tr>
            </table>
            <center>
                <ul class="pagination">
                    <li class="page-item"><a class="prev page-link disabled">&lt;&lt;</a></li>
                    <li class="page-item active"><a class="page-link">1</a></li>
                    <li class="page-item"><a class="next page-link disabled">&gt;&gt;</a></li>
                </ul>
            </center>


        </div>
    </div>
@stop
