@extends('layouts.landing-master')
@section('page-title', 'Registration completed')

@section('page-styles')
    <style>
        .btn-login {
            font-size: 15px;
            font-weight: 600;
            color: #0c0a05 !important;
            text-transform: uppercase;
            border-bottom: 2px solid transparent;
            transition: border-bottom-color 500ms ease-in-out;
        }
        .btn-login:hover {
            border-bottom-color: #0c0a05;
        }
        .loginwrappers {
            margin: 0 auto;
            display: flex;
            flex-direction: column;
            align-items: center;
            max-width: 700px;
            background: #F5DA3B;
            border-radius: 36px;
            box-shadow: 0 0 5px #00000096;
            padding: 30px 35px 35px 35px;
            overflow: hidden;
            color: #0c0a05;
        }
    </style>
@stop
@section('extra-classes', 'insideheaders')
@section('extra-content')
    <div class="bannerwrap">
        <div class="content">
            <h1 class="bounceInDown wow">Registration <span>completed</span></h1>
        </div>
    </div>
@stop

@section('content')
    <div class="loginpage">
        <div class="content">
            <div class="loginwrappers">
                <h2>Thank you for joining our program.</h2>
                <p>You are now an official member of this program. You can login to your account to start investing with
                    us and use all the services that are available for our members.</p>
                <p><b>Important:</b> Do not provide your login and password to anyone!</p>
                <p>You can proceed to <a href="{{ route('login') }}" class="btn-login">login</a> to your dashboard.</p>
            </div>
        </div>

    </div>
@stop
