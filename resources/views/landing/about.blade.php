@extends('layouts.landing-master')
@section('page-title', 'About us')

@section('page-styles')

@stop
@section('extra-classes', 'insideheaders')
@section('extra-content')
    <div class="bannerwrap">

        <div class="content">

            <h1 class="bounceInDown wow">About <span>{{ config('app.name') }}</span></h1>
        </div>
    </div>
@stop

@section('content')
    <div class="welcomewrap">
        <div class="content">
            <div class="welcomeright zoomIn wow"><img src="{{asset('assets/images/welcomethumb.png')}}"
                                                      alt=""/></div>
            <div class="welcocmeleft">
                <h1 class="fadeInRight wow">WELCOME TO <span>{{ config('app.name') }}</span></h1>
                <p>
                    With years of
                    successful experience {{ config('app.name') }} has built the the safest, most
                    efficient and convenient online investing platform centered on online assets trading.

                    We offer superb business solutions to each of our clients under transparent and completely
                    clear conditions. We give opportunities and implement them based on our investment program,
                    which takes your income to a completely new level.
                </p>
                <p>
                    The
                    investment team has a unique mixture of technology and operating expertise in the
                    distributed ledger systems as well as financial and capital markets experience – this unique
                    skill set allows for sophisticated technical and valuation analysis within the portfolio
                    construction process. With team located all around the world, {{ config('app.name') }} has
                    24-hour coverage of the constantly trading digital assets.
                </p>

                <p>{{ config('app.name') }}, is an officially registered company which gives its clients all required
                    guarantees, including confidentiality of data provided by clients at the registration procedure.
                    Apart from this we guarantee accrual of requested payments in due time and full amount. The
                    company's system, including support service, works 24 hours, which means you are always welcome
                    to contact our experts in case you have any questions.</p>
            </div>

        </div>
    </div>
    <div class="referralwrap">

        <div class="content">
            <div class="features-inner">
                <h2 class="fadeIn wow">{{ config('app. name') }} <span>Features</span></h2>
                <div class="featuresbox fadeInLeft wow">
                    <div class="features-content">
                        <div class="left hvr-forward"><img src="{{asset('assets/images/feat1.png')}}" alt=""/>
                        </div>
                        <div class="right">
                            <h4>24/7 Customer Support</h4>
                            <p>We provide 24/7 customer support through e-mail. Our support representatives are
                                always available to answer any questions. </p>
                        </div>
                    </div>
                </div>
                <div class="featuresbox fadeInDown wow">
                    <div class="features-content">
                        <div class="left hvr-forward"><img src="{{asset('assets/images/feat2.png')}}" alt=""/>
                        </div>
                        <div class="right">
                            <h4>Dedicated Server</h4>
                            <p>We use a dedicated server with the highest level of DDOS protection to ensure that
                                your funds are always safe with us. </p>
                        </div>
                    </div>
                </div>
                <div class="featuresbox fadeInRight wow">
                    <div class="features-content">
                        <div class="left hvr-forward"><img src="{{asset('assets/images/feat3.png')}}" alt=""/>
                        </div>
                        <div class="right">
                            <h4>EV SSL</h4>
                            <p>Our website is secured with 256-bit encryption from Comodo with Extended Validation
                                that verifies the authenticity of our company.</p>
                        </div>
                    </div>
                </div>
                <div class="featuresbox fadeInLeft wow">
                    <div class="features-content">
                        <div class="left hvr-forward"><img src="{{asset('assets/images/feat4.png')}}" alt=""/>
                        </div>
                        <div class="right">
                            <h4>United Kingdom Company</h4>
                            <p>Our company is legally registered in the United Kingdom as
                                <span style="text-transform: uppercase">"{{ config('app.name') }}"</span>.
                            </p>
                        </div>
                    </div>
                </div>
                <div class="featuresbox fadeInUp wow">
                    <div class="features-content">
                        <div class="left hvr-forward"><img src="{{asset('assets/images/feat5.png')}}" alt=""/>
                        </div>
                        <div class="right">
                            <h4>Instant Withdrawals</h4>
                            <p>Our withdrawals are all processed instantly after they are requested. Minimum
                                withdrawal is only $3 with crypto currencies and $0.01 with payeer and perfect
                                Money.</p>
                        </div>
                    </div>
                </div>
                <div class="featuresbox fadeInRight wow">
                    <div class="features-content">
                        <div class="left hvr-forward"><img src="{{asset('assets/images/feat6.png')}}" alt=""/>
                        </div>
                        <div class="right">
                            <h4>DDoS Protected System</h4>
                            <p>We use the highest level of protection. Our website can resists attacks of any size.
                            </p>
                        </div>
                    </div>
                </div>


            </div>
        </div>
    </div>
@stop
