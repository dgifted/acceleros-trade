@extends('layouts.landing-master')
@section('page-title', 'Affiliates')

@section('page-styles')

@stop
@section('extra-classes', 'insideheaders')
@section('extra-content')
    <div class="bannerwrap">
        <div class="content">

            <h1 class="bounceInDown wow">Referral <span>Program</span></h1>
        </div>
    </div>
@stop

@section('content')
    <div class="inside_wrap affiliatepage">
        <div class="content">
            <p class="headinfo">Even if you do not make a deposit to our program, it is still possible to earn with
                {{ config('app.name') }}! You can still take part in our referral program which offers a 2-level
                referral commission of 5% - 1%. That means you earn 5% of the deposit for any member who joins us
                through your referral link. If that member refers a new member who makes a deposit, you will earn 1%
                of that new member's deposit. Your referral commission is always immediately credited to your
                account and can be withdrawn instantly! </p>
            <div class="affbox left">

                <div class="affleft">
                    <h3>HOW TO START EARNING</h3>
                    <p>To start referring members with our referral program, simply register a new account by
                        clicking on the "Register" link. After signing up, log in to your member's area and you will
                        see your referral link with your username. Use this link to promote our program and start
                        earning referral commission!</p>
                </div>
                <div class="affright"><img src="{{asset('assets/client-4/images/affith1.png')}}" alt=""/></div>
            </div>
            <div class="affbox right">
                <div class="affleft">
                    <h3>REFERRAL TOOLS</h3>
                    <p>We give you the tools you need to advertise our program! To use our banner links, log in to
                        your account and click "Banners" on the user menu. There you will find banners of different
                        size, with code underneath. All of the banner codes already have your referral link
                        included. Use these banners to promote our program, and you will earn a referral bonus from
                        any member who makes a deposit!</p>
                </div>
                <div class="affright"><img src="{{asset('assets/client-4/images/affith2.png')}}" alt=""/></div>
            </div>
        </div>
    </div>


    <div class="referralwrap">
        <div class="content">
            <div class="trading-wrap">
                <div class="trading-left">
                    <img src="{{asset('assets/client-4/images/aff-left.png')}}" alt=""/>
                    <div class="trading-text">
                        <h2>5% Referral Bonus</h2>
                        <h4>Level 1 Referral Commission</h4>
                        <p>We offer 5% referral bonus for each new member you invite to our program. After he makes
                            a deposit you receive a referral commission of 5%.</p>
                    </div>
                </div>
                <div class="trading-right">
                    <img src="{{asset('assets/client-4/images/aff-right.png')}}" alt=""/>
                    <div class="trading-text">
                        <h2>1% Referral Bonus</h2>
                        <h4>Level 2 Referral Commission</h4>
                        <p>We offer an extra bonus of 1% for any new member who is invited by your referral. After
                            he makes a deposit you receive a referral commission of 1%.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
