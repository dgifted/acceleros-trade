@extends('layouts.landing-master')
@section('page-title', 'Contact Us')

@section('page-styles')

@stop
@section('extra-classes', 'insideheaders')
@section('extra-content')
    <div class="bannerwrap">

        <div class="content">

            <h1 class="bounceInDown wow">Contact <span>Us</span></h1>
        </div>
    </div>
@stop

@section('content')
    <div class="inside_wrap support">
        <div class="content">
            <div class="supportleft">
                <h2>Do You have any questions?</h2>
                <p>We strongly recommend that you start searching for the necessary information in the FAQ section. If
                    you
                    need advice or technical assistance, do not hesitate to contact our specialists. Customer support is
                    available around the clock. You just need to send a letter or a request via the feedback form to
                    promptly
                    receive the necessary assistance. Remember that the more detailed the description of your problem
                    is, the
                    sooner it will be solved. Do not forget to indicate your login, if you are a registered user of the
                    website.
                </p>
                <h3>E-mail us at <a href="mailto:{{ config('mail.support') }}"><span>Our email address</span></a></h3>
            </div>
            <div class="supportright">
                <form method=post name=mainform onsubmit="javascript.void(0)">
                    <div class="form-block">
                        <input type="text" name="name" value="" size="30" class="inpts"
                               placeholder="Your Name"/></div>
                    <div class="form-block">
                        <input type="text" name="email" value="" size="30" class="inpts"
                               placeholder="Your Email"/></div>
                    <div class="form-block">
                        <textarea name=message class=inpts cols=45 rows=4 placeholder="Your Message"></textarea>
                    </div>
                    <div class="form-block">
                        <input type=submit value="Send" class=sbmt>
                    </div>
                </form>
            </div>
        </div>
    </div>
@stop
