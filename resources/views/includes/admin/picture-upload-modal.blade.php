{{--Profile picture upload modal--}}
<div class="modal fade" id="picture-modal" role="dialog" aria-labelledby="Profile picture upload">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form action="#" enctype="multipart/form-data" id="form-profile" novalidate>
                <div class="modal-header bg-primary">
                    <h4 class="modal-title m-0 p-0">Profile picture upload</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <h3 class="text-sm">Choose picture file. Accept file types: 'jpeg', 'jpg', 'png'.</h3>
{{--                    <div id="file_path" style="display: none;"></div>--}}
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="inputGroupFileAddon01">Upload</span>
                        </div>
                        <div class="custom-file">
                            <input type="file" name="file" class="custom-file-input" id="inputGroupFile01" aria-describedby="inputGroupFileAddon01" required>
                            <label class="custom-file-label" for="inputGroupFile01">Choose file</label>
                        </div>
                    </div>
                    <div style="width: 80px; height: auto; overflow: hidden;" id="photoPreview" class="d-none">
                        <img src="" alt="" style="width: 80px; height: auto" id="imgElem">
                    </div>
                </div>
                <div class="modal-footer justify-content-end">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" id="upload">
                        <i class="fas fa-upload"></i>
                        Upload
                    </button>
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
{{--<!-- /.modal -->--}}
