<div class="modal fade" id="modal-confirm" tabindex="-1" role="dialog" aria-labelledby="confirm">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <div class="modal-header bg-danger">
                <h4 class="modal-title">Delete <span id="modalTitle"></span></h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" style="display:flex; align-items: center; flex-direction: column; justify-content: space-between">
                <i class="fas fa-exclamation-triangle text-danger mb-2" style="font-size: 3rem;"></i>
                <h3 class="text-center">Are sure you want to delete <span id="model"></span>?</h3>
                <p class="text-bold">Action is not reversible.</p>
            </div>
            <div class="modal-footer justify-content-end">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <a id="action" type="button" class="btn btn-danger text-white"><i class="fas fa-trash mr-2"></i>Delete</a>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
