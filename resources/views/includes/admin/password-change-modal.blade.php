{{--Pasdword update modal--}}
<div class="modal fade" id="password-modal" role="dialog" aria-labelledby="Password update">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form action="#" enctype="multipart/form-data" id="form-profile" novalidate>
                <div class="modal-header">
                    <h4 class="modal-title">Password update</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <h3 class="text-md">Change admin password</h3>
                    <div class="input-group mb-2">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="inputGroupPrepend1">New password</span>
                        </div>
                        <input type="password" name="password" class="form-control" id="new_password" aria-describedby="inputGroupPrepend2" required>
                    </div>
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="inputGroupPrepend2">Confirm password</span>
                        </div>
                        <input type="password" name="password_confirmation" class="form-control" id="confirm_password" aria-describedby="inputGroupPrepend2" required>
                    </div>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" id="update">Update</button>
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
{{--<!-- /.modal -->--}}
