<!-- Main Footer -->
<footer class="main-footer">
    <!-- To the right -->
    <div class="float-right d-none d-sm-inline">
        Powered by <a href="mailto:j.swiftlaunchme@gmail.com">Swift Launch Nig</a>
    </div>
    <!-- Default to the left -->
    <strong>Copyright &copy; 2012-{{ now()->year }} <a href="{{ route('admin.home') }}" onclick="(function(e){e.preventDefault();})(event)">{{config('app.name')}} &trade;</a>.</strong> All rights reserved.
</footer>
