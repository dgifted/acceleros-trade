<?php $route = Route::currentRouteName(); ?>
<!-- Main Sidebar Container -->
<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="{{route('admin.home')}}" class="brand-link">
        <span class="brand-text font-weight-light">
            <img src="{{ asset('assets/images/logo.png') }}" alt="" style="width: 100%">
        </span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">

        <!-- Sidebar Menu -->
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                <!-- Add icons to the links using the .nav-icon class
                     with font-awesome or any other icon font library -->
                <li class="nav-item">
                    <a href="{{route('admin.home')}}" class="nav-link {{
                    $route == 'admin.home' ? 'active' : '' }}">
                        <i class="nav-icon fas fa-th"></i>
                        <p>
                            Dashboard
                            {{-- <i class="right fas fa-angle-left"></i> --}}
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{route('admin.sent-emails')}}" class="nav-link {{
                        $route == 'admin.sent-emails'
                        || $route === 'admin.send-new-email' ? 'active' : '' }}">
                        <i class="nav-icon fas fa-envelope"></i>
                        <p>
                            Email
                             <i class="right badge badge-info">New</i>
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ route('admin.view-customers') }}" class="nav-link {{
                    $route == 'admin.view-customers' ||
                    $route == 'admin.view-customer-details' ? 'active' : '' }}">
                        <i class="nav-icon fas fa-user-cog"></i>
                        <p>
                            User Management
                            {{-- <span class="right badge badge-danger">New</span> --}}
                        </p>
                    </a>
                </li>
                <li class="nav-item has-treeview {{
                $route == 'admin.view-deposit-details'
                || $route == 'admin.view-deposits'
                || $route == 'admin.view-withdrawal-details'
                || $route == 'admin.view-withdrawals' ? 'menu-open' : ''}}">
                    <a href="#" class="nav-link {{
                $route == 'admin.view-deposit-details'
                || $route == 'admin.view-deposits'
                || $route == 'admin.view-withdrawal-details'
                || $route == 'admin.view-withdrawals'
                    ? 'active' : '' }}">
                        <i class="nav-icon fas fa-money-check"></i>
                        <p>
                            Transactions
                            <i class="fas fa-angle-left right"></i>
                            {{-- <span class="right badge badge-danger">New</span> --}}
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item ml-4">
                            <a href="{{ route('admin.view-deposits') }}" class="nav-link
                        {{$route == 'admin.view-deposit-details'
                         || $route == 'admin.view-deposits' ? 'active' : ''}}">
                                <i class="fas fa-suitcase nav-icon"></i>
                                <p>Deposits</p>
                            </a>
                        </li>
                        <li class="nav-item ml-4">
                            <a href="{{ route('admin.view-withdrawals') }}" class="nav-link
                        {{ $route == 'admin.view-withdrawal-details'
                         || $route == 'admin.view-withdrawals' ? 'active' : ''}}">
                                <i class="fas fa-money-check-alt nav-icon"></i>
                                <p>Withdrawals</p>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="nav-item has-treeview {{$route == 'admin.settings'
                || $route == 'admin.profile'
                || $route == 'admin.payment-channel-details'
                || $route == 'admin.view-payment-channels'
                || $route == 'admin.plan-details'
                || $route == 'admin.view-plans' ?'menu-open' : ''}}">
                    <a href="#" class="nav-link {{$route == 'admin.settings'
                || $route == 'admin.profile'
                || $route == 'admin.payment-channel-details'
                || $route == 'admin.view-payment-channels'
                || $route == 'admin.plan-details'
                || $route == 'admin.view-plans' ? 'active' : ''}}">
                        <i class="nav-icon fas fa-cog"></i>
                        <p>
                            Settings
                            <i class="fas fa-angle-left right"></i>
                            {{-- <span class="right badge badge-danger">New</span> --}}
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item ml-4">
                            <a href="{{ route('admin.view-plans') }}" class="nav-link {{$route == 'admin.plan_add'
                             || $route == 'admin.plan-details'
                             || $route == 'admin.view-plans' ? 'active' : ''}}">
                                {{-- <i class="far fa-circle nav-icon"></i> --}}
                                <i class="fas fa-suitcase nav-icon"></i>
                                <p>Plans</p>
                            </a>
                        </li>
                        <li class="nav-item ml-4">
                            <a href="{{ route('admin.view-payment-channels') }}" class="nav-link
                        {{ $route == 'admin.view-payment-channels' || $route == 'admin.payment-channel-details' ? 'active' : ''}}">
                                <i class="fas fa-wallet nav-icon"></i>
                                <p>Payment channels</p>
                            </a>
                        </li>
                        <li class="nav-item ml-4">
                            <a href="javascript:void(0)" class="nav-link">
                                <i class="fas fa-blog nav-icon"></i>
                                <p>News</p>
                            </a>
                        </li>
                        @if (env('APP_ENV') == 'local')
                            <li class="nav-item ml-4">
                                <a href="javascript:void(0)" class="nav-link disabled">
                                    <i class="far fa-share-square nav-icon"></i>
                                    <p>Content seeding</p>
                                </a>
                            </li>
                        @endif
                    </ul>
                </li>
            </ul>
        </nav>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>
