<div class="plan-item">
    <h2>@yield('percentage')% @yield('description')</h2>
    <table class="plan-table">
        <thead>
        <tr>
            <th>Plan</th>
            <th>Capital ($)</th>
            <th>Profit(%)</th>
        </tr>
        </thead>

        <tbody>
        <tr>
            <td>@yield('title')</td>
            <td>$ @yield('min') - $ @yield('max')</td>
            <td>@yield('percentage')</td>
        </tr>
        </tbody>
    </table>
    <button type="button" data-amount-min="@yield('min')" data-amount-max="@yield('max')" class="cal-btn">Calculate profit</button>
</div>
