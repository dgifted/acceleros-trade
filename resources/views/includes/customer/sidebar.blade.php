@php
    $route = \Route::currentRouteName()
@endphp

<div class="membersidebar">
    <div class="welcome">
        <span class="icon">
            <img src="{{asset('assets/images/membersidebar1.png')}}" alt="">
        </span>

        <h2>Welcome, <span class="text-capitalize">{{ \Auth()->user()->username }}</span></h2>
    </div>

    <div class="navbar">
        <div class="content">
            <ul class="navigation">
                <li>
                    <a href="{{ route('customer.home') }}" class="{{ $route === 'customer.home' ? 'active' : '' }}">
                        Dashboard
                    </a>
                </li>
                <li>
                    <a href="{{ route('bonuses.all') }}" class="{{ $route === 'bonuses.all'
                    || $route === 'bonus-withdraw-show'
                    || $route === 'bonus-reinvest-show'
                     ? 'active' : '' }}">
                        Bonuses
                    </a>
                </li>
                <li>
                    <a href="{{ route('deposit.create') }}" class="{{ $route === 'deposit.create' ? 'active' : '' }}">
                        Deposit
                    </a>
                </li>
                <li>
                    <a href="{{ route('deposit.all') }}" class="{{ $route === 'deposit.all' ? 'active' : '' }}">
                        Deposits History
                    </a>
                </li>
                <li>
                    <a href="{{ route('withdrawal.create') }}"
                       class="{{ $route === 'withdrawal.create' ? 'active' : '' }}">
                        Withdraw
                    </a>
                </li>
                <li>
                    <a href="{{ route('account.history') }}" class="{{ $route === 'account.history' ? 'active' : '' }}">Account
                        History</a>
                </li>
                <li>
                    <a href="{{ route('referrals.all') }}" class="{{ $route === 'referrals.all' ? 'active' : ''}}">
                        Referrals
                    </a>
                </li>
                <li>
                    <a href="{{ route('account.home') }}"
                       class="{{ $route === 'account.home' ? 'active' : ''}}">Account</a>
                </li>
            </ul>
        </div>
    </div>
</div>
