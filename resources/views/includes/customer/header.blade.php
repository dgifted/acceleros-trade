<div class="headerwrap @yield('extra-classes')">
    <div class="logowrap">
        <div class="content">
            <div class="logo fadeInLeft wow">
                <a href="{{ route('home') }}">
                    <img src="{{asset('assets/images/logo.png')}}" alt=""/>
                </a>
            </div>
            <div class="navwrap fadeInRight wow">
                <div class="dropdown">
                    <div id="google_translate_element" style="width:10px;"></div>
                    <script type="text/javascript">
                        function googleTranslateElementInit() {
                            new google.translate.TranslateElement({pageLanguage: 'en'}, 'google_translate_element');
                        }
                    </script>


                    <script type="text/javascript"
                            src="{{url('http://translate.google.com/translate_a/element.js?cb=googleTranslateElementInit')}}"></script>

                </div>
                <ul>
                    <li class="login"><a href="{{ route('account.home') }}">Profile</a></li>
                    <li class="signup">
                        <a href="{{ route('logout') }}"
                           onclick="event.preventDefault(); document.getElementById('logout-form').submit();">logout</a>
                        <form id="logout-form" action="{{ route('logout') }}" method="post" style="display: none">
                            @csrf
                        </form>
                    </li>

                </ul>
            </div>
        </div>

    </div>
    @yield('extra-content')
</div>
