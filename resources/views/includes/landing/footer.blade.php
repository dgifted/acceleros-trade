<div class="footerwrap">
    <div class="content">
        <div class="copyright">
            <p>&copy; 2012 - {{ now()->year }}. {{ config('app.name') }} All Rights Reserved</p>


        </div>
        <div class="footerlinks">
            <p>
                <a href="{{ route('home') }}">HOME</a>
                |<a href="{{ route('about') }}">ABOUT US</a>
                |<a href="{{ route('contact') }}">CONTACT US</a>
                |<a href="{{ route('terms-policy') }}">TERMS &amp; CONDITIONS</a>
            </p>
        </div>
    </div>
</div>
