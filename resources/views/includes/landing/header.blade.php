@php
    $route = \Route::currentRouteName();
@endphp

<div class="headerwrap @yield('extra-classes')">
    <div class="logowrap">
        <div class="content">
            <div class="logo fadeInLeft wow">
                <a href="{{ route('home') }}">
                    <img src="{{asset('assets/images/logo.png')}}" alt=""/>
                </a>
            </div>
            <div class="navwrap fadeInRight wow">
                <div class="dropdown">
                    <div id="google_translate_element" style="width:10px;"></div>
                    <script type="text/javascript">
                        function googleTranslateElementInit() {
                            new google.translate.TranslateElement({pageLanguage: 'en'}, 'google_translate_element');
                        }
                    </script>
                    <script type="text/javascript"
                            src="{{url('http://translate.google.com/translate_a/element.js?cb=googleTranslateElementInit')}}"></script>

                </div>
                <ul>
                    <li><a href="{{ route('home') }}">home</a></li>
                    <li><a href="{{ route('about') }}">about</a></li>
                    <li><a href="{{ route('faqs') }}">faq</a></li>
                    <li><a href="{{ route('news') }}">news</a></li>
                    <li><a href="{{ route('affiliate') }}">affiliates</a></li>
                    <li><a href="{{ route('contact') }}">contact us</a></li>
                    <li class="login"><a href="{{ route('login') }}">login</a></li>
                    <li class="signup"><a href="{{ route('register') }}">signup</a></li>

                </ul>
            </div>
        </div>

    </div>
    @yield('extra-content')
</div>
