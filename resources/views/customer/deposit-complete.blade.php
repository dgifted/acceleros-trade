@extends('layouts.customer-master')
@section('page-title', 'Deposit')

@section('page-styles')
    <style>
        .info-row {
            width: 100%;
        }

        .info-row .label {
            width: 150px;
            margin-right: 1rem;
            display: inline-block;
            text-align: left;
            font-size: 1.2rem !important;
        }
    </style>
@stop
@section('extra-classes', 'insideheaders')
@section('extra-content')
    <div class="bannerwrap">
        <div class="content">
            <h1 class="bounceInDown wow">My <span>Dashboard</span></h1>
        </div>
    </div>
@stop

@section('content')
    <div class="myaccount_wrap">
        <div class="my_accont">
            <h2>Transaction summary</h2>
            <div class="info-row">
                <b class="label">Plan</b> <span>{{ $plan->title }}</span>
            </div>
            <div class="info-row">
                <b class="label">Profit</b> <span>{{ $plan->description }}</span>
            </div>
            <div class="info-row">
                <b class="label">Principal return</b> <span>yes</span>
            </div>
            <div class="info-row">
                <b class="label">Principal Withdraw:</b> <span>yes</span>
            </div>
            <div class="info-row">
                <b class="label">Credit Amount:</b> <span>${{ $deposit->amount }}</span>
            </div>
            <div class="info-row">
                <b class="label">Deposit Fee:</b> <span>$0.00</span>
            </div>
            <div class="info-row">
                <b class="label">Debit Amount:</b> <span>${{ $deposit->amount }}</span>
            </div>
            <div class="info-row">
                <b class="label">{{ $channel->abbreviation }} Debit Amount:</b> <span>${{ $deposit->amount }}</span>
            </div>
            <div class="info-row">
                <b class="label">Order status:</b>
                <span>
                    @switch($deposit->status)
                        @case(\App\Models\Deposit::STATUS_PENDING)
                        <i class="text-warning">Awaiting payment</i>
                        @break
                        @case(\App\Models\Deposit::STATUS_APPROVED)
                        <i class="text-success">Payment confirmed</i>
                        @break
                        @case(\App\Models\Deposit::STATUS_CANCELLED)
                        <i class="text-danger">Payment cancelled</i>
                        @break
                        @default
                        <i class="text-danger">Payment failed</i>
                    @endswitch
                </span>
            </div>
            <hr/>
            <br/>

            @if($deposit->status === \App\Models\Deposit::STATUS_PENDING)
                <p>
                    Please send <b>
                        @if(!is_null($deposit->currency_equivalent))
                            {{$deposit->currency_equivalent}} {{ $channel->abbreviation }}
                        @else
                            ${{$deposit->amount}}.00
                        @endif
                    </b> to <b class="color: #fbbc14">{{ $channel->wallet }}</b>
                </p>
                <div>
                    <p>Scan qrcode for payment</p>
                    <div style="width: 200px; background: white; padding: 0">
                        <div id="qrcode"></div>
                    </div>

                </div>
            @endif
        </div>
    </div>
@stop

@section('page-scripts')
    <script src="{{ asset('assets/jquery-qrcode.min.js') }}"></script>
    <script>
        var walletAddress = "{{ $channel->wallet }}";

        $("#qrcode").qrcode({
            minVersion: 1,
            maxVersion: 40,
            ecLevel: "L",
            fill: "#000",
            background: null,
            radius: 0,
            quiet: 0,
            mode: 0,
            size: 200,
            text: walletAddress
        });
    </script>
@endsection
