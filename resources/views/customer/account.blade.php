@extends('layouts.customer-master')
@section('page-title', 'Account profile')

@section('page-styles')
    <style>
        .stat-row {
            display: flex;
            justify-items: flex-start;
            align-items: center;
            margin-bottom: 10px;
        }

        .stat-row .label {
            display: inline-block;
            width: 160px;
            font-weight: bold;
            margin-right: 0.3rem;
            font-size: 1.3rem;
            text-align: left;
        }

        .stat-row .label + span {
            width: 150px;
            display: inline-block;
            text-align: left;
        }

        .stat-row input {
            max-width: 400px;
        }
    </style>
@stop
@section('extra-classes', 'insideheaders')
@section('extra-content')
    <div class="bannerwrap">
        <div class="content">
            <h1 class="bounceInDown wow">My <span>Account</span></h1>
        </div>
    </div>
@stop

@section('content')
    <div class="myaccount_wrap">
        @if(session('message'))
            <div class="alert alert-info" role="alert">
                {{ session('message') }}
            </div>
        @endif

        <form id="account-form" method="post" action="{{ route('account.home') }}">
            @csrf

            <div class="stat-row">
                <span class="label">Email: </span>
                <span>{{ $user->email }}</span>
            </div>
            <div class="stat-row">
                <span class="label">Username: </span>
                <span>{{ $user->username }}</span>
            </div>
            <div class="stat-row">
                <span class="label">Full name: </span>
                <input type="text"
                       name="name"
                       class="inpts"
                       value="{{ $user->name  }}"
                />
            </div>
            @foreach($channels as $channel)
                <div class="stat-row">
                    <span class="label">{{$channel->name}}: </span>
                    <input type="text"
                           name="{{ $channel->slug }}"
                           class="inpts"
                           value="{{ $channel->customerWallet ?? ''}}"
                    />
                </div>
            @endforeach

            <div class="stat-row">
                <span class="label">&nbsp;</span>
                <button type="submit">Update account detail</button>
            </div>
        </form>
    </div>
@stop
