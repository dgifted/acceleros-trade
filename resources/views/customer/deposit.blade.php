@extends('layouts.customer-master')
@section('page-title', 'Deposit')

@section('page-styles')

@stop
@section('extra-classes', 'insideheaders')
@section('extra-content')
    <div class="bannerwrap">
        <div class="content">
            <h1 class="bounceInDown wow">Investment <span>Deposit</span></h1>
        </div>
    </div>
@stop

@section('content')
    <div class="myaccount_wrap">
        <div class="my_accont">

            <h2>Choose a plan</h2>
            @if (session('message'))
                <div style="width: 100%; display: flex; align-items: center; justify-content: center">
                    <div class="alert alert-{{ session('type') }}" role="alert">
                        {{ session('message') }}
                    </div>
                </div>
            @endif

            <div style="width: 100%; display: flex; align-items: center; justify-content: center">
                @if(session('errors'))
                    <div>
                        @foreach($errors as $msg)
                            <p class="error">{{ $msg }}</p>
                        @endforeach
                    </div>
                @endif
            </div>

            <form method="post" action="{{ route('deposit.create') }}" id="deposit-form">
                @csrf
                @php
                    $count = 0;
                @endphp

                @foreach($plans as $plan)
                    @php
                        $count++
                    @endphp

                    <div class="plan-item">
                        <h2><input type="radio" name="plan" value="{{ $plan->id }}" class="plan"
                                   data-amount="{{ $plan->min }}" @if($count === 1) checked @endif>
                            {{ $plan->description }}
                        </h2>
                        <table class="plan-table">
                            <thead>
                            <tr>
                                <th>Plan</th>
                                <th>Capital ($)</th>
                                <th>Profit(%)</th>
                            </tr>
                            </thead>

                            <tbody>
                            <tr>
                                <td>{{ $plan->title}}</td>
                                <td>${{ $plan->min }} - ${{ $plan->max }}</td>
                                <td>{{ $plan->percentage }}</td>
                            </tr>
                            </tbody>
                        </table>
{{--                        <button type="button" data-amount-min="{{ $plan->min }}" data-amount-max="{{ $plan->max }}" class="cal-btn">--}}
{{--                            Calculate profit >>--}}
{{--                        </button>--}}
                    </div>
                @endforeach
                <br>

                <div style="display: flex; align-items: center">
                    <span style="margin-right: 3rem">Amount to invest: </span>
                    <input type="text" class="inpts" value="20"
                           name="amount"
                           id="amount"
                           style="max-width: 100px"
                    />
                    <br/>@error('amount') <p class="error">{{ $message }}</p> @enderror
                </div>

                <hr>
                <div style="display: flex; flex-direction: column; padding: 0 2rem">
                    @foreach($channels as $channel)
                        <label for="{{ $channel->slug }}">
                            <input type="radio"
                                   id="{{ $channel->slug }}"
                                   name="channel"
                                   value="{{ $channel->id }}"
                                   style="margin-right: 1.5rem"
                                   @if($channel->is_default == \App\Models\PaymentChannel::IS_DEFAULT) checked @endif
                                   required
                            />
                            Fund through {{ $channel->name }}
                        </label>
                    @endforeach
                </div>
                <hr>
                <button class="sbmt" type="submit" id="submit">Invest</button>
            </form>
        </div>
    </div>
@stop

@section('page-scripts')
    <script src="{{ asset('assets/sweetalert2.all.min.js') }}"></script>

    <script>
        var allPlans = document.querySelectorAll('input.plan');
        var amountDisplayInput = document.getElementById('amount');
        amountDisplayInput.value = allPlans[0].dataset.amount;

        for (var i = 0; i < allPlans.length; i++) {

            allPlans[i].addEventListener('click', (evt) => {
                amountDisplayInput.value = evt.target.dataset.amount;
            });
        }

        var form = document.getElementById('deposit-form');

        form.addEventListener('submit', (evt) => {
            var amount = Number(amountDisplayInput.value);
            var planMinAmount = 0;
            allPlans.forEach((item) => {
                if (item.checked)
                    planMinAmount = Number(item.dataset.amount);
            });

            if (amount < planMinAmount || amount <= 0) {
                Swal.fire({
                    title: 'Sorry!',
                    icon: 'error',
                    text: 'Investment amount cannot be less than the plan minimum capital.'
                });

                evt.preventDefault();
            }
        });
    </script>
@endsection
