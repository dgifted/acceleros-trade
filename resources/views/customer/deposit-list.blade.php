@extends('layouts.customer-master')
@section('page-title', 'My Deposits')

@section('page-styles')

@stop
@section('extra-classes', 'insideheaders')
@section('extra-content')
    <div class="bannerwrap">
        <div class="content">
            <h1 class="bounceInDown wow">Deposit <span>History</span></h1>
        </div>
    </div>
@stop

@section('content')
    <div class="myaccount_wrap">
        <div class="my_accont">
            <h2>Total Deposits: <span class="num-format">{{ $balance }}</span></h2>

            @foreach($plansWithDeposit as $plan)
                <table cellspacing="1" cellpadding="2" border="0" width="100%" class="line">
                    <tbody>
                    <tr>
                        <td class="item">
                            <table cellspacing="1" cellpadding="2" border="0" width="100%">
                                <tbody>
                                <tr>
                                    <td colspan="3" align="center"><b>5% After 1 Day</b></td>
                                </tr>
                                <tr>
                                    <td class="inheader">Plan</td>
                                    <td class="inheader" width="200">Deposit Amount</td>
                                    <td class="inheader" width="100" nowrap>Profit (%)</td>
                                </tr>
                                <tr>
                                    <td class="item">{{ $plan->title }}</td>
                                    <td class="item" align="right">$<span class="num-format">{{ $plan->min }}</span> - $<span
                                            class="num-format">{{ $plan->max}}</span></td>
                                    <td class="item" align="right"><span
                                            class="num-format">{{ $plan->percentage }}</span></td>
                                </tr>
                                </tbody>
                            </table>

                            <div class="my_accont">
                                <br>
                                <table cellspacing="1" cellpadding="2" border="0" width="100%">
                                    <tbody>
                                    <tr>
                                        <td colspan="4">
                                            @if($plan->subscribedTo === 1)

                                                <b class="text-success">Active deposit of $<span class="num-format">
                                                        {{ $plan->depositedAmount }}
                                                        @if($plan->withdrawan)
                                                            [withdrawn]
                                                            @endif
                                                    </span>
                                                </b>
                                            @else
                                                <b>No deposits on this plan</b>
                                            @endif
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                                <br>
                            </div>
                        </td>
                    </tr>
                    </tbody>
                </table>
            @endforeach
        </div>
    </div>
@stop
