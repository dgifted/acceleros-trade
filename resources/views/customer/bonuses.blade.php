@extends('layouts.customer-master')
@section('page-title', 'Bonuses')

@section('page-styles')
    <style>
        .bonus-row {
            display: flex;
            flex-flow: row wrap;
            align-items: start;
            min-height: 70vh;
        }

        .bonus-card {
            display: flex;
            flex-direction: column;
            align-items: center;
            width: 250px;
            background-color: #1e2738;
            padding: 10px 15px 20px;
            border-radius: 5px;
            margin-right: 1rem;
            min-height: 160px !important;
            justify-content: space-between;
        }

        .bonus-card .title {
            font-size: 2.5rem;
            font-weight: bold;
        }

    </style>
@stop
@section('extra-classes', 'insideheaders')
@section('extra-content')
    <div class="bannerwrap">
        <div class="content">
            <h1 class="bounceInDown wow">My <span>Bonuses</span></h1>
        </div>
    </div>
@stop

@section('content')
    <div class="myaccount_wrap">
        @if(session('message'))
            <div class="alert alert-info" role="alert">
                {{ session('message') }}
            </div>
        @endif

        <div class="my_accont">
            @if($bonuses->count() > 0)
                <div class="bonus-row">
                    @foreach($bonuses as $bonus)
                        <div class="bonus-card">
                            <h3 class="title">${{ $bonus->amount }}</h3>
                            @if($bonus->expire_days !== 0)
                                <div style="margin-bottom: 10px">
                                    <span>Expires on:</span> <i>{{ $bonus->expiry_date->toFormattedDateString() }}</i>
                                </div>
                            @endif
                            <div>
                                @if(!$bonus->getBonusExpiryStatus())
                                    @if($bonus->isWithdrawn())
                                        <a class="btn btn-warning" href="javascript:void(0)">Used</a>
                                    @else
                                        @if($bonus->eligiblePlans->count() > 0)
                                            <a class="btn btn-info mr-2" href="{{ route('bonus-reinvest-show', $bonus->ref_id) }}">Reinvest</a>
                                        @endif
                                        <a class="btn btn-success" href="{{ route('bonus-withdraw-show', $bonus->ref_id) }}">Withdraw</a>
                                    @endif
                                @else
                                    <span>Expired</span>
                                @endif
                            </div>
                        </div>
                    @endforeach
                </div>
            @else
                <div style="display: flex; justify-content: center; min-height: 50vh; align-items: center">
                    <b>You do not have any bonus at this time.</b>
                </div>
            @endif
        </div>
    </div>
@stop
