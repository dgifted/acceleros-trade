@extends('layouts.customer-master')
@section('page-title', 'Withdrawal')

@section('page-styles')
    <style>
        .inpts {
            color: #fff;
            width: 180px !important;
            height: 46px;
            background: #0c0a05;
            border: 1px solid #0c0a05;
            padding: 0 15px;
            border-radius: 36px;
            font-size: 15px;
        }

        select.inpts {
            width: auto !important;
        }
    </style>
@stop
@section('extra-classes', 'insideheaders')
@section('extra-content')
    <div class="bannerwrap">
        <div class="content">
            <h1 class="bounceInDown wow">Investment <span>Withdrawal</span></h1>
        </div>
    </div>
@stop

@section('content')
    <div class="myaccount_wrap">
        <div class="my_accont">
            <h2>Withdraw investment</h2>
            <br/>
            @if (session('message'))
                <div style="width: 100%; display: flex; align-items: center; justify-content: center">
                    <div class="alert alert-{{ session('type') }}" role="alert">
                        {{ session('message') }}
                    </div>
                </div>
            @endif
            <div><b>Account Balance:</b> $<span class="num-format">{{ $balance }}</span></div>
            <div><b>Pending Withdrawal:</b> $<span class="num-format">{{ $pendingWithdrawals }}</span></div>
            <hr/>

            <table class="table table-condensed">
                <thead>
                <tr>
                    <th>Processing</th>
                    <th>Available</th>
                    <th>Pending</th>
                    <th>Account address</th>
                </tr>
                </thead>
                <tbody>
                @foreach($channels as $channel)
                    <tr>
                        <td>
                            <img src="{{ $channel->icon }}" alt=""/>
                            <span style="text-transform: capitalize">{{ $channel->name }}</span>
                        </td>
                        <td style="{{ (double)$channel->availableFund > 0 ? 'color: green' : ''}}">$<span
                                class="num-format">{{ $channel->availableFund }}</span></td>
                        <td style="{{ (double)$channel->pendingFund > 0 ? 'color: green' : ''}}">$<span
                                class="num-format">{{ $channel->pendingFund }}</span></td>
                        <td>{{ $channel->userWallet }}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            <form method="post" action="{{ route('withdrawal.create') }}" id="withdraw-form">
                @csrf

                <div style="display: flex; flex-direction: column; align-items: flex-start">
                    <input type="text"
                           id="withdrawalAmount"
                           name="withdrawalAmount"
                           class="inpts"
                           value="{{ $pendingWithdrawals  }}"
                    />
                    <div style="margin-top: 10px; margin-bottom: 20px; display: flex; flex-direction: column">

                        @if($userWallets->count() === 0)
                            <a type="submit" class="sbmt" id="submit" style="margin-left: 5px;"
                               href="{{ route('account.home') }}">
                                Add crypto wallet to proceed
                            </a>
                        @else
                            <label for="wallet" style="margin-left: 10px">Select wallet</label>
                            <select name="wallet" class="inpts" id="wallet" required>
                                @foreach($userWallets as $wallet)
                                    <option value="{{ $wallet->id }}">
                                        {{ $wallet->paymentChannel->name }} - {{ $wallet->address }}
                                    </option>
                                @endforeach
                            </select>
                        @endif

                    </div>

                    @if($userWallets->count() > 0)
                        <div>
                            <button type="submit" class="sbmt" id="submit" style="margin-left: 5px;">
                                Withdraw
                            </button>

                            <button type="button" class="sbmt" id="reInvestButton" style="margin-left: 5px;">
                                Reinvest
                            </button>
                        </div>

                    @endif

                </div>
                <input type="hidden" name="can_withdraw" value="{{ $canWithdraw }}" id="can-withdraw">
            </form>
        </div>
    </div>
@stop

@section('page-scripts')
    <script src="{{ asset('assets/sweetalert2.all.min.js') }}"></script>

    <script>
        var withDrawableAmount = "{{ $pendingWithdrawals }}";
        var canWithDraw = $('#can-withdraw').val();
        var form = document.getElementById('withdraw-form');
        var amount = document.getElementById('withdrawalAmount').value;


        form.addEventListener('submit', (evt) => {
            if (canWithDraw === 'xxx-xxx' || +amount <= 0 || +amount > +withDrawableAmount) {
                Swal.fire({
                    icon: 'error',
                    title: 'Warning!',
                    text: 'You are not allowed to make a withdrawal at this time. Try again later'
                });

                evt.preventDefault();
            }
        });

        var reInvestButton = document.getElementById('reInvestButton');
        reInvestButton.addEventListener('click', () => {
            if (canWithDraw === 'xxx-xxx' || +amount <= 0 || +amount > +withDrawableAmount) {
                Swal.fire({
                    icon: 'error',
                    title: 'Warning!',
                    text: 'You are not allowed to perform this transaction at this moment.'
                });

                return;
            }

            location.href = `/reinvest-profits?profits=${amount}`
        });
    </script>
@endsection

