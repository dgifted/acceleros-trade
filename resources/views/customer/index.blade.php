@extends('layouts.customer-master')
@section('page-title', 'Dashboard')

@section('page-styles')

@stop
@section('extra-classes', 'insideheaders')
@section('extra-content')
    <div class="bannerwrap">
        <div class="content">
            <h1 class="bounceInDown wow">My <span>Dashboard</span></h1>
        </div>
    </div>
@stop

@section('content')
    <div class="myaccount_wrap">
        <div class="refcopy">
            <button id="copyButton" class="copyButton" data-clipboard-target="#copyTarget">&nbsp</button>
            <input id="copyTarget" value="{{ config('app.url') }}/register?ref={{ $user->username }}" readonly>
            <span id="msg"></span>
            <div class="getban"><a href="{{ route('deposit.create') }}">Deposit</a></div>
        </div>


        <h2 class="account-balance"> Account Balance
            <span>$<span class="num-format">{{ $balance }}</span></span>
        </h2>

        <div class="accbox left">
            <div class="icon"><img src="{{asset('assets/images/staticon2.png')}}" alt=""></div>
            <h3>
                <span style="display: flex; justify-content: center">
                    $ <span class="num-format">{{ $totalDeposit }}</span>
                </span>
                Active Deposit
            </h3>
            <div class="acc_button"><a href="{{ route('deposit.create') }}">Deposit Now</a></div>
            <ul>
                <li>
                    Last Deposit $<span class="num-format">{{ $lastDeposit->amount ?? 0 }}</span>

                </li>
                <li>Total Deposit $<span class="num-format">{{ $totalDeposit }}</span></li>
            </ul>

        </div>
        <div class="accbox right">
            <div class="icon"><img src="{{asset('assets/images/staticon3.png')}}" alt=""></div>
            <h3><span>${{ $profits }}</span>Earned Total</h3>
            <div class="acc_button"><a href="{{ route('withdrawal.create') }}">Withdraw Funds</a></div>
            <ul>
                <li>Last Withdrawal $<span class="num-format">{{ $lastWithdrawal->amount ?? 0 }}</span></li>
                <li>Total Withdrawal $<span class="num-format">{{ $totalWithdrawal }}</span></li>
            </ul>

        </div>
    </div>
@stop

@section('page-scripts')
    <script type="text/javascript">
        var clipboard = new ClipboardJS('button.copyButton');
        var targetInput = $('#copyTarget');
        clipboard.on('success', function() {
            var originalInputContent = targetInput.val();
            targetInput.val('Copied');
            setTimeout(() => {
                targetInput.val(originalInputContent);
            }, 1000);
        });
    </script>

@endsection
