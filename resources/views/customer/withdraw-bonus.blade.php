@extends('layouts.customer-master')
@section('page-title', 'Withdraw Bonus')

@section('page-styles')
    <style>
        .inpts {
            color: #fff;
            width: 180px !important;
            height: 46px;
            background: #0c0a05;
            border: 1px solid #0c0a05;
            padding: 0 15px;
            border-radius: 36px;
            font-size: 15px;
        }

        select.inpts {
            width: auto !important;
        }

        .badge-success {
            color: #ffffff;
            background-color: #0F9E5E;
        }

        .badge-warning {
            background-color: #e0a609;
            color: #0a1015;
        }

        .badge-danger {
            background-color: #be3727;
            color: #ffffff;
        }
    </style>
@stop
@section('extra-classes', 'insideheaders')
@section('extra-content')
    <div class="bannerwrap">
        <div class="content">
            <h1 class="bounceInDown wow">Bonus <span>Withdrawal</span></h1>
        </div>
    </div>
@stop

@section('content')
    <div class="myaccount_wrap">
        <div class="my_accont">
            <h2>Withdraw Bonus</h2>
            <br/>
            @if (session('message'))
                <div style="width: 100%; display: flex; align-items: center; justify-content: center">
                    <div class="alert alert-{{ session('type') }}" role="alert">
                        {{ session('message') }}
                    </div>
                </div>
            @endif
            <div><b>Amount:</b> $<span class="num-format">{{ $bonus->amount }}</span></div>
            <div>
                <b>Status:</b>
                @if(!$bonus->getBonusExpiryStatus())
                    @if($bonus->isWithdrawn())
                        <span class="badge badge-warning">Used</span>
                    @else
                        <span class="badge badge-success">Available</span>
                    @endif
                @else
                    <span class="badge badge-danger">Expired</span>
                @endif
            </div>
            <hr/>

            <table class="table table-condensed">
                <thead>
                <tr>
                    <th>Processing</th>
                    <th>Available</th>
                    <th>Pending</th>
                    <th>Account address</th>
                </tr>
                </thead>
                <tbody>
                @foreach($channels as $channel)
                    <tr>
                        <td>
                            <img src="{{ $channel->icon }}" alt=""/>
                            <span style="text-transform: capitalize">{{ $channel->name }}</span>
                        </td>
                        <td style="{{ (double)$channel->availableFund > 0 ? 'color: green' : ''}}">$<span
                                class="num-format">{{ $channel->availableFund }}</span></td>
                        <td style="{{ (double)$channel->pendingFund > 0 ? 'color: green' : ''}}">$<span
                                class="num-format">{{ $channel->pendingFund }}</span></td>
                        <td>{{ $channel->userWallet }}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            <form method="post" action="{{ route('bonus-withdraw', $bonus->ref_id) }}" id="withdraw-form">
                @csrf

                <div style="display: flex; flex-direction: column; align-items: flex-start">
                    <input type="text"
                           id="amount"
                           name="amount"
                           class="inpts"
                           value="{{ $bonus->amount }}"
                           readonly
                    />
                    <div style="margin-top: 10px; margin-bottom: 20px; display: flex; flex-direction: column">

                        @if($userWallets->count() === 0)
                            <a type="submit" class="sbmt" id="submit" style="margin-left: 5px;"
                               href="{{ route('account.home') }}">
                                Add crypto wallet to proceed
                            </a>
                        @else
                            <label for="wallet" style="margin-left: 10px">Select wallet</label>
                            <select name="wallet" class="inpts" id="wallet" required>
                                @foreach($userWallets as $wallet)
                                    <option value="{{ $wallet->id }}">
                                        {{ $wallet->paymentChannel->name }} - {{ $wallet->address }}
                                    </option>
                                @endforeach
                            </select>
                        @endif
                    </div>

                    @if($userWallets->count() > 0)
                        <div>
                            @if($bonus->isWithdrawn() || $bonus->getBonusExpiryStatus())
                            @else
                                <button type="submit" class="sbmt" id="submit" style="margin-left: 5px;">
                                    Withdraw
                                </button>
                            @endif
                        </div>
                    @endif
                </div>
            </form>
        </div>
    </div>
@stop

@section('page-scripts')
    <script src="{{ asset('assets/sweetalert2.all.min.js') }}"></script>
@endsection

