@extends('layouts.customer-master')
@section('page-title', 'Reinvest Bonus')

@section('page-styles')

@stop
@section('extra-classes', 'insideheaders')
@section('extra-content')
    <div class="bannerwrap">
        <div class="content">
            <h1 class="bounceInDown wow">Investment <span>Bonus</span></h1>
        </div>
    </div>
@stop

@section('content')
    <div class="myaccount_wrap">
        <div class="my_accont">
            <h2>Choose a plan</h2>
            @if (session('message'))
                <div style="width: 100%; display: flex; align-items: center; justify-content: center">
                    <div class="alert alert-{{ session('type') }}" role="alert">
                        {{ session('message') }}
                    </div>
                </div>
            @endif

            <div style="width: 100%; display: flex; align-items: center; justify-content: center">
                @if(session('errors'))
                    <div>
                        @foreach($errors as $msg)
                            <p class="error">{{ $msg }}</p>
                        @endforeach
                    </div>
                @endif
            </div>

            <form method="post" action="{{ route('bonus-reinvest', $bonus->ref_id) }}" id="deposit-form">
                @csrf
                @php
                    $count = 0;
                @endphp

                @foreach($plans as $plan)
                    @php
                        $count++
                    @endphp

                    <div class="plan-item">
                        <h2>
                            <input type="radio" name="plan" value="{{ $plan->id }}" class="plan"
                                   data-amount="{{ $plan->capital }}" @if($count === 1) checked @endif required>
                            {{ $plan->description }}
                        </h2>
                        <table class="plan-table">
                            <thead>
                            <tr>
                                <th>Plan</th>
                                <th>Capital ($)</th>
                                <th>Profit(%)</th>
                            </tr>
                            </thead>

                            <tbody>
                            <tr>
                                <td>{{ $plan->title}}</td>
                                <td>${{ $plan->min }} - ${{ $plan->max }}</td>
                                <td>{{ $plan->percentage }}</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                @endforeach
                <br>

                <div style="display: flex; align-items: center">
                    <span style="margin-right: 3rem">Amount to invest: </span>
                    <input type="text" class="inpts" value="{{ $bonus->amount }}"
                           name="amount"
                           id="amount"
                           style="max-width: 100px"
                           readonly
                    />
                    <br/>@error('amount') <p class="error">{{ $message }}</p> @enderror
                </div>

                <hr>
                <div style="display: flex; flex-direction: column; padding: 0 2rem">
                    @foreach($channels as $channel)
                        <label for="{{ $channel->slug }}">
                            <input type="radio"
                                   id="{{ $channel->slug }}"
                                   name="channel"
                                   value="{{ $channel->id }}"
                                   style="margin-right: 1.5rem"
                                   @if($channel->is_default == \App\Models\PaymentChannel::IS_DEFAULT) checked @endif
                                   required
                            />
                            Fund through {{ $channel->name }}
                        </label>
                    @endforeach
                </div>
                <hr>
                @if($bonus->isWithdrawn() || $bonus->getBonusExpiryStatus())
                    <button class="sbmt" type="button" id="" onclick="void(0)">Used</button>
                @else
                    <button class="sbmt" type="submit" id="submit">Invest</button>
                @endif
            </form>
        </div>
    </div>
@stop

@section('page-scripts')
    <script>
        setTimeout(function () {
            document.getElementById('amount').value = "{!! $bonus->amount !!}";
        }, 10);
    </script>
@endsection
