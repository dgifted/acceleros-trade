@extends('layouts.customer-master')
@section('page-title', 'Dashboard')

@section('page-styles')
    <style>
        .stat-row {
            display: flex;
            justify-items: flex-start;
            align-items: center;
        }

        .stat-row .label {
            display: inline-block;
            width: 160px;
            font-weight: bold;
            margin-right: 0.3rem;
            font-size: 1.3rem;
            text-align: left;
        }

        .stat-row .label + span {
            width: 50px;
            display: inline-block;
            text-align: right;
        }
    </style>
@stop
@section('extra-classes', 'insideheaders')
@section('extra-content')
    <div class="bannerwrap">
        <div class="content">
            <h1 class="bounceInDown wow">My <span>Referrals</span></h1>
        </div>
    </div>
@stop

@section('content')
    <div class="myaccount_wrap">
        <div style="border-bottom: 2px solid #268d94; margin-bottom: 3rem">
            <div class="stat-row">
                <span class="label">Referred users:</span>
                <span>{{ $referees->count() }}</span>
            </div>
            <div class="stat-row">
                <span class="label">Total referral commission:</span>
                <span>
                    $ <span class="num-format">{{ $commission * $referees->count() }}</span>
                </span>

            </div>
            <br/>
        </div>

        <div style="border-bottom: 2px solid #268d94; padding-bottom: 2rem; margin-bottom: 3rem">
            <br/>
            @if (session('message'))
                <div style="width: 100%; display: flex; align-items: center; justify-content: center">
                    <div class="alert alert-{{ session('type') }}" role="alert">
                        {{ session('message') }}
                    </div>
                </div>
            @endif
            <table class="table table-bordered">
                <thead>
                <tr>
                    <th>Referee Name</th>
                    <th>Referee First Deposit</th>
                    <th>Date Registered</th>
                </tr>
                </thead>
                <tbody>
                @if($referees->count() > 0)
                    @foreach($referees as $ref)
                        <tr>
                            <td>{{ $ref->name }}</td>
                            <td>$ <span>{{ $ref->deposits->first()->amount ?? 0 }}</span></td>
                            <td>{{ $ref->created_at->toFormattedDateString() }}</td>
                        </tr>
                    @endforeach
                @else
                @endif
                </tbody>
            </table>
        </div>


        @if($referees->count() > 0)
            <form action="{{ route('referrals.withdraw') }}" method="post">
                @csrf

                <div>
                    <label for="wallet">Choose wallet</label><br>
                    <select type="text" id="wallet" name="wallet" class="inpts" required>
                        @if($userWallets->count() === 0)
                            <option value="">Select wallet</option>
                        @else
                            @foreach($userWallets as $wallet)
                                <option value="{{ $wallet->id }}">{{ $wallet->paymentChannel->name }} - {{ $wallet->address }}</option>
                            @endforeach
                        @endif
                    </select>
                    <input type="hidden" name="amount" value="{{ $referees->count() * $commission }}">
                    <br>
                    <br>
                    <div>
                        <button class="sbmt"
                                type="submit"
                                id="withdrawBonus"
                                @if($commission * $referees->count() === 0) disabled @endif
                                style="cursor: {{ $commission * $referees->count() === 0 ? 'not-allowed' : 'pointer' }}"
                        >
                            @if($commission * $referees->count() === 0)
                                No bonus
                            @else
                                Withdraw bonus
                            @endif
                        </button>
                    </div>
                </div>
            </form>

        @endif
    </div>


@stop
