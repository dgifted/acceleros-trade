@extends('layouts.customer-master')
@section('page-title', 'Account history')

@section('page-styles')
    <style>
        .stat-row {
            display: flex;
            justify-items: flex-start;
            align-items: center;
            margin-bottom: 10px;
        }

        .stat-row .label {
            display: inline-block;
            width: 160px;
            font-weight: bold;
            margin-right: 0.3rem;
            font-size: 1.3rem;
            text-align: left;
        }

        .stat-row .label + span {
            width: 150px;
            display: inline-block;
            text-align: left;
        }

        .stat-row input {
            max-width: 400px;
        }
    </style>
@stop
@section('extra-classes', 'insideheaders')
@section('extra-content')
    <div class="bannerwrap">
        <div class="content">
            <h1 class="bounceInDown wow">Account <span>History</span></h1>
        </div>
    </div>
@stop

@section('content')
    <div class="myaccount_wrap">
        @if(session('message'))
            <div class="alert alert-info" role="alert">
                {{ session('message') }}
            </div>
        @endif

        <form id="filter-form" method="post" action="{{ route('account.history') }}" name="filter"
              style="margin-bottom: 20px">
            @csrf

            <table cellspacing="0" cellpadding="0" border="0" width="100%">
                <tbody>
                <tr>
                    <td>
                        <select name="type" class="inpts" id="type">
                            @foreach($types as $key => $type)
                                <option value="{{ $key }}" @if($aType === $key) selected @endif>{{ $type }}</option>
                            @endforeach
                        </select>
                        <br><br>
                        {{--                        <select name="currency" class="inpts">--}}
                        {{--                            <option value="-1">All eCurrencies</option>--}}
                        {{--                            @foreach($paymentChannels as $channel)--}}
                        {{--                                <option value="{{ $channel->id }}"--}}
                        {{--                                        style="text-transform: capitalize">{{ $channel->name }}</option>--}}
                        {{--                            @endforeach--}}
                        {{--                        </select>--}}
                    </td>
                    <td align="right">
                        From: <select name="month_from" class="inpts">
                            @foreach($months as $key => $month)
                                <option value="{{ $key }}" @if($key === ($today->month - 1)) selected @endif>
                                    {{ $month }}
                                </option>
                            @endforeach
                        </select> &nbsp;
                        <select name="day_from" class="inpts">
                            @foreach($days as $day)
                                <option value="{{ $day }}">{{ $day }}</option>
                            @endforeach
                        </select> &nbsp;

                        <select name="year_from" class="inpts">
                            <option value="{{ $user->created_at->year }}">{{ $user->created_at->year }}</option>
                            <option value="{{ $today->year }}">{{ $today->year }}</option>
                        </select><br><br>

                        To: <select name="month_to" class="inpts">
                            @foreach($months as $key => $month)
                                <option value="{{ $key }}"
                                        @if($key === $today->month) selected @endif>{{ $month }}</option>
                            @endforeach
                        </select> &nbsp;
                        <select name="day_to" class="inpts">
                            @foreach($days as $day)
                                <option value="{{ $day }}" @if($day == $today->day) selected @endif>{{ $day }}</option>
                            @endforeach
                        </select> &nbsp;

                        <select name="year_to" class="inpts">
                            <option value="{{ $user->created_at->year }}">{{ $user->created_at->year }}</option>
                            <option value="{{ $today->year }}" selected>{{ $today->year }}</option>
                        </select>

                    </td>
                    <td>
                        <button type="submit" id="submitBtn" class="sbmt">Go</button>
                    </td>
                </tr>
                </tbody>
            </table>
        </form>
        <div>
            <table cellspacing="1" cellpadding="2" border="0" width="100%">
                <tbody>
                <tr>
                    <td class="inheader"><b>Type</b></td>
                    <td class="inheader" width="200"><b>Amount</b></td>
                    <td class="inheader" width="170"><b>Date</b></td>
                </tr>
                @php
                    $totalAmount = 0;
                @endphp
                @if(session('payload') && session('payload')->count() > 0)
                    <tr style="margin-top: 0">
                        <td colspan="3">&nbsp;</td>
                    </tr>
                    @foreach(session('payload') as $data)
                        @php
                            $totalAmount += $data->amount ?? 0
                        @endphp
                        <tr>
                            <td style="padding: 10px 0">{{ $data->type }}</td>
                            <td width="200">
                                $<span class="num-format">{{ $data->amount ?? 0}}</span>.00
                            </td>
                            <td class="" width="170">{{ $data->date->toFormattedDateString() }}</td>
                        </tr>
                    @endforeach
                @else
                    <tr>
                        <td colspan="3" align="center" style="padding: 40px">No transactions found</td>
                    </tr>
                @endif
                <hr/>
                <tr>
                    <td>Total:</td>
                    <td align="left"><b>$ <span class="num-format">{{ $totalAmount }}</span>.00</b></td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
@stop

@section('page-scripts')
    <script>
        var filterForm = $('#filter-form');
        var typeSelector = $('#type');
        var type = typeSelector.val();


        filterForm.on('submit', formSubmitHandler);
        typeSelector.on('change', filterSelectorChangeHandler);

        function formSubmitHandler() {
            attachUrlParams({
                a_type: type
            });
        }

        function filterSelectorChangeHandler() {
            attachUrlParams({
                a_type: type
            });
            filterForm.submit();
        }

        function attachUrlParams(queryParams = {a_type: 'all'}) {
            var pageUrl = new URL(window.location.href);
            pageUrl.searchParams.append('a_type', type);

            for (const [key, val] of Object.entries(queryParams)) {
                console.log(key, ' => ', val);
            }
        }
    </script>
@endsection
