@extends('layouts.landing-master')
@section('page-title', 'User registration')

@section('page-styles')
    <link href="{{ asset('assets/sweetalert2.min.css') }}" rel="stylesheet"/>
    <style>
        .btn-login {
            font-size: 15px;
            font-weight: 600;
            color: #0c0a05 !important;
            text-transform: uppercase;
        }
    </style>
@stop
@section('extra-classes', 'insideheaders')
@section('extra-content')
    <div class="bannerwrap">
        <div class="content">
            <h1 class="bounceInDown wow">User <span>Registration</span></h1>
        </div>
    </div>
@stop

@section('content')
    <div class="loginpage">
        <div class="content">
            <div class="loginwrappers">

                <center><img src="{{asset('assets/images/icon2.png')}}" alt=""></center>

                <div class="form-container login">
                    <form method="post" action="{{ route('register') }}" name="regform" id="register-form">
                        @csrf

                        <table width="100%" border="0" cellspacing="0">
                            <tr>
                                <td width="100%">
                                    <input type=text
                                           name="name"
                                           class="inpts"
                                           placeholder="Your full name"
                                           required
                                           value="{{ old('name') }}"
                                    />
                                    <br>@error('name') <p class="error">{{ $message }}</p> @enderror
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <input type=text
                                           name="username"
                                           class="inpts"
                                           placeholder="Your username"
                                           required
                                           value="{{ old('username') }}"
                                    />
                                    <br>@error('username') <p class="error">{{ $message }}</p> @enderror
                                </td>

                            </tr>
                            <tr>
                                <td>
                                    <input type=text
                                           name="phone"
                                           class="inpts"
                                           placeholder="Your phone number"
                                           value="{{ old('phone') }}"
                                    />
                                    <br>@error('phone') <p class="error">{{ $message }}</p> @enderror
                                </td>

                            </tr>
                            <tr>
                                <td>
                                    <input type="password"
                                           name="password"
                                           class="inpts"
                                           placeholder="Your Password"
                                           required
                                           value="{{ old('password') }}"
                                    />
                                    <br>@error('password') <p class="error">{{ $message }}</p> @enderror
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <input type="password"
                                           name="password_confirmation"
                                           class="inpts"
                                           placeholder="Retype password"
                                           required
                                           value="{{ old('password_confirmation') }}"
                                    />
                                    <br>@error('password_confirmation') <p class="error">{{ $message }}</p> @enderror
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <input type="text"
                                           name="email"
                                           class="inpts"
                                           placeholder="Your email address"
                                           required
                                           value="{{ old('email') }}"
                                    />
                                    <br>@error('email') <p class="error">{{ $message }}</p> @enderror
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <input type="text"
                                           name="email_confirmation"
                                           class="inpts"
                                           placeholder="Retype email address"
                                           required
                                           value="{{ old('email_confirmation') }}"
                                    />
                                    <br>@error('email_confirmation') <p class="error">{{ $message }}</p> @enderror
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <input type="text"
                                           name="secret_question"
                                           class="inpts"
                                           placeholder="Secret Question"
                                           required
                                           value="{{ old('secret_question') }}"
                                    />
                                    <br>@error('secret_question') <p class="error">{{ $message }}</p> @enderror
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <input type=text
                                           name="secret_answer"
                                           class="inpts"
                                           placeholder="Secret Answer"
                                           required
                                           value="{{ old('secret_answer') }}"
                                    />
                                    <br>@error('secret_answer') <p class="error">{{ $message }}</p> @enderror
                                </td>
                            </tr>

                        </table>
                        <table width="100%" border=0 cellspacing=0>
                            @foreach($channels as $channel)
                                <tr>
                                    <td>
                                        <input type="text"
                                               class="inpts"
                                               name="{{ $channel->slug }}"
                                               placeholder="Your {{ $channel->name }} account"
                                               value="{{ old($channel->slug) }}"
                                        />
                                        <br>@error($channel->slug) <p class="error">{{ $message }}</p> @enderror
                                    </td>
                                </tr>
                            @endforeach
                                <tr>
                                    <td>
                                        <input type=text
                                               name="secret_answer"
                                               class="inpts"
                                               required
                                               readonly
                                               value="@if($referee) {{$referee->name}} @endif"
                                        />
                                    </td>
                                </tr>
                            <tr>
                                <td align="left">
                                    <div class="iagree">
                                        <input type="checkbox" name="agree" required>
                                        <a href="{{ route('terms-policy') }}"> Terms and conditions </a>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <button type="submit" class="sbmt" id="submit">Register</button>
                                    <span style="margin: 0 10px">|</span>
                                    <a href="{{ route('login') }}" class="btn-login">Login</a>
                                </td>
                            </tr>
                        </table>
                    </form>
                </div>
            </div>
        </div>
    </div>
@stop

@section('page-scripts')

@endsection

