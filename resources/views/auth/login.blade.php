@extends('layouts.landing-master')
@section('page-title', 'User login')

@section('page-styles')
    <link href="{{ asset('assets/sweetalert2.min.css') }}" rel="stylesheet"/>
    <style>
        .btn-login {
            font-size: 15px;
            font-weight: 600;
            color: #0c0a05 !important;
            text-transform: uppercase;
        }
    </style>
@stop
@section('extra-classes', 'insideheaders')
@section('extra-content')
    <div class="bannerwrap">

        <div class="content">

            <h1 class="bounceInDown wow">User <span>login</span></h1>
        </div>
    </div>
@stop

@section('content')
    <div class="loginpage">
        <div class="content">
            <div class="loginwrappers">

                <center><img src="{{asset('assets/images/icon2.png')}}" alt=""></center>

                <div class="form-container login">
                    <form method="post" action="{{ route('login') }}" id="login-form">
                        @csrf

                        <table width="100%" border="0" cellpadding="4" cellspacing="4">
                            <tr>
                                <td colspan="2">
                                    <input type="text"
                                           name="identifier"
                                           class="inpts" id="identifier"
                                           autofocus="autofocus"
                                           placeholder="Email or username"
                                           required
                                           value="{{ old('identifier') }}"
                                    />
                                    <br>@error('identifier') <p class="error">{{ $message }}</p> @enderror
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <input type="password"
                                           name="password"
                                           class="inpts"
                                           id="password"
                                           placeholder="Password"
                                           required
                                           value="{{ old('password') }}"
                                    />
                                </td>
                            </tr>
                            <tr>
                                <td height="40" valign="middle">
                                    <button type="submit" class="sbmt" id="submit">Login</button>
                                    <span style="margin: 0 10px">|</span>
                                    <a href="{{ route('register') }}" class="btn-login">Register</a>
                                </td>
                                <td align="right" valign="middle"><a href="{{ route('password.request') }}">Forgot
                                        Password?</a></td>
                            </tr>
                        </table>
                    </form>
                </div>
            </div>
        </div>

    </div>
@stop

@section('page-scripts')
    <script src="{{ asset('assets/sweetalert2.all.min.js') }}"></script>
{{--    <script>--}}
{{--        var form = document.getElementById('login-form');--}}
{{--        var usernameField = document.getElementById('username');--}}
{{--        var passwordField = document.getElementById('password');--}}

{{--        var submit = document.getElementById('submit');--}}

{{--        form.addEventListener('submit', (evt) => {--}}
{{--            evt.preventDefault();--}}
{{--            submit.innerText = 'Please wait...';--}}

{{--            setTimeout(() => {--}}
{{--                usernameField.value = '';--}}
{{--                passwordField.value = '';--}}

{{--                Swal.fire({--}}
{{--                    title: 'Hey!',--}}
{{--                    text: 'This part of this website is still under active development. We will launch in a few days.',--}}
{{--                    icon: 'success',--}}
{{--                });--}}

{{--                submit.innerText = 'Login'--}}
{{--            }, 2000);--}}
{{--        });--}}
{{--    </script>--}}
@endsection
