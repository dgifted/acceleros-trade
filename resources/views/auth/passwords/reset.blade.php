@extends('layouts.landing-master')
@section('page-title', 'User password change')

@section('page-styles')
    <link href="{{ asset('assets/sweetalert2.min.css') }}" rel="stylesheet"/>
    <style>
        .btn-login {
            font-size: 15px;
            font-weight: 600;
            color: #0c0a05 !important;
            text-transform: uppercase;
        }
    </style>
@stop
@section('extra-classes', 'insideheaders')
@section('extra-content')
    <div class="bannerwrap">
        <div class="content">
            <h1 class="bounceInDown wow">Reset <span>Password</span></h1>
        </div>
    </div>
@stop

@section('content')
    <div class="loginpage">
        <div class="content">
            <div class="loginwrappers">
                {{--                <h3>Forgot your password:</h3><br>--}}
                <form method=post action="{{ route('password.update') }}" id="reset-form">
                    @csrf

                    <input type="hidden" name="token" value="{{ $token }}">

                    <table width="100%" border="0" cellpadding="4" cellspacing="4">
                        <tr>
                            <td>
                                <input type="email"
                                       name="email"
                                       class="inpts"
                                       placeholder="Enter your email"
                                       required
                                       autofocus
                                       autocomplete="email"
                                       value="{{ $email ?? old('email') }}"
                                />
                                <br>@error('email') <p class="error">{{ $message }}</p> @enderror
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <input type="password"
                                       name="password"
                                       class="inpts"
                                       placeholder="Enter new password"
                                       required
                                       autofocus
                                       autocomplete="new password"
                                       value="{{ old('password') }}"
                                />
                                <br>@error('password') <p class="error">{{ $message }}</p> @enderror
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <input type="password"
                                       name="password_confirmation"
                                       class="inpts"
                                       placeholder="Confirm new password"
                                       required
                                       autofocus
                                       autocomplete="new password"
                                />
                                <br>@error('email') <p class="error">{{ $message }}</p> @enderror
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <button type="submit" class="sbmt" id="submit">Reset password</button>
                                <span style="margin: 0 10px">|</span>
                                <a href="{{ route('login') }}" class="btn-login">Login</a>
                            </td>
                        </tr>
                    </table>
                </form>
            </div>
        </div>
    </div>
@stop

@section('page-scripts')
    <script src="{{ asset('assets/sweetalert2.all.min.js') }}"></script>
@endsection
