@extends('layouts.admin-master')
@section('title', 'Payment channels')
@section('page-styles')

@stop

@section('page-title', 'Payment channels')

@section('content')
    @if(session('message'))
        <div class="alert alert-{{ session('type') }} alert-dismissible fade show" role="alert">
            {{ session('message') }}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    @endif
    <div class="row">
        <div class="col-md-7">
            <div class="card card-secondary">
                <div class="card-header">
                    <h3 class="card-title">Available payment channel</h3>
                </div>

                <div class="card-body">
                    @if (!isset($channels) || $channels->count() == 0)
                        You do not have any payment channel set up yet. Start by adding a payment channel on the right
                        pane.
                    @else
                        @foreach ($channels as $channel)
                            <div
                                class="info-box border {{ $channel->isSupported() ? 'border-success' : 'border-danger' }}">
                                <span class="d-none d-sm-block info-box-icon bg-info elevation-1"><i class="fas fa-wallet"></i></span>

                                <div class="info-box-content">
                                    <div class="d-flex justify-content-start justify-content-sm-end align-items-center">
                                        @if($channel->isSupported())
                                            <a href="{{ route('admin.payment-channel-toggle-status', $channel->ref_id) }}"
                                               class="btn btn-xs btn-warning"
                                               data-ref="{{ $channel->ref_id }}">
                                                <i class="fas fa-times mr-1"></i>
                                                Deactivate
                                            </a>
                                        @else
                                            <a href="{{ route('admin.payment-channel-toggle-status', $channel->ref_id) }}"
                                               class="btn btn-xs btn-success">
                                                <i class="fas fa-check mr-1"></i>
                                                Activate
                                            </a>
                                        @endif
                                        <a href="#" class="btn btn-xs btn-danger mx-1"
                                           data-toggle="modal" data-target="#modal-confirm"
                                           data-ref="{{ $channel->ref_id }}">
                                            <i class="fas fa-trash mr-1"></i>
                                            Delete
                                        </a>
                                        <a href="{{ route('admin.payment-channel-update', $channel->ref_id) }}"
                                           class="btn btn-xs btn-info">
                                            <i class="fas fa-edit mr-1"></i>
                                            Edit
                                        </a>
                                    </div>
                                    <div class="d-flex justify-content-between">
                                        <span class="info-box-text"><b>Currency:</b>
                                            <span class="text-capitalize">{{$channel->name}}</span>
                                            @if($channel->abbreviation) ({{ $channel->abbreviation }}) @endif
                                        </span>

                                    </div>

                                    <div class="info-box-number">
                                        <div class="info-box-text d-flex flex-column d-sm-block">
                                            Wallet address: <small>{{$channel->wallet}}</small>
                                        </div>
                                    </div>
                                </div>
                                <!-- /.info-box-content -->
                            </div>
                        @endforeach
                    @endif
                </div>
            </div>
        </div>
        <div class="col-md-5">
            <div class="card card-secondary">
                <div class="card-header">
                    <h3 class="card-title">Add payment channel</h3>
                </div>
                <!-- /.card-header -->
                <!-- form start -->
                <form name="frmWallet" action="{{route('admin.view-payment-channels')}}" method="POST">
                    @csrf

                    <div class="card-body">
                        <div class="form-group">
                            <label for="name">Account name</label>
                            <input type="text" name="name" class="form-control @error('name') is-invalid @enderror" id="name"
                                   placeholder="Enter account name (e.g. Bitcoin)">
                            @error('name') <p class="text-danger text-sm">{{ $message }}</p> @enderror
                        </div>
                        <div class="form-group">
                            <label for="abbreviation">Wallet abbreviation</label>
                            <input type="text" name="abbreviation" class="form-control" id="abbreviation"
                                   placeholder="Enter wallet abbreviate (e.g. BTC)">
                        </div>
                        <div class="form-group">
                            <label for="wallet">Wallet address</label>
                            <input type="text" name="wallet" class="form-control @error('wallet') is-invalid @enderror" id="wallet"
                                   placeholder="Enter wallet address ...">
                            @error('wallet') <p class="text-danger text-sm">{{ $message }}</p> @enderror
                        </div>
                    </div>
                    <!-- /.card-body -->
                    <div class="card-footer">
                        <button type="submit" class="btn btn-secondary">
                            <i class="fas fa-check ml-2"></i> Save
                        </button>
                    </div>
                </form>
            </div>
            <!-- /.card -->
        </div>
    </div>
    {{--    Deposit delete form--}}
    <form action="" method="post" class="d-none" id="channelDeleteForm">
        @method('DELETE')
        @csrf
    </form>
@stop

@section('modal-confirm')
    @include('includes.admin.confirm-modal')
@stop

@section('page-scripts')
    <script>
        //Payment channel delete logic
        var deleteModal = $('#modal-confirm');
        var submitButton = deleteModal.find('#action');
        var channelDeleteForm = $('#channelDeleteForm');

        deleteModal.on('show.bs.modal', function (event) {
            deleteModal.find('.modal-title').text('Confirm');
            deleteModal.find('#model').text('payment channel');
            var btn = $(event.relatedTarget);
            var ref = btn.data('ref');

            channelDeleteForm.attr('action', `/admin/payment-channels/${ref}/delete`);
        });

        submitButton.on('click', function (evt) {
            var button = $(this);
            button.attr('href', '/admin/payment-channels');

            evt.preventDefault();
            channelDeleteForm.submit();
        })
    </script>
@endsection
