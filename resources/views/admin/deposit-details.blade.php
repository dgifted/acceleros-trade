@extends('layouts.admin-master')
@section('title', 'Deposit Details')
@section('page-styles')

@stop

@section('page-title', 'Deposit Details')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-4 offset-md-4">
                                    <input type="text" name="ref_id" id="ref_id" class="form-control"
                                           value="{!!$deposit->ref_id!!}">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <div class="d-flex justify-content-between">
                        <h3 class="card-title mt-2">Deposit Detail</h3>
                        <div>
                            @if ($deposit->status === 0)
                                <a href="{{route('admin.deposit-approve', $deposit->ref_id)}}"
                                   class="btn btn-secondary btn-xs" id="approveDepositButton" data-ref="{{ $deposit->ref_id }}">
                                    Approve <i class="fas fa-check ml-2"></i>
                                </a>
                            @endif
                            @if($deposit->status !== 1)
                                <button type="button" id="edit-investment" class="btn btn-xs btn-info"
                                        data-ref="{{$deposit->ref_id}}" data-toggle="modal"
                                        data-target="#modal-default">
                                    <i class="far fa-edit"></i> Edit
                                </button>
                            @endif
                        </div>

                    </div>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-4">
                            <!-- Widget: user widget style 1 -->
                            <div class="card card-widget widget-user">
                                <!-- Add the bg color to the header using any of the bg-* classes -->
                                <div class="widget-user-header bg-info">
                                    <h3 class="widget-user-username">{{$deposit->customer->name}}</h3>
                                    <h5 class="widget-user-desc">{{$deposit->customer->email}}</h5>
                                </div>
                                <div class="widget-user-image">
                                    <img class="img-circle elevation-2" src="{{$deposit->customer->avatar_path}}"
                                         alt="User Avatar">
                                </div>
                                <div class="card-footer">
                                    <div class="row">
                                        <div class="col-sm-6 offset-md-3">
                                            <div class="description-block">
                                                <h5 class="description-header">Register on:</h5>
                                                <span
                                                    class="description-text text-sm">{{ $deposit->customer->created_at->diffForHumans() }}</span>
                                            </div>
                                            <!-- /.description-block -->
                                        </div>
                                        <!-- /.col -->
                                        <!-- /.col -->
                                    </div>
                                    <!-- /.row -->
                                </div>
                            </div>
                            <!-- /.widget-user -->
                        </div>
                        <div class="col-md-8">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="info-box bg-light">
                                        <div class="info-box-content">
                                            <span class="info-box-text text-center text-muted">Amount</span>
                                            <span class="info-box-number text-center text-muted mb-0"><i
                                                    class="fab fa-bitcoin text-warning"></i> {{$deposit->amount}}</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="info-box bg-light">
                                        <div class="info-box-content">
                                            <span class="info-box-text text-center text-muted">Deposit status</span>
                                            <span
                                                class="info-box-number text-center @if($deposit->status === 0) text-warning @elseif($deposit->status === 1) text-success @else text-danger @endif mb-0">
                                                @switch($deposit->status)
                                                    @case(0)
                                                    pending
                                                    @break
                                                    @case(1)
                                                    approved
                                                    @break
                                                    @case(2)
                                                    cancelled
                                                    @break
                                                    @default
                                                    failed
                                                @endswitch
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-4">
                                    <div class="info-box bg-light">
                                        <div class="info-box-content">
                                            <span class="info-box-text text-center text-muted">Plan</span>
                                            <span
                                                class="info-box-number text-center text-muted mb-0">{{$deposit->plan->title}} plan</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="info-box bg-light">
                                        <div class="info-box-content">
                                            <span class="info-box-text text-center text-muted">Interest rate</span>
                                            <span
                                                class="info-box-number text-center text-muted mb-0">{{ $deposit->plan->percentage }} &percnt;</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="info-box bg-light">
                                        <div class="info-box-content">
                                            <span class="info-box-text text-center text-muted">Duration</span>
                                            <span
                                                class="info-box-number text-center text-muted mb-0">{{ $deposit->plan->duration }} days</span>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row mt-3">
                                <div class="col-md-6">
                                    <div class="info-box bg-light">
                                        <div class="info-box-content">
                                            <span class="info-box-text text-center text-muted">Plan description</span>
                                            <span
                                                class="info-box-number text-center text-muted mb-0">{{$deposit->plan->description ?? ''}}</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="info-box bg-light">
                                        <div class="info-box-content">
                                            <span class="info-box-text text-center text-muted">Payment channel</span>
                                            <span
                                                class="info-box-number text-center text-muted mb-0">{{ $deposit->channel->name }} ({{ $deposit->channel->wallet }})</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <form action="" class="d-none" method="POST" id="depositApprovalForm">
        @csrf
    </form>
    @include('includes.admin.deposit-edit-modal')
@stop

@section('modal-confirm')
@stop

@section('page-scripts')
    <script src="{{asset('plugins/bs-custom-file-input/bs-custom-file-input.min.js')}}"></script>
    <script src="{{ asset('assets/js/toastr.min.js') }}"></script>
    <script src="{{ asset('js/blockui.js') }}"></script>
    <script src="{{ asset('assets/js/alert.controller.js') }}"></script>
    <script>
        var request = undefined;
        $(document).ready(function () {
            bsCustomFileInput.init();
        });

        $('#save-edit').on('click', function (event) {
            event.preventDefault();
            var investmentAmount = $('#investment-amount').val();
            var refId = $(this).data('ref');

            if (investmentAmount && investmentAmount > 0) {
                var url = `/admin/deposits/${refId}/edit`;
                var requestPayload = {amount: investmentAmount};
                // console.log('url => ', url, 'requestPayload => ', requestPayload); return;

                if (request) {
                    request.abort();
                }

                request = $.ajax({
                    url: url,
                    type: 'POST',
                    dataType: 'json',
                    data: requestPayload
                });

                request.done(function (res) {
                    showToast('success', 'Deposit successfully updated.');

                });

                request.fail(function (_, __, errorMessage) {
                    console.log('errorMessage => ', errorMessage);
                    showToast('error', 'Error updating investment. Please try again');
                });
            }
        });

        var modalTargetButton = $('#edit-investment');
        var refId = modalTargetButton.data('ref');
        var modal = $('#modal-default');

        var fullDateString = "{{$deposit->approval_at ?? $deposit->created_at}}";
        var date = fullDateString.split(' ')[0];
        modal.find('#date').val(date);
        modal.find('#amount').val("{{$deposit->amount}}");

        modalTargetButton.on('click', function () {
            modal.show();
        });

        $('#update-investment').on('click', function (event) {
            event.preventDefault();

            $.blockUI({
                css: {
                    border: 'none',
                    padding: '15px',
                    backgroundColor: '#000',
                    '-webkit-border-radius': '10px',
                    '-moz-border-radius': '10px',
                    opacity: .5,
                    color: '#fff'
                }
            });

            var requestPayload = {
                date: modal.find('#date').val(),
                amount: modal.find('#amount').val()
            };

            if (requestPayload.amount && requestPayload.amount > 0) {
                var url = `/admin/deposits/${refId}/edit`;

                if (request) {
                    request.abort();
                }

                request = $.ajax({
                    url: url,
                    type: 'POST',
                    dataType: 'json',
                    data: requestPayload
                });

                request.done(function () {
                    $.unblockUI();
                    showToast('success', 'Deposit successfully updated.');
                    modal.hide();
                    setTimeout(() => {
                        location.reload();
                    }, 3500)
                });

                request.fail(function (_, __, errorMessage) {
                    $.unblockUI();
                    showToast('error', 'Error updating investment. Please try again');
                });
            }
        });

        var depositApprovalForm = $('#depositApprovalForm');
        var approveDepositButton = $('#approveDepositButton');

        approveDepositButton.on('click', function (evt) {
            evt.preventDefault();
            var ref = $(this).data('ref')

            depositApprovalForm.attr('action', `/admin/deposits/${ref}`);
            depositApprovalForm.submit();
        })
    </script>
@endsection
