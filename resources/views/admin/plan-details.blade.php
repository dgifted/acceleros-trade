@extends('layouts.admin-master')
@section('title', 'Plan details')
@section('page-styles')

@stop

@section('page-title', 'Plan details')

@section('content')
    @if(session('message'))
        <div class="alert alert-{{ session('type') }} alert-dismissible fade show" role="alert">
            {{ session('message') }}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    @endif

    <div class="row">
        <div class="col-md-3">
            <!-- Profile Image -->
            <div class="card card-primary card-outline">
                <div class="card-body box-profile">

                    <h3 class="profile-username text-center text-bold">{{$plan->title}}</h3>

                    <ul class="list-group list-group-unbordered mb-3">
                        <li class="list-group-item">
                            <b>Percentage</b> <a class="float-right">{{$plan->percentage}}%</a>
                        </li>
                        <li class="list-group-item">
                            <b>Min principal</b> <a class="float-right">$ {{$plan->min}}</a>
                        </li>
                        <li class="list-group-item">
                            <b>Max principal</b> <a class="float-right">$ {{$plan->max}}</a>
                        </li>
                        <li class="list-group-item">
                            <b>Duration</b><a class="float-right">{{$plan->duration}} days</a>
                        </li>
                        <li class="list-group-item">
                            <b>Total active deposits: </b><a class="float-right">$<span>{{$totalDeposits}}</span>.00</a>
                        </li>
                    </ul>
                </div>
                <!-- /.card-body -->
            </div>
            <!-- /.card -->

        </div>
        <div class="col-md-9">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <!-- /.card-header -->
                        @if($errors->any())
                            <div class="alert alert-warning alert-dismissible fade show mt-2 mx-2" role="alert">
                                @foreach ($errors->all() as $msg)
                                    <i class="fas fa-exclamation text-danger mr-2"></i>{{ $msg }}
                                @endforeach
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        @endif
                        <div class="card-body">
                            <form action="{{route('admin.plan-edit', $plan->ref_id)}}" method="POST" name="frmPlan">
                                @method('PATCH')
                                @csrf

                                <div class="row p-2">
                                    <div class="col-12 col-md-6">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label for="name">Name</label>
                                                    <input type="text" name="title" class="form-control" id="title"
                                                           placeholder="Enter plan name"
                                                           value="{{$plan->title}}" required>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label for="min">Minimum capital</label>
                                                    <input type="number" name="min" class="form-control"
                                                           id="min" placeholder="Enter plan min amount"
                                                           value="{{$plan->min}}" required>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12 col-md-6">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label for="max">Maximum capital</label>
                                                    <input type="number" name="max" class="form-control"
                                                           id="max" placeholder="Enter plan max amount"
                                                           value="{{$plan->max}}" required>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label for="percentage">Accrual percentage</label>
                                                    <input type="number" name="percentage" class="form-control"
                                                           id="percentage"
                                                           placeholder="Enter percentage (per day)"
                                                           value="{{$plan->percentage}}" required>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <hr class="mt-1">
                                <div class="row p-2">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="duration">Duration</label>
                                            <input type="number" name="duration" class="form-control"
                                                   id="duration" placeholder="Enter duration (in days)"
                                                   value="{{$plan->duration}}" required>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="description">Description</label>
                                            <input type="text" name="description" class="form-control" id="description"
                                                   placeholder="Enter description"
                                                   value="{{$plan->description}}" required>
                                        </div>
                                    </div>
                                </div>
                                <div class="row mt-2 p-2">
                                    <div class="col-md-6">
                                        <div class="input-group">
                                            <button type="submit" id="submit" class="btn btn-md btn-primary">
                                                Update changes
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('modal-confirm')
    @include('includes.admin.confirm-modal')
@stop

@section('page-scripts')

@endsection
