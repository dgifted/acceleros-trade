@extends('layouts.admin-master')
@section('title', 'Withdrawals')
@section('page-styles')
    <link rel="stylesheet" href="{{asset('plugins/datatables-bs4/css/dataTables.bootstrap4.min.css')}}">
    <link rel="stylesheet" href="{{asset('plugins/datatables-responsive/css/responsive.bootstrap4.min.css')}}">
    <link rel="stylesheet" href="{{asset('plugins/datatables-buttons/css/buttons.bootstrap4.min.css')}}">
@stop

@section('page-title', 'Withdrawals')

@section('content')
    @if(session('message'))
        <div class="alert alert-{{ session('type') }} alert-dismissible fade show" role="alert">
            {{ session('message') }}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    @endif
    <div class="card">
        <!-- /.card-header -->
        <div class="card-body">
            <table id="example1" class="table table-bordered table-striped">
                <thead>
                <th>Ref ID</th>
                <th>Customer</th>
                <th>Amount</th>
                <th>Date</th>
                <th>Status</th>
                <th>Action</th>
                </thead>
                <tbody>
                @if ($withdrawals->count() > 0)
                    @foreach ($withdrawals as $withdrawal)
                        <tr>
                            <td>
                                <a href="{{route('admin.view-withdrawal-details', $withdrawal->ref_id)}}">{{$withdrawal->ref_id}}</a>
                            </td>
                            <td class="text-right">{{$withdrawal->customer->name}}</td>
                            <td class="text-right">$ {{$withdrawal->amount}}</td>
                            <td>{{$withdrawal->created_at->toFormattedDateString()}}</td>
                            <td>
                                @switch($withdrawal->status)
                                    @case(0)
                                    <span class="btn btn-xs btn-info">pending</span>
                                    @break
                                    @case(1)
                                    <span class="btn btn-xs btn-success">approved</span>
                                    @break
                                    @case(2)
                                    <span class="btn btn-xs btn-danger">cancelled</span>
                                    @break
                                    @default
                                    <span class="btn btn-xs btn-warning">failed</span>
                                @endswitch
                            </td>
                            <td>
                                <a href="#" data-toggle="modal" data-target="#modal-confirm"
                                   data-ref="{{$withdrawal->ref_id}}" data-type="withdrawal"
                                   class="btn btn-xs btn-danger"
                                >
                                    <i class="fas fa-trash"></i> Delete
                                </a>
                            </td>
                        </tr>
                    @endforeach
                @endif
                </tbody>
                <tfoot>
                <th>Ref ID</th>
                <th>Customer</th>
                <th>Amount</th>
                <th>Date</th>
                <th>Status</th>
                <th>Action</th>
                </tfoot>
            </table>
        </div>
    </div>
    {{--    Withdrawal delete form--}}
    <form action="" method="post" class="d-none" id="withdrawalDeleteForm">
        @method('DELETE')
        @csrf
    </form>
@stop

@section('modal-confirm')
    @include('includes.admin.confirm-modal')
@stop

@section('page-scripts')
    <script src="{{asset('plugins/datatables/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('plugins/datatables-bs4/js/dataTables.bootstrap4.min.js')}}"></script>
    <script src="{{asset('plugins/datatables-responsive/js/dataTables.responsive.min.js')}}"></script>
    <script src="{{asset('plugins/datatables-responsive/js/responsive.bootstrap4.min.js')}}"></script>
    <script src="{{asset('plugins/datatables-buttons/js/dataTables.buttons.min.js')}}"></script>
    <script src="{{asset('plugins/datatables-buttons/js/buttons.bootstrap4.min.js')}}"></script>
    <script src="{{asset('plugins/jszip/jszip.min.js')}}"></script>
    <script src="{{asset('plugins/pdfmake/pdfmake.min.js')}}"></script>
    <script src="{{asset('plugins/pdfmake/vfs_fonts.js')}}"></script>
    <script src="{{asset('plugins/datatables-buttons/js/buttons.html5.min.js')}}"></script>
    <script src="{{asset('plugins/datatables-buttons/js/buttons.print.min.js')}}"></script>
    <script src="{{asset('plugins/datatables-buttons/js/buttons.colVis.min.js')}}"></script>

    <script>
        $(function () {
            $("#example1").DataTable({
                "responsive": true, "lengthChange": false, "autoWidth": false,
                "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
            }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
            $('#example2').DataTable({
                "paging": true,
                "lengthChange": false,
                "searching": false,
                "ordering": true,
                "info": true,
                "autoWidth": false,
                "responsive": true,
            });
        });

        //Withdrawal delete logic
        var deleteModal = $('#modal-confirm');
        var submitButton = deleteModal.find('#action');
        var withdrawalDeleteForm = $('#withdrawalDeleteForm');

        deleteModal.on('show.bs.modal', function (event) {
            deleteModal.find('.modal-title').text('Confirm');
            deleteModal.find('#model').text('withdrawal');
            var btn = $(event.relatedTarget);
            var ref = btn.data('ref');

            withdrawalDeleteForm.attr('action', `/admin/withdrawals/${ref}/delete`);
        });

        submitButton.on('click', function (evt) {
            var button = $(this);
            button.attr('href', '/admin/withdrawals')

            evt.preventDefault();
            withdrawalDeleteForm.submit();
        })
    </script>
@endsection
