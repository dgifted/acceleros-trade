@extends('layouts.admin-master')
@section('title', 'Plans')
@section('page-styles')
    <link rel="stylesheet" href="{{asset('plugins/datatables-bs4/css/dataTables.bootstrap4.min.css')}}">
    <link rel="stylesheet" href="{{asset('plugins/datatables-responsive/css/responsive.bootstrap4.min.css')}}">
    <link rel="stylesheet" href="{{asset('plugins/datatables-buttons/css/buttons.bootstrap4.min.css')}}">
@stop

@section('page-title', 'Plans')

@section('content')
    @if(session('message'))
        <div class="alert alert-{{ session('type') }} alert-dismissible fade show" role="alert">
            {{ session('message') }}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    @endif
    <div class="card">
        <!-- /.card-header -->
        <div class="card-body">
            <table id="example1" class="table table-bordered table-striped">
                <thead>
                <th>Title</th>
                <th>Description</th>
                <th>Duration</th>
                <th>Min. Amount</th>
                <th>Max. Amount</th>
                <th>Action</th>
                </thead>
                <tbody>
                @if ($plans->count() > 0)
                    @foreach ($plans as $plan)
                        <tr>
                            <td>{{ $plan->title }}</td>
                            <td>{{$plan->description}}</td>
                            <td>{{ $plan->duration }} days</td>
                            <td class="text-right">$ {{$plan->min}}</td>
                            <td class="text-right">$ {{$plan->max}}</td>
                            <td>
                                <a href="{{ route('admin.plan-details', $plan->ref_id)}}" class="btn btn-xs btn-info">
                                    <i class="fas fa-edit"></i> Edit
                                </a>
                                <a href="javascript:void(0)"
                                   class="btn btn-xs btn-danger"
                                   data-toggle="modal"
                                   data-target="#modal-confirm"
                                   data-ref="{{ $plan->ref_id }}"
                                >
                                    <i class="fas fa-trash"></i> Delete
                                </a>
                            </td>
                        </tr>
                    @endforeach
                @endif
                </tbody>
                <tfoot>
                <th>Title</th>
                <th>Description</th>
                <th>Duration</th>
                <th>Min. Amount</th>
                <th>Max. Amount</th>
                <th>Action</th>
                </tfoot>
            </table>
        </div>
    </div>
    {{--    Deposit delete form--}}
    <form action="" method="post" class="d-none" id="planDeleteForm">
        @method('DELETE')
        @csrf
    </form>
@stop

@section('modal-confirm')
    @include('includes.admin.confirm-modal')
@stop

@section('page-scripts')
    <script src="{{asset('plugins/datatables/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('plugins/datatables-bs4/js/dataTables.bootstrap4.min.js')}}"></script>
    <script src="{{asset('plugins/datatables-responsive/js/dataTables.responsive.min.js')}}"></script>
    <script src="{{asset('plugins/datatables-responsive/js/responsive.bootstrap4.min.js')}}"></script>
    <script src="{{asset('plugins/datatables-buttons/js/dataTables.buttons.min.js')}}"></script>
    <script src="{{asset('plugins/datatables-buttons/js/buttons.bootstrap4.min.js')}}"></script>
    <script src="{{asset('plugins/jszip/jszip.min.js')}}"></script>
    <script src="{{asset('plugins/pdfmake/pdfmake.min.js')}}"></script>
    <script src="{{asset('plugins/pdfmake/vfs_fonts.js')}}"></script>
    <script src="{{asset('plugins/datatables-buttons/js/buttons.html5.min.js')}}"></script>
    <script src="{{asset('plugins/datatables-buttons/js/buttons.print.min.js')}}"></script>
    <script src="{{asset('plugins/datatables-buttons/js/buttons.colVis.min.js')}}"></script>

    <script>
        $(function () {
            $("#example1").DataTable({
                "responsive": true, "lengthChange": false, "autoWidth": false,
                "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
            }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
            $('#example2').DataTable({
                "paging": true,
                "lengthChange": false,
                "searching": false,
                "ordering": true,
                "info": true,
                "autoWidth": false,
                "responsive": true,
            });
        });

        //Deposit delete logic
        var deleteModal = $('#modal-confirm');
        var submitButton = deleteModal.find('#action');
        var planDeleteForm = $('#planDeleteForm');
        var _ref;

        deleteModal.on('show.bs.modal', function (event) {
            deleteModal.find('.modal-title').text('Confirm');
            deleteModal.find('#model').text('plan');
            var btn = $(event.relatedTarget);
            var ref = btn.data('ref');
            _ref = ref;

            planDeleteForm.attr('action', `/admin/plans/${ref}/delete`);
        });

        submitButton.on('click', function (evt) {
            var button = $(this);
            button.attr('href', '/admin/customers')

            evt.preventDefault();
            planDeleteForm.submit();
        })
    </script>
@endsection
