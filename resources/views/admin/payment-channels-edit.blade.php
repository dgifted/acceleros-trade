@extends('layouts.admin-master')
@section('title', 'Payment channel update')
@section('page-styles')

@stop

@section('page-title', 'Payment channel update')

@section('content')
    @if(session('message'))
        <div class="alert alert-{{ session('type') }} alert-dismissible fade show" role="alert">
            {{ session('message') }}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    @endif
    <div class="row">
        <div class="col-md-6 offset-md-3">
            <div class="card card-secondary">
                <div class="card-header">
                    <div class="d-flex justify-content-between">
                        <h3 class="card-title">Edit payment channel</h3>
                        <a class="btn btn-xs btn-dark" href="{{ route('admin.view-payment-channels') }}">
                            <i class="fas fa-arrow-left"></i>
                            Back
                        </a>
                    </div>
                </div>
                <!-- /.card-header -->
                <!-- form start -->
                <form name="frmWallet" action="{{route('admin.payment-channel-update', $channel->ref_id)}}"
                      method="POST">
                    @method('PATCH')
                    @csrf

                    <div class="card-body">
                        <div class="form-group">
                            <label for="name">Account name</label>
                            <input type="text" name="name" class="form-control @error('name') is-invalid @enderror" id="name"
                                   placeholder="Enter account name (e.g. Bitcoin)"
                                   value="{{ ucwords($channel->name) }}"
                            >
                            @error('name') <p class="text-danger text-sm">{{ $message }}</p> @enderror
                        </div>
                        <div class="form-group">
                            <label for="abbreviation">Wallet abbreviation</label>
                            <input type="text" name="abbreviation" class="form-control" id="abbreviation"
                                   placeholder="Enter wallet abbreviate (e.g. BTC)"
                                   value="{{ $channel->abbreviation ?? '' }}"
                            >
                        </div>
                        <div class="form-group">
                            <label for="wallet">Wallet address</label>
                            <input type="text" name="wallet" class="form-control @error('wallet') is-invalid @enderror" id="wallet"
                                   placeholder="Enter wallet address ..."
                                   value="{{ $channel->wallet }}"
                            >
                            @error('wallet') <p class="text-danger text-sm">{{ $message }}</p> @enderror
                        </div>
                    </div>
                    <!-- /.card-body -->
                    <div class="card-footer">
                        <button type="submit" class="btn btn-secondary">
                            <i class="fas fa-check ml-2"></i> Save changes
                        </button>
                    </div>
                </form>
            </div>
            <!-- /.card -->
        </div>
    </div>
@stop
