@extends('layouts.admin-master')
@section('title', 'Send out email')
@section('page-styles')
    <link rel="stylesheet" href="{{asset('plugins/datatables-bs4/css/dataTables.bootstrap4.min.css')}}">
    <link rel="stylesheet" href="{{asset('plugins/datatables-responsive/css/responsive.bootstrap4.min.css')}}">
    <link rel="stylesheet" href="{{asset('plugins/datatables-buttons/css/buttons.bootstrap4.min.css')}}">
    <link rel="stylesheet" href="{{ asset('plugins/chosen_v1.8.7/chosen.min.css') }}">

    <style>
        .note-editing-area {
            min-height: 400px !important;
        }

        .form-check {
            display: flex;
            align-items: baseline;
        }
    </style>
@stop

@section('page-title', 'New email')

@section('content')
    @if(session('message'))
        <div class="alert alert-{{ session('type') }} alert-dismissible fade show" role="alert">
            {{ session('message') }}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    @endif

    <div class="card">
        <!-- /.card-header -->
        <div class="card-body">
            <form action="{{route('admin.send-new-email')}}" method="POST" id="emailForm">
                @csrf

                <div class="row my-5">
                    <div class="col-12 col-md-8 offset-md-2">
                        <div class="form-group">
                            <label for="recipient">Recipients</label>
                            <select class="form-control"
                                    data-placeholder="Choose customers..."
                                    id="recipient" multiple name="recipients[]" required>
                                @foreach($users as $user)
                                    <option value="{{ $user->id }}">
                                        {{ $user->name }}
                                    </option>
                                @endforeach
                            </select>
                            <div class="form-check mt-2">
                                <input type="checkbox" class="form-check-input" id="all" name="option">
                                <label class="form-check-label text-muted" for="all" style="font-size: 0.9rem">Send to
                                    all customers</label>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="subject">Subject</label>
                            <input type="text" class="form-control" id="subject" name="subject" required
                                   value="{{ old('subject') }}">
                        </div>

                        <div class="form-group">
                            <label for="summernote">Message</label>
                            <textarea id="summernote" name="content"></textarea>
                            <input type="hidden" value="{{ old('content') ?? '' }}" id="content-value">
                        </div>

                        <div class="mt-3">
                            <button type="submit" class="btn btn-primary">Send email</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
        <!-- /.card-body -->
    </div>
@stop

@section('modal-confirm')
    @include('includes.admin.confirm-modal')
@stop

@section('page-scripts')
    <script src="{{asset('plugins/datatables/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('plugins/datatables-bs4/js/dataTables.bootstrap4.min.js')}}"></script>
    <script src="{{asset('plugins/datatables-responsive/js/dataTables.responsive.min.js')}}"></script>
    <script src="{{asset('plugins/datatables-responsive/js/responsive.bootstrap4.min.js')}}"></script>
    <script src="{{asset('plugins/datatables-buttons/js/dataTables.buttons.min.js')}}"></script>
    <script src="{{asset('plugins/datatables-buttons/js/buttons.bootstrap4.min.js')}}"></script>
    <script src="{{asset('plugins/jszip/jszip.min.js')}}"></script>
    <script src="{{asset('plugins/pdfmake/pdfmake.min.js')}}"></script>
    <script src="{{asset('plugins/pdfmake/vfs_fonts.js')}}"></script>
    <script src="{{asset('plugins/datatables-buttons/js/buttons.html5.min.js')}}"></script>
    <script src="{{asset('plugins/datatables-buttons/js/buttons.print.min.js')}}"></script>
    <script src="{{asset('plugins/datatables-buttons/js/buttons.colVis.min.js')}}"></script>
    <script src="{{ asset('plugins/chosen_v1.8.7/chosen.jquery.min.js') }}"></script>
    <script src="{{ asset('plugins/chosen_v1.8.7/chosen.proto.min.js') }}"></script>

    <script>
        $(function () {
            $("#example1").DataTable({
                "responsive": true, "lengthChange": false, "autoWidth": false,
                "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
            }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');

            $('#recipient').chosen();
            $('#summernote').summernote('pasteHTML', $('#content-value').val())
        });

        var allOption = $('#all');
        allOption.on('change', function () {
            var boxChecked = $(this).is(':checked');
            if (boxChecked) {
                $('#recipient').attr('required', false);
                $('#recipient_chosen').attr('style', 'height: 0; opacity: 0');
            }
            else {
                $('#recipient').attr('required', true);
                $('#recipient_chosen').attr('style', 'height: auto; opacity: 1; display: block');
            }
        });

        // $('#emailForm').on('submit', function (evt) {
        //     evt.preventDefault();
        //     console.log('recipients: ', $('#recipient').val());
        // });
    </script>
@endsection
