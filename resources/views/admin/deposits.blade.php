@extends('layouts.admin-master')
@section('title', 'Deposits')
@section('page-styles')
    <link rel="stylesheet" href="{{asset('plugins/datatables-bs4/css/dataTables.bootstrap4.min.css')}}">
    <link rel="stylesheet" href="{{asset('plugins/datatables-responsive/css/responsive.bootstrap4.min.css')}}">
    <link rel="stylesheet" href="{{asset('plugins/datatables-buttons/css/buttons.bootstrap4.min.css')}}">
@stop

@section('page-title', 'Deposits')

@section('content')
    @if(session('message'))
        <div class="alert alert-{{ session('type') }} alert-dismissible fade show" role="alert">
            {{ session('message') }}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    @endif
    <div class="card">
        <!-- /.card-header -->
        <div class="card-body">
            <table id="example1" class="table table-bordered table-striped">
                <thead>
                <th>Ref ID</th>
                <th>Customer</th>
                <th>Amount</th>
                <th>Date</th>
                <th>Status</th>
                <th>Investment Plan</th>
                <th>Action</th>
                </thead>
                <tbody>
                @if ($deposits->count() > 0)
                    @foreach ($deposits as $deposit)
                        <tr>
                            <td>
                                <a href="{{route('admin.view-deposit-details', $deposit->ref_id)}}">{{$deposit->ref_id}}</a>
                            </td>
                            <td class="text-right">{{$deposit->customer->name}}</td>
                            <td class="text-right">$ {{$deposit->amount}}</td>
                            <td>{{$deposit->created_at->toFormattedDateString()}}</td>
                            <td>
                                @switch($deposit->status)
                                    @case(0)
                                    <span class="btn btn-xs btn-info">pending</span>
                                    @break
                                    @case(1)
                                    <span class="btn btn-xs btn-success">approved</span>
                                    @break
                                    @case(2)
                                    <span class="btn btn-xs btn-danger">cancelled</span>
                                    @break
                                    @default
                                    <span class="btn btn-xs btn-warning">failed</span>
                                @endswitch
                            </td>
                            <td>
                                <span class="">{{ $deposit->plan->title }}</span>
                            </td>
                            <td>
                                <a href="#" data-toggle="modal" data-target="#modal-confirm"
                                   data-ref="{{$deposit->ref_id}}" data-type="investment"
                                   class="btn btn-xs btn-danger"
                                >
                                    <i class="fas fa-trash"></i> Delete
                                </a>
                            </td>
                        </tr>
                    @endforeach
                @endif
                </tbody>
                <tfoot>
                <th>Ref ID</th>
                <th>Customer</th>
                <th>Amount</th>
                <th>Date</th>
                <th>Status</th>
                <th>Investment Plan</th>
                <th>Action</th>
                </tfoot>
            </table>
        </div>
    </div>
    {{--    Deposit delete form--}}
    <form action="" method="post" class="d-none" id="depositDeleteForm">
        @method('DELETE')
        @csrf
    </form>
@stop

@section('modal-confirm')
    @include('includes.admin.confirm-modal')
@stop

@section('page-scripts')
    <script src="{{asset('plugins/datatables/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('plugins/datatables-bs4/js/dataTables.bootstrap4.min.js')}}"></script>
    <script src="{{asset('plugins/datatables-responsive/js/dataTables.responsive.min.js')}}"></script>
    <script src="{{asset('plugins/datatables-responsive/js/responsive.bootstrap4.min.js')}}"></script>
    <script src="{{asset('plugins/datatables-buttons/js/dataTables.buttons.min.js')}}"></script>
    <script src="{{asset('plugins/datatables-buttons/js/buttons.bootstrap4.min.js')}}"></script>
    <script src="{{asset('plugins/jszip/jszip.min.js')}}"></script>
    <script src="{{asset('plugins/pdfmake/pdfmake.min.js')}}"></script>
    <script src="{{asset('plugins/pdfmake/vfs_fonts.js')}}"></script>
    <script src="{{asset('plugins/datatables-buttons/js/buttons.html5.min.js')}}"></script>
    <script src="{{asset('plugins/datatables-buttons/js/buttons.print.min.js')}}"></script>
    <script src="{{asset('plugins/datatables-buttons/js/buttons.colVis.min.js')}}"></script>

    <script>
        $(function () {
            $("#example1").DataTable({
                "responsive": true, "lengthChange": false, "autoWidth": false,
                "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
            }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
        });

        //Deposit delete logic
        var deleteModal = $('#modal-confirm');
        var submitButton = deleteModal.find('#action');
        var depositDeleteForm = $('#depositDeleteForm');

        deleteModal.on('show.bs.modal', function (event) {
            deleteModal.find('.modal-title').text('Confirm');
            deleteModal.find('#model').text('deposit');
            var btn = $(event.relatedTarget);
            var ref = btn.data('ref');

            depositDeleteForm.attr('action', `/admin/deposits/${ref}/delete`);
        });

        submitButton.on('click', function (evt) {
            var button = $(this);
            button.attr('href', '/admin/customers')

            evt.preventDefault();
            depositDeleteForm.submit();
        })
    </script>
@endsection
