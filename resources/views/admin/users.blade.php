@extends('layouts.admin-master')
@section('title', 'Customers')
@section('page-styles')
    <link rel="stylesheet" href="{{asset('plugins/datatables-bs4/css/dataTables.bootstrap4.min.css')}}">
    <link rel="stylesheet" href="{{asset('plugins/datatables-responsive/css/responsive.bootstrap4.min.css')}}">
    <link rel="stylesheet" href="{{asset('plugins/datatables-buttons/css/buttons.bootstrap4.min.css')}}">
@stop

@section('page-title', 'Customers')

@section('content')
    @if(session('message'))
        <div class="alert alert-{{ session('type') }} alert-dismissible fade show" role="alert">
            {{ session('message') }}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    @endif

    <div class="card">
        <div class="card-body">
            <table id="example1" class="table table-bordered table-striped">
                <thead>
                <th>Name</th>
                <th>Email</th>
                <th>Username</th>
                <th>Date Registered</th>
                <th>Verification Status</th>
                <th>Action</th>
                </thead>
                <tbody>
                @if ($users->count() > 0)
                    @foreach ($users as $user)
                        <tr>
                            <td>
                                <a href="{{ route('admin.view-customer-details', $user->uid) }}">
                                    {{$user->name}}
                                </a>
                            </td>
                            <td class="text-right">{{$user->email}}</td>
                            <td class="text-capitalize"><span>@</span>{{ $user->username }}</td>
                            <td>{{$user->created_at->toFormattedDateString()}}</td>
                            <td>{!!$user->isVerified()
                                ? '<span class="btn btn-xs btn-success">Verified</span>'
                                : '<span class="btn btn-xs btn-danger">Not verified</span>'!!}
                            </td>
                            <td>
                                <a href="#" class="btn btn-xs btn-danger" id="user-delete-btn"
                                   data-uid="{{ $user->uid }}" data-toggle="modal" data-target="#modal-confirm">
                                    <i class="fas fa-trash"></i> Delete
                                </a>
                            </td>
                        </tr>
                    @endforeach
                @endif
                </tbody>
                <tfoot>
                <th>Name</th>
                <th>Email</th>
                <th>Username</th>
                <th>Date Registered</th>
                <th>Verification Status</th>
                <th>Action</th>
                </tfoot>
            </table>
        </div>
    </div>
    {{--    User delete form--}}
    <form action="" method="post" class="d-none" id="userDeleteForm">
        @method('DELETE')
        @csrf
    </form>
@stop

@section('modal-confirm')
    @include('includes.admin.confirm-modal')
@stop

@section('page-scripts')
    <script src="{{asset('plugins/datatables/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('plugins/datatables-bs4/js/dataTables.bootstrap4.min.js')}}"></script>
    <script src="{{asset('plugins/datatables-responsive/js/dataTables.responsive.min.js')}}"></script>
    <script src="{{asset('plugins/datatables-responsive/js/responsive.bootstrap4.min.js')}}"></script>
    <script src="{{asset('plugins/datatables-buttons/js/dataTables.buttons.min.js')}}"></script>
    <script src="{{asset('plugins/datatables-buttons/js/buttons.bootstrap4.min.js')}}"></script>
    <script src="{{asset('plugins/jszip/jszip.min.js')}}"></script>
    <script src="{{asset('plugins/pdfmake/pdfmake.min.js')}}"></script>
    <script src="{{asset('plugins/pdfmake/vfs_fonts.js')}}"></script>
    <script src="{{asset('plugins/datatables-buttons/js/buttons.html5.min.js')}}"></script>
    <script src="{{asset('plugins/datatables-buttons/js/buttons.print.min.js')}}"></script>
    <script src="{{asset('plugins/datatables-buttons/js/buttons.colVis.min.js')}}"></script>

    <script>
        $(function () {
            $("#example1").DataTable({
                "responsive": true, "lengthChange": false, "autoWidth": false,
                "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
            }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
            $('#example2').DataTable({
                "paging": true,
                "lengthChange": false,
                "searching": false,
                "ordering": true,
                "info": true,
                "autoWidth": false,
                "responsive": true,
            });

            //User delete logic
            var deleteModal = $('#modal-confirm');
            var submitButton = deleteModal.find('#action');
            var userDeleteForm = $('#userDeleteForm');

            deleteModal.on('show.bs.modal', function (event) {
                deleteModal.find('.modal-title').text('Confirm');
                deleteModal.find('#model').text('user');
                var btn = $(event.relatedTarget);
                var userUid = btn.data('uid');

                userDeleteForm.attr('action', `/admin/customers/${userUid}`);
            });

            submitButton.on('click', function (evt) {
                var button = $(this);
                button.attr('href', '/admin/customers')

                evt.preventDefault();
                userDeleteForm.submit();
            })
        });
    </script>
@endsection
