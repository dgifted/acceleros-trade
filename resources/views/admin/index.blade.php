@extends('layouts.admin-master')
@section('title', 'Admin Dashboard')
@section('page-styles')
@stop

@section('page-title', 'Dashboard')

@section('content')
    <div class="row">
        <div class="col-12 col-sm-6 col-md-3">
            <div class="info-box">
                <span class="info-box-icon bg-info elevation-1"><i class="fas fa-hand-holding-usd"></i></span>

                <div class="info-box-content">
                    <span class="info-box-text">Total deposits</span>
                    <span class="info-box-number">
                        $ {{ $depositedAmount }}.00
                        <small>&nbsp;</small>
                    </span>
                </div>
                <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
        </div>
        <!-- /.col -->
        <div class="col-12 col-sm-6 col-md-3">
            <div class="info-box mb-3">
                <span class="info-box-icon bg-danger elevation-1"><i class="fab fa-bitcoin"></i></span>

                <div class="info-box-content">
                    <span class="info-box-text">Total interests</span>
                    <span class="info-box-number">$ {{ $accumulatedInterest }}.00</span>
                </div>
                <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
        </div>
        <!-- /.col -->

        <!-- fix for small devices only -->
        <div class="clearfix hidden-md-up"></div>

        <div class="col-12 col-sm-6 col-md-3">
            <div class="info-box mb-3">
                <span class="info-box-icon bg-success elevation-1"><i class="fas fa-business-time"></i></span>

                <div class="info-box-content">
                    <span class="info-box-text">Due investments</span>
                    <span class="info-box-number">$ {{ $dueDeposit }}.00</span>
                </div>
                <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
        </div>
        <!-- /.col -->
        <div class="col-12 col-sm-6 col-md-3">
            <div class="info-box mb-3">
                <span class="info-box-icon bg-warning elevation-1"><i class="fas fa-users"></i></span>

                <div class="info-box-content">
                    <span class="info-box-text">Total registered users</span>
                    <span class="info-box-number">{{ $usersCount }}</span>
                </div>
                <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
        </div>
        <!-- /.col -->
    </div>

    <div class="row">
        <div class="col-md-12">
            <!-- Bar chart -->
            <div class="card card-primary card-outline">
                <div class="card-header">
                    <h3 class="card-title">
{{--                        <i class="far fa-chart-bar"></i>--}}
                        Total in/out
                    </h3>

                    <div class="card-tools">
                        <button type="button" class="btn btn-tool" data-card-widget="collapse">
                            <i class="fas fa-minus"></i>
                        </button>
                        <button type="button" class="btn btn-tool" data-card-widget="remove">
                            <i class="fas fa-times"></i>
                        </button>
                    </div>
                </div>
                <div class="card-body">
                    <div id="bar-chart" style="height: 300px;"></div>
                </div>
                <!-- /.card-body-->
            </div>
            <!-- /.card -->
        </div>
    </div>

    <div class="row">
        <div class="col-12 col-sm-12 col-md-8">
            <!-- TABLE: LATEST ORDERS -->
            <div class="card">
                <div class="card-header border-transparent">
                    <h3 class="card-title">Recent Deposits</h3>

                    <div class="card-tools">
                        <button type="button" class="btn btn-tool" data-card-widget="collapse">
                            <i class="fas fa-minus"></i>
                        </button>
                        <button type="button" class="btn btn-tool" data-card-widget="remove">
                            <i class="fas fa-times"></i>
                        </button>
                    </div>
                </div>
                <!-- /.card-header -->
                <div class="card-body p-0">
                    <div class="table-responsive">
                        <table class="table m-0">
                            <thead>
                            <tr>
                                <th>Ref ID</th>
                                <th>Customer</th>
                                <th>Amount</th>
                                <th>Date</th>
                                <th>Status</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($recentDeposits as $deposit)
                                <tr>
                                    <td><a
                                            href="{{ route('admin.view-deposit-details', $deposit->ref_id) }}">{{ $deposit->ref_id }}</a>
                                    </td>
                                    <td>{{ $deposit->customer->name }}</td>
                                    <td class="text-right"><i
                                            class="fas fa-dollar-sign mr-2"></i>{{ $deposit->amount }}.00
                                    </td>
                                    <td>{{ $deposit->created_at->toFormattedDateString() }}</td>
                                    <td>
                                        @switch($deposit->status)
                                            @case(0)
                                            <span class="badge badge-info">pending</span>
                                            @break
                                            @case(1)
                                            <span class="badge badge-success">approved</span>
                                            @break
                                            @case(2)
                                            <span class="badge badge-danger">cancelled</span>
                                            @break
                                            @default
                                            <span class="badge badge-primary">failed</span>
                                        @endswitch
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <!-- /.table-responsive -->
                </div>
                <!-- /.card-body -->
                <div class="card-footer clearfix">
                    <a href="javascript:void(0)" class="btn btn-xs btn-light float-left">&nbsp;</a>
                    <a href="{{ route('admin.view-deposits') }}" class="btn btn-xs btn-secondary float-right">View all
                        deposits</a>
                </div>
                <!-- /.card-footer -->
            </div>
            <!-- /.card -->
        </div>
        <div class="col-12 col-sm-12 col-md-4">
            <!-- PRODUCT LIST -->
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Recently Registered Users</h3>

                    <div class="card-tools">
                        <button type="button" class="btn btn-tool" data-card-widget="collapse">
                            <i class="fas fa-minus"></i>
                        </button>
                        <button type="button" class="btn btn-tool" data-card-widget="remove">
                            <i class="fas fa-times"></i>
                        </button>
                    </div>
                </div>
                <!-- /.card-header -->
                <div class="card-body p-0">
                    <ul class="products-list product-list-in-card pl-2 pr-2">
                        @foreach ($recentCustomers as $customer)
                            <li class="item">
                                <div class="product-img">
                                    <img src="{{ $customer->avatar_path }}" alt="investor avatar" class="img-size-50">
                                </div>
                                <div class="product-info">
                                    <a href="{{ route('admin.view-customer-details', $customer->uid) }}"
                                       class="product-title">{{ $customer->name }}
                                    </a>
                                    <small class="product-description">Registered on:
                                        {{ $customer->created_at->toFormattedDateString() }}.
                                    </small>
                                </div>
                            </li>
                        @endforeach
                    </ul>
                </div>
                <!-- /.card-body -->
                <div class="card-footer text-center">
                    <a href="#" class="uppercase">View all users<i
                            class="fas fa-chevron-right ml-2"></i></a>
                </div>
                <!-- /.card-footer -->
            </div>
            <!-- /.card -->
        </div>

    </div>
@stop

@section('modal-confirm')
@stop

@section('page-scripts')
    <script src="{{ asset('plugins/flot/jquery.flot.js') }}"></script>
    <script src={{ asset('plugins/flot/plugins/jquery.flot.resize.js') }}"></script>
    <script src="{{ asset('plugins/flot/plugins/jquery.flot.pie.js') }}"></script>
    <script>
          var bar_data = {
            data: [[1, 10], [2, 8], [3, 4], [4, 13], [5, 17], [6, 9]],
            bars: {show: true}
        };
        $.plot('#bar-chart', [bar_data], {
            grid: {
                borderWidth: 1,
                borderColor: '#f3f3f3',
                tickColor: '#f3f3f3'
            },
            series: {
                bars: {
                    show: true, barWidth: 0.5, align: 'center',
                },
            },
            colors: ['#3c8dbc'],
            xaxis: {
                ticks: [[1, 'January'], [2, 'February'], [3, 'March'], [4, 'April'], [5, 'May'], [6, 'June']]
            }
        })
    </script>
@endsection
