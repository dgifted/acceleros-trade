@extends('layouts.admin-master')
@section('title', 'Customer Details')
@section('page-styles')
    <link rel="stylesheet" href="{{asset('plugins/datatables-bs4/css/dataTables.bootstrap4.min.css')}}">
    <link rel="stylesheet" href="{{asset('plugins/datatables-responsive/css/responsive.bootstrap4.min.css')}}">
    <link rel="stylesheet" href="{{asset('plugins/datatables-buttons/css/buttons.bootstrap4.min.css')}}">
@stop

@section('page-title', 'Customer Details')

@section('content')
    <div class="row">
        <div class="col-md-3">
            <!-- Profile Image -->
            <div class="card card-primary card-outline">
                <div class="card-body box-profile">
                    <div class="text-center">
                        <img class="profile-user-img img-fluid img-circle"
                             src="{{$user->avatar_path}}"
                             alt="User profile picture">
                    </div>

                    <h3 class="profile-username text-center">{{$user->name}}</h3>

                    <p class="text-muted text-center">
                        @if ($user->isActive())
                            {!!$user->isVerified()
                            ? '<span class="text-success p-2">Verified</span>'
                            : '<span class="text-warning p-2">Not verified</span>'!!}
                        @else
                            <span class="text-danger p-2">Deactivated</span>
                        @endif

                    </p>

                    <ul class="list-group list-group-unbordered mb-3">
                        <li class="list-group-item">
                            <b>Deposits</b> <a class="float-right">${{ $depositedAmount }}</a>
                        </li>
                        <li class="list-group-item">
                            <b>Earning</b> <a class="float-right">${{ $earnings }}</a>
                        </li>
                        <li class="list-group-item">
                            <b>Withdrawal</b> <a class="float-right">${{ $withdrawnAmount }}</a>
                        </li>
                    </ul>

                    @if (!$user->isVerified())
                        <a href="{{ route('admin.customer-verify', $user->uid) }}"
                           onclick="event.preventDefault(); document.getElementById('verifyForm').submit();"
                           class="btn btn-success btn-block text-white mb-2">
                            <i class="fas fa-check mr-2"></i>
                            <b class="">Verify User</b>
                        </a>
                        <form class="d-none" id="verifyForm" action="{{ route('admin.customer-verify', $user->uid) }}"
                              method="post">
                            @csrf
                        </form>
                    @endif

                    @if ($user->isActive())
                        <a href="{{ route('admin.customer-deactivate', $user->uid) }}"
                           onclick="event.preventDefault(); document.getElementById('deactivateForm').submit();"
                           class="btn btn-warning btn-block text-white">
                            <i class="fas fa-exclamation-triangle mr-2"></i>
                            <b class="">Deactivate Account</b>
                        </a>
                        <form class="d-none" id="deactivateForm"
                              action="{{ route('admin.customer-deactivate', $user->uid) }}" method="post">
                            @csrf
                        </form>
                    @else
                        <a href="{{ route('admin.customer-activate', $user->uid) }}"
                           onclick="event.preventDefault(); document.getElementById('activateForm').submit();"
                           class="btn btn-success btn-block text-white">
                            <i class="fas fa-unlock-alt mr-2"></i>
                            <b class="">Activate Account</b>
                        </a>
                        <form class="d-none" id="activateForm"
                              action="{{ route('admin.customer-activate', $user->uid) }}" method="post">
                            @csrf
                        </form>
                    @endif

                </div>
                <!-- /.card-body -->
            </div>
            <!-- /.card -->

        </div>
        <div class="col-md-9">
            @if(session('message'))
                <div class="alert alert-{{ session('type') }} alert-dismissible fade show" role="alert">
                    {{ session('message') }}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            @endif
            <div class="card">
                <div class="card-header p-2">
                    <ul class="nav nav-pills">
                        <li class="nav-item"><a class="nav-link active" href="#settings" data-toggle="tab">Profile</a>
                        </li>
                        <li class="nav-item"><a class="nav-link" href="#transactions" data-toggle="tab">Transactions</a>
                        </li>
                        <li class="nav-item"><a class="nav-link" href="#bonuses" data-toggle="tab">Bonuses</a>
                        </li>
                    </ul>
                </div>
                <div class="card-body  overflow-auto" style="max-height: 500px;">
                    <div class="tab-content">

                        <div class="tab-pane active" id="settings">
                            <form method="POST"
                                  class="form-horizontal">
                                @csrf

                                <div class="form-group row">
                                    <label for="name" class="col-sm-2 col-form-label">{{__('Name')}}</label>
                                    <div class="col-sm-10">
                                        <input type="text" name="name" class="form-control" id="name" placeholder="Name"
                                               value="{{$user->name}}">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="email" class="col-sm-2 col-form-label">{{__('Email')}}</label>
                                    <div class="col-sm-10">
                                        <input type="email" name="email" class="form-control" id="email"
                                               placeholder="Email"
                                               value="{{ $user->email }}" readonly>
                                    </div>
                                </div>
                                <hr>

                                @foreach($channels as $channel)
                                    <div class="form-group row">
                                        <label for="{{ $channel->slug }}"
                                               class="col-sm-2 col-form-label text-capitalize">{{ $channel->name }}</label>
                                        <div class="col-sm-10">
                                            <input type="text"
                                                   name="{{ $channel->slug }}"
                                                   class="form-control"
                                                   id="{{ $channel->slug }}"
                                                   placeholder="Enter {{ $channel->name }} account"
                                                   value="{{ $channel->address ?? '' }}"
                                            >
                                        </div>
                                    </div>
                                @endforeach

                                <div class="form-group row">
                                    <div class="offset-sm-2 col-sm-10">
                                        <button type="submit" class="btn btn-primary">
                                            <i class="fas fa-save mr-2"></i>
                                            Update
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="tab-pane" id="transactions">
                            <table id="example1" class="table table-bordered table-striped">
                                <thead>
                                <th>Ref ID</th>
                                <th>Amount</th>
                                <th>Date</th>
                                <th>Status</th>
                                <th>Transaction type</th>
                                </thead>
                                <tbody>
                                @if ($transactions->count() > 0)
                                    @foreach ($transactions as $transaction)
                                        <tr>
                                            <td><a href=" @if ($transaction->type === 'deposit')
                                                {{route('admin.view-deposit-details', $transaction->ref_id)}}
                                                @else
                                                {{route('admin.view-withdrawal-details', $transaction->ref_id)}}
                                                @endif ">{{$transaction->ref_id}}</a></td>
                                            <td class="text-right">$ {{$transaction->amount}}</td>
                                            <td>{{$transaction->date->toFormattedDateString()}}</td>
                                            <td>
                                                @switch($transaction->status)
                                                    @case('approved')
                                                    <span
                                                        class="badge badge-xs badge-success">{{ $transaction->status }}</span>
                                                    @break
                                                    @case('cancelled')
                                                    <span
                                                        class="badge badge-xs badge-danger">{{ $transaction->status }}</span>
                                                    @break
                                                    @case('pending')
                                                    <span
                                                        class="badge badge-xs badge-info">{{ $transaction->status }}</span>
                                                    @break
                                                    @default
                                                    <span
                                                        class="badge badge-xs badge-warning">{{ $transaction->status }}</span>
                                                @endswitch
                                            </td>
                                            <td>
                                                <span class="badge
                                                    @if ($transaction->type === 'deposit')
                                                {{'badge-success'}}
                                                @endif
                                                @if($transaction->type === 'withdrawal')
                                                {{'badge-warning'}}
                                                @endif ">{{ $transaction->type === 'deposit' ? 'Deposit' : 'Withdrawal' }}</span>

                                            </td>
                                        </tr>
                                    @endforeach
                                @endif
                                </tbody>
                                <tfoot>
                                <th>Ref ID</th>
                                <th>Amount</th>
                                <th>Date</th>
                                <th>Status</th>
                                <th>Investment Plan</th>
                                </tfoot>
                            </table>

                        </div>
                        <div class="tab-pane" id="bonuses">
                            <table id="example2" class="table table-bordered table-striped">
                                <thead>
                                <th>S/N</th>
                                <th>Amount</th>
                                <th>Date</th>
                                <th>For</th>
                                <th>Message</th>
                                <th>Status</th>
                                <th>Action</th>
                                </thead>
                                <tbody>
                                @if ($user->bonuses->count() > 0)
                                    @php
                                        $count = 0
                                    @endphp

                                    @foreach ($user->bonuses as $bonus)
                                        @php
                                            $count++
                                        @endphp
                                        <tr>
                                            <td>{{ $count }}</td>
                                            <td class="text-right">$ {{$bonus->amount}}</td>
                                            <td>{{$bonus->created_at->toFormattedDateString()}}</td>
                                            <td>{{ $bonus->for ?? 'N/A' }}</td>
                                            <td>{{ $bonus->description }}</td>
                                            <td>
                                                @switch($bonus->withdrawn)
                                                    @case(\App\Models\Bonus::WITHDRAWN)
                                                    <span
                                                        class="badge badge-xs badge-info">Withdrawn</span>
                                                    @break
                                                    @default
                                                    <span
                                                        class="badge badge-xs badge-success">Not withdrawn</span>
                                                @endswitch
                                            </td>
                                            <td>
                                                <button class="btn btn-sm btn-danger"
                                                        data-toggle="modal"
                                                        data-target="#modal-confirm"
                                                        data-bonus-ref="{{ $bonus->ref_id }}"
                                                >Delete
                                                </button>
                                            </td>
                                        </tr>
                                    @endforeach
                                @endif
                                </tbody>
                                <tfoot>
                                <th>S/N</th>
                                <th>Amount</th>
                                <th>Date</th>
                                <th>For</th>
                                <th>Message</th>
                                <th>Status</th>
                                <th>Action</th>
                                </tfoot>
                            </table>

                            <hr class="my-5">
                            <h5 class="mb-2">Add Bonus</h5>
                            <form method="post" action="{{ route('admin.customer-add-bonus', $user->uid) }}">
                                @csrf

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="amount">Amount</label>
                                            <input type="number" name="amount" class="form-control" id="amount"
                                                   placeholder="100"
                                                   value="{{ old('amount') }}" required>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="for">For</label>
                                            <input type="text" name="for" class="form-control" id="for"
                                                   placeholder="Registration"
                                                   value="{{old('for')}}">
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="description">Description</label>
                                    <textarea name="description" class="form-control" id="description" rows="5"
                                              placeholder="registering newly on our platform"
                                              required>{{ old('description') }}</textarea>
                                </div>

                                <div class="form-group">
                                    <label for="expire_days">Expires in(days)</label>
                                    <input type="number" name="expire_days" class="form-control" id="expire_days"
                                           placeholder="3"
                                           value="{{old('expire_days')}}">
                                </div>

                                <button type="submit" class="btn btn-primary">Add bonus</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('modal-confirm')
    @include('includes.admin.confirm-modal')
@stop

@section('page-scripts')
    <script src="{{asset('plugins/datatables/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('plugins/datatables-bs4/js/dataTables.bootstrap4.min.js')}}"></script>
    <script src="{{asset('plugins/datatables-responsive/js/dataTables.responsive.min.js')}}"></script>
    <script src="{{asset('plugins/datatables-responsive/js/responsive.bootstrap4.min.js')}}"></script>
    <script src="{{asset('plugins/datatables-buttons/js/dataTables.buttons.min.js')}}"></script>
    <script src="{{asset('plugins/datatables-buttons/js/buttons.bootstrap4.min.js')}}"></script>
    <script src="{{asset('plugins/jszip/jszip.min.js')}}"></script>
    <script src="{{asset('plugins/pdfmake/pdfmake.min.js')}}"></script>
    <script src="{{asset('plugins/pdfmake/vfs_fonts.js')}}"></script>
    <script src="{{asset('plugins/datatables-buttons/js/buttons.html5.min.js')}}"></script>
    <script src="{{asset('plugins/datatables-buttons/js/buttons.print.min.js')}}"></script>
    <script src="{{asset('plugins/datatables-buttons/js/buttons.colVis.min.js')}}"></script>

    <script>
        $(function () {
            $("#example1").DataTable({
                "responsive": true, "lengthChange": false, "autoWidth": false,
                "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
            }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
            $('#example2').DataTable({
                "paging": true,
                "lengthChange": false,
                "searching": false,
                "ordering": true,
                "info": true,
                "autoWidth": false,
                "responsive": true,
            });
        });

        var bonusDeleteModal = $('#modal-confirm');
        bonusDeleteModal.on('show.bs.modal', function (evt) {
            bonusDeleteModal.find('#modalTitle').text('Bonus');
            bonusDeleteModal.find('#model').text('bonus');
            var triggerButton = $(evt.relatedTarget);
            var bonusRef = triggerButton.data('bonusRef');
            bonusDeleteModal.find('#action').attr('href', `/admin/remove-bonus/${bonusRef}`);
        });
    </script>
@endsection
