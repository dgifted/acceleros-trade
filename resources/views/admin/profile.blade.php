@extends('layouts.admin-master')
@section('title', 'Admin profile')
@section('page-styles')
@stop

@section('page-title', 'Admin profile')

@section('content')
    <div class="row">
        <div class="col-md-3">
            <div class="card card-primary card-outline">
                <div class="card-body box-profile">
                    <div class="text-center">
                        <img class="profile-user-img img-fluid img-circle"
                             src="{{$user->avatar_path}}"
                             alt="User profile picture">
                    </div>

                    <h3 class="profile-username text-center">{{$user->name}}</h3>

                    <p class="text-muted text-center">
                        Admin
                    </p>
                </div>
                <!-- /.card-body -->
            </div>
            <div>
                <a href="#" class="btn btn-outline-primary btn-block rounded-10" data-toggle="modal"
                   data-target="#picture-modal">Upload profile picture</a>
            </div>
            <!-- /.card -->
        </div>

        <div class="col-md-9">
            <div class="card">
                <div class="card-header">
                    <div class="d-flex justify-content-between">
                        <h3 class="card-title mt-1 text-bold">Personal Information</h3>

                        <button type="button" class="btn btn-secondary btn-sm px-2" id="btnEdit">
                            Edit<i class="fas fa-user-edit ml-2"></i>
                        </button>
                    </div>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                    <form class="form-horizontal" name="frmProfile" id="frmProfile">
                        @csrf

                        <div class="form-group row">
                            <label for="name" class="col-sm-2 col-form-label">Name</label>
                            <div class="col-sm-10">
                                <input type="text" name="name" class="form-control" id="name" placeholder="Name"
                                       value="{{$user->name}}" disabled>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="email" class="col-sm-2 col-form-label">Email</label>
                            <div class="col-sm-10">
                                <input type="email" name="email" class="form-control" id="email" placeholder="Email"
                                       value="{{$user->email}}" disabled>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-sm-2 col-form-label">Password</label>
                            <div class="col-sm-7">
                                <input type="password" name="password" class="form-control" id="password"
                                       placeholder="Wallet Address"
                                       value="{{'**********'}}" disabled>
                            </div>
                            <div class="col-sm-3">
                                <button class="btn btn-outline-secondary btn-block" id="changePassword"
                                        type="button" disabled data-toggle="modal" data-target="#password-modal">Change
                                    password
                                </button>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="offset-sm-2 col-sm-10">
                                <button type="submit" id="submit" class="btn btn-primary" disabled>
                                    <i class="fas fa-save mr-2"></i>
                                    Save changes
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        {{--        Profile avatar upload original position--}}
    </div>

    @include('includes.admin.picture-upload-modal')
    @include('includes.admin.password-change-modal')
@stop

@section('modal-confirm')
@stop

@section('page-scripts')
    <script src="{{ asset('assets/js/toastr.min.js') }}"></script>
    <script src="{{ asset('assets/js/alert.controller.js') }}"></script>
    <script src="{{asset('dist/js/profile_edit.js')}}"></script>
@endsection
