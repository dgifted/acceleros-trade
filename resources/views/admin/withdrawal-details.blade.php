@extends('layouts.admin-master')
@section('title', 'Withdrawal Details')
@section('page-styles')

@stop

@section('page-title', 'Withdrawal Details')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-4 offset-md-4">
                                    <input type="text" name="ref_id" id="ref_id" class="form-control"
                                           value="{!!$withdrawal->ref_id!!}">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <div class="d-flex justify-content-between">
                        <h3 class="card-title mt-2">Investment Detail</h3>
                        <div>
                            @if ($withdrawal->status === 0)
                                <a href="{{route('admin.withdrawal-approve', $withdrawal->ref_id)}}"
                                   class="btn btn-secondary btn-xs" id="approvalButton">Approve
                                    <i class="fas fa-check ml-2"></i>
                                </a>
                            @endif
                        </div>

                    </div>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-4">
                            <!-- Widget: user widget style 1 -->
                            <div class="card card-widget widget-user">
                                <!-- Add the bg color to the header using any of the bg-* classes -->
                                <div class="widget-user-header bg-info">
                                    <h3 class="widget-user-username">{{$withdrawal->customer->name}}</h3>
                                    <h5 class="widget-user-desc">{{$withdrawal->customer->email}}</h5>
                                </div>
                                <div class="widget-user-image">
                                    <img class="img-circle elevation-2" src="{{$withdrawal->customer->avatar_path}}"
                                         alt="User Avatar">
                                </div>
                                <div class="card-footer">
                                    <div class="row">
                                        <div class="col-sm-6 offset-md-3">
                                            <div class="description-block">
                                                <h5 class="description-header">Register on:</h5>
                                                <span
                                                    class="description-text text-sm">{{ $withdrawal->customer->created_at->diffForHumans() }}</span>
                                            </div>
                                            <!-- /.description-block -->
                                        </div>
                                        <!-- /.col -->
                                        <!-- /.col -->
                                    </div>
                                    <!-- /.row -->
                                </div>
                            </div>
                            <!-- /.widget-user -->
                        </div>
                        <div class="col-md-8">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="info-box bg-light">
                                        <div class="info-box-content">
                                            <span class="info-box-text text-center text-muted">Amount</span>
                                            <span class="info-box-number text-center text-muted mb-0"><i
                                                    class="fab fa-bitcoin text-warning"></i> {{$withdrawal->amount}}</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="info-box bg-light">
                                        <div class="info-box-content">
                                            <span class="info-box-text text-center text-muted">Investment status</span>
                                            <span
                                                class="info-box-number text-center @if($withdrawal->status === 0) text-warning @elseif($withdrawal->status === 1) text-success @else text-danger @endif mb-0">
                                                @switch($withdrawal->status)
                                                    @case(0)
                                                    pending
                                                    @break
                                                    @case(1)
                                                    approved
                                                    @break
                                                    @case(2)
                                                    cancelled
                                                    @break
                                                    @default
                                                    failed
                                                @endswitch
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <form class="d-none" id="approvalForm" method="post" action="{{ route('admin.withdrawal-approve', $withdrawal->ref_id) }}">
        @csrf
    </form>
@stop

@section('modal-confirm')
@stop

@section('page-scripts')
<script>
    var approvalButton = document.getElementById('approvalButton');
    approvalButton.addEventListener('click', (evt) => {
        evt.preventDefault();
        var approvalForm = document.getElementById('approvalForm');
        approvalForm.submit();
    });
</script>
@endsection
