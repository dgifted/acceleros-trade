@extends('layouts.admin-master')
@section('title', 'Sent emails')
@section('page-styles')
    <link rel="stylesheet" href="{{asset('plugins/datatables-bs4/css/dataTables.bootstrap4.min.css')}}">
    <link rel="stylesheet" href="{{asset('plugins/datatables-responsive/css/responsive.bootstrap4.min.css')}}">
    <link rel="stylesheet" href="{{asset('plugins/datatables-buttons/css/buttons.bootstrap4.min.css')}}">
@stop

@section('page-title', 'Emails')

@section('content')
    @if(session('message'))
        <div class="alert alert-{{ session('type') }} alert-dismissible fade show" role="alert">
            {{ session('message') }}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    @endif

    <div class="card">
        <div class="card-header">
            <div class="text-right">
                <a class="btn btn-sm btn-primary" href="{{ route('admin.send-new-email') }}">Send email</a>
            </div>
        </div>
        <div class="card-body">
            <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th>S/N</th>
                    <th>Recipient</th>
                    <th>Subject</th>
                    <th>Preview</th>
                    <th>Created on</th>
                    <th>Actions</th>
                </tr>
                </thead>
                <tbody>
                @php
                    $count = 0;
                @endphp
                @if ($emails->count() > 0)
                    @foreach ($emails as $email)
                        @php
                            $count++;
                        @endphp
                        <tr>
                            <td>{{ $count }}</td>
                            <td>
                                <a href="{{ route('admin.view-customer-details', $email->recipient->uid) }}">
                                    {{ $email->recipient->name }}
                                </a>
                            </td>
                            <td>{{ $email->subject }}</td>
                            <td>
                                {{ $email->excerpt }}
                            </td>
                            <td>
                                {{ $email->created_at->toFormattedDateString() }}
                            </td>
                            <td>
                                <div class="dropdown">
                                    <button class="btn btn-light dropdown-toggle" type="button"
                                            id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true"
                                            aria-expanded="false">
                                        <i class="fas fa-ellipsis-v"></i>
                                    </button>

                                    <div class="dropdown-menu dropdown-menu-right"
                                         aria-labelledby="dropdownMenu2">
                                        <button class="dropdown-item">
                                            <a class="text-info"
                                               href="{{ route('admin.sent-emails-delete', $email->ref_id) }}">
                                                <i class="fas fa-pen-square mr-1"></i> Delete
                                            </a>
                                        </button>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                @endif
                </tbody>

                <tfoot>
                <tr>
                    <th>S/N</th>
                    <th>Recipient</th>
                    <th>Subject</th>
                    <th>Preview</th>
                    <th>Created on</th>
                    <th>Actions</th>
                </tr>
                </tfoot>
            </table>
        </div>
        <!-- /.card-body -->
    </div>
    {{--    Deposit delete form--}}
    <form action="" method="post" class="d-none" id="depositDeleteForm">
        @method('DELETE')
        @csrf
    </form>
@stop

@section('modal-confirm')
    @include('includes.admin.confirm-modal')
@stop

@section('page-scripts')
    <script src="{{asset('plugins/datatables/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('plugins/datatables-bs4/js/dataTables.bootstrap4.min.js')}}"></script>
    <script src="{{asset('plugins/datatables-responsive/js/dataTables.responsive.min.js')}}"></script>
    <script src="{{asset('plugins/datatables-responsive/js/responsive.bootstrap4.min.js')}}"></script>
    <script src="{{asset('plugins/datatables-buttons/js/dataTables.buttons.min.js')}}"></script>
    <script src="{{asset('plugins/datatables-buttons/js/buttons.bootstrap4.min.js')}}"></script>
    <script src="{{asset('plugins/jszip/jszip.min.js')}}"></script>
    <script src="{{asset('plugins/pdfmake/pdfmake.min.js')}}"></script>
    <script src="{{asset('plugins/pdfmake/vfs_fonts.js')}}"></script>
    <script src="{{asset('plugins/datatables-buttons/js/buttons.html5.min.js')}}"></script>
    <script src="{{asset('plugins/datatables-buttons/js/buttons.print.min.js')}}"></script>
    <script src="{{asset('plugins/datatables-buttons/js/buttons.colVis.min.js')}}"></script>

    <script>
        $(function () {
            $("#example1").DataTable({
                "responsive": true, "lengthChange": false, "autoWidth": false,
                "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
            }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
        });

        //Deposit delete logic
        var deleteModal = $('#modal-confirm');
        var submitButton = deleteModal.find('#action');
        var depositDeleteForm = $('#depositDeleteForm');

        deleteModal.on('show.bs.modal', function (event) {
            deleteModal.find('.modal-title').text('Confirm');
            deleteModal.find('#model').text('deposit');
            var btn = $(event.relatedTarget);
            var ref = btn.data('ref');

            depositDeleteForm.attr('action', `/admin/deposits/${ref}/delete`);
        });

        submitButton.on('click', function (evt) {
            var button = $(this);
            button.attr('href', '/admin/customers')

            evt.preventDefault();
            depositDeleteForm.submit();
        })
    </script>
@endsection
