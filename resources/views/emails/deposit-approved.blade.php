@extends('layouts.email-master')
@section('title', 'Investment deposit approved.')

@section('content')
    <strong>Hello {{ $deposit->customer->name }},</strong> <br>

    <p>Your investment deposit of $ {{ $deposit->amount }}.00 initiated
        on {{ $deposit->created_at->toFormattedDateString() }} has been approved.</p>

    <div style="display: flex; justify-content: center">
        <a href="{{ route('login') }}" target="_blank" class="btn btn-primary">
            Login
        </a>
    </div>
@endsection
