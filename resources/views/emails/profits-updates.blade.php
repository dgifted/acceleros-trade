@extends('layouts.email-master')
@section('title', 'Deposits profit update.')

@section('content')
    <strong>Hello {{ $user->name }},</strong> <br>

    <p>Your investment profits as of today {{ today()->toFormattedDateString() }} is ${{ $profits }}</p>

    <p>Thank you for choosing our platform.</p>
@endsection
