@extends('layouts.email-master')
@section('title', 'Investment deposit approved.')

@section('content')
    <strong>Hello {{ $referee->name }},</strong> <br>

    <p>{{ $referred->name }} has signed up with your referral link.</p>
    <p>You are entitle to {{ \App\Models\Referral::REFERRAL_COMMISSION_PERCENTAGE }}% commission of your referred users
        deposit which will be credited to your account on his/her first deposit.</p>
@endsection
