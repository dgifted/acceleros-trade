@extends('layouts.email-master')
@section('title', $subject)

@section('content')
    <h2>{{ $subject }}</h2>
    <strong>Hello {{$recipient->name}},</strong> <br>
    {!! $content !!}
@endsection
