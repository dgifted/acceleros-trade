@extends('layouts.email-master')
@section('title', 'Withdrawal request.')

@section('content')
    <strong>Hello {{ $user->name }},</strong> <br>

    <p>You have request for withdrawal of ${{ $withdrawal->amount }} from your investments
        on {{ $withdrawal->created_at->toFormattedDateString() }}.</p>
    <p>You will received the request amount into your
        {{ $withdrawal->userWallet->paymentChannel->name }} - {{ $withdrawal->userWallet->address }}
        account shortly.</p>
@endsection
