@extends('layouts.email-master')
@section('title',
    'New investment
    deposit.')

@section('content')
    <strong>Hello
        {{ $user->name }},</strong>
    <br>

    <p>You just
        opened an
        investment
        with a
        deposit of $
        {{ $deposit->amount }}.00
        on
        {{ $deposit->created_at->toFormattedDateString() }}.
    </p>
    <p>Please send
        @if (!is_null($deposit->currency_equivalent))
            {{ $deposit->currency_equivalent }}
            {{ $deposit->currency->abbreviation ?? '' }}
        @else
            $<span>{{ $deposit->amount }}</span>.00
        @endif
        @if (!is_null($deposit->channel->abbreviation))
            ({{ $deposit->channel->abbreviation ?? '' }})
        @endif
        to
        {{ $deposit->channel->wallet }}
        as soon as
        possible.
    </p>

    <div
        style="display: flex; justify-content: center">
        <a href="{{ route('login') }}"
           target="_blank"
           class="btn btn-primary">
            Login
        </a>
    </div>
@endsection
