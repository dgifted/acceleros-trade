@extends('layouts.email-master')
@section('title', 'New bonus')

@section('content')
    <strong>Hello {{ $bonus->customer->name }},</strong> <br>

    <p>The sum of ${{ $bonus->amount }} bonus has been given to you for {{ $bonus->description }}</p>

    @if($bonus->expire_days > 0)
        <p>This bonus is valid till {{ $bonus->created_at->addDays($bonus->expire_days)->toFormattedDateString() }}</p>
    @endif
@endsection
