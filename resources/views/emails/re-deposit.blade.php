@extends('layouts.email-master')
@section('title',
    'New investment
    deposit.')

@section('content')
    <strong>Hello
        {{ $user->name }},</strong>
    <br>

    <p>You have
        reinvested the sum of ${{ $deposit->amount }}.00 into your investment portfolio
        at {{ $deposit->created_at->toFormattedDateString() }}.
    </p>

    <div
        style="display: flex; justify-content: center">
        <a href="{{ route('login') }}"
           target="_blank"
           class="btn btn-primary">
            Login
        </a>
    </div>
@endsection
