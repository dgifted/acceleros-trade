@extends('layouts.email-master')
@section('title', 'Welcome! Account created.')

@section('content')
    <strong>Welcome {{$user->name}},</strong> <br>
    <p>Thank you for signing up with {{ config('app.name') }}! We hope you achieve your financial goals with us. Check
        your account and update your profile.</p>


    <p>Follow the link below to activate your account.</p>
    <div style="display: flex; justify-content: center">
        <a href="{{ route('verify.email', $user->verification_token) }}" target="_blank" class="btn btn-primary">
            Verify account
        </a>
    </div>
@endsection
