@extends('layouts.email-master')
@section('title', 'Verification, link.')

@section('content')
    <strong>Hello {{$user->name}},</strong> <br>

    <p>Verify your account by clicking on the link below.</p>
    <div style="display: flex; justify-content: center">
        <a href="{{ route('verify.email', $user->verification_token) }}"></a>
    </div>
@endsection
