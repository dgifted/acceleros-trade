@extends('errors::minimal')

@section('title', __('Not Found'))
@section('code', '404')
@section('message')
    <p>UH OH! You're lost.<br>
        The page you are looking for does not exist. How you got here is a mystery. But you can click the button below
        to go back to the homepage.</p>
@stop
@section('extra-content')
    <div style="display: flex; justify-content: space-around">
        <div class="acc_button">
            <a href="{{ route('home') }}">Go Home</a>
        </div>
    </div>
@endsection
