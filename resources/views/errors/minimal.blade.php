<!DOCTYPE html
    PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
    <title>{{ config('app.name') }} | @yield('title')</title>
    <link rel="icon" href="{{asset('assets/images/favicon.png')}}">
    <link rel="stylesheet" href="{{url('https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css')}}">
    <link href="{{asset('assets/animate.css')}}" rel="stylesheet" type="text/css">
    <link href="{{url('https://fonts.googleapis.com/css?family=Muli:300,400,600,700,800&amp;display=swap')}}"
          rel="stylesheet">
    <link href="{{asset('assets/_custom.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('assets/hover.css')}}" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="{{asset('plugins/fontawesome-free/css/all.min.css')}}">
    <script src="{{url('https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js')}}"
            type="text/javascript"></script>
    <script src="{{url('https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js')}}"
            type="text/javascript"></script>
    <script src="{{asset('assets/setting2.js')}}" type="text/javascript"></script>

    <script src="{{asset('assets/wow.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/wow.min.js')}}" type="text/javascript"></script>
    {{--    @include('includes.general.chat-widget')--}}
    <style type="text/css">
        .content > .statwrap {
            margin: 0 !important;
        }

        p.error {
            font-size: 1.2rem;
            color: red;
        }

        .wrapper {
            min-height: 100vh;
            position: relative;
            overflow-y: hidden;
        }

        .headerwrap {
            min-height: auto;
        }

        .footerwrap {
            position: absolute;
            bottom: 0;
            left: 0;
        }

        .loginpage {
            height: 100vh;
            display: flex;
            flex-direction: column;
            align-items: center;
            justify-content: center;
        }

        .loginwrappers {
            display: flex;
            flex-direction: column;
            align-items: center;
            justify-content: space-between;
        }

        .loginwrappers h3 {
            font-size: 5rem;
            color: white;
        }

        .acc_button a {
            background: #42bf8c;
            color: #fff;
            height: 45px;
            line-height: 45px;
            border-radius: 40px;
            display: block;
            font-size: 14px;
            font-weight: 600;
            text-transform: uppercase;
            margin: 10px auto 20px;
            min-width: 100px;
            text-align: center;
        }
    </style>
    @yield('page-styles')
</head>
<body id="main">
<!-- Content -->
<div class="wrapper">
    <div class="loginpage">
        <div class="content">
            <div class="loginwrappers">
                <h3>@yield('code')</h3>
                @yield('message')
                @yield('extra-content')
            </div>
        </div>
    </div>

    {{--    @include('includes.landing.footer')--}}
</div>

@yield('page-scripts')
@include('includes.general.chat-widget')
</body>
</html>
