@extends('errors::minimal')

@section('title', __('Unauthorized'))
@section('code', '401')
@section('message')
    <p>Sorry!<br>
    You need to login or register to access this page.</p>
@stop
@section('extra-content')
    <div style="display: flex; justify-content: space-around">
        <div class="acc_button">
            <a href="{{ route('login') }}">Login</a> OR <a href="{{ route('register') }}">Register</a>
        </div>
    </div>
@endsection
