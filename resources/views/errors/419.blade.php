@extends('errors::minimal')

@section('title', __('Page Expired'))
@section('code', '419')
@section('message')
    <p>Sorry!<br>
        The page you're trying to access requires a session that has expired. Please login again.</p>
@stop
@section('extra-content')
    <div style="display: flex; justify-content: space-around">
        <div class="acc_button">
            <a href="#" id="refreshButton">Login again</a>
        </div>
    </div>
@stop

@section('page-scripts')
    <script>
        var btn = document.getElementById('refreshButton');
        btn.addEventListener('click', evt => {
            evt.preventDefault();

            window.location.href = '/login';
        })
    </script>
@endsection
