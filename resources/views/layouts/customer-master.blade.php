<!DOCTYPE html
    PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
    <title>{{ config('app.name') }} | @yield('page-title')</title>
    <link rel="icon" href="{{asset('assets/images/favicon.png')}}">
    <link rel="stylesheet" href="{{url('https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css')}}">
    <link href="{{asset('assets/animate.css')}}" rel="stylesheet" type="text/css">
    <link href="{{url('https://fonts.googleapis.com/css?family=Muli:300,400,600,700,800&amp;display=swap')}}"
          rel="stylesheet">
    <link href="{{asset('assets/_custom.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('assets/hover.css')}}" rel="stylesheet" type="text/css">
    <link href="{{ asset('assets/sweetalert2.min.css') }}" rel="stylesheet"/>

    <script src="{{url('https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js')}}" type="text/javascript"></script>
    <script src="{{url('https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js')}}" type="text/javascript"></script>
    <script src="{{url('https://cdn.jsdelivr.net/npm/clipboard@2.0.10/dist/clipboard.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/setting2.js')}}" type="text/javascript"></script>

    <script src="{{asset('assets/wow.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/wow.min.js')}}" type="text/javascript"></script>

    <style type="text/css">
        .content > .statwrap {
            margin: 0 !important;
        }

        p.error {
            font-size: 1.2rem;
            color: red;
        }

        .plan-item {
            display: flex;
            flex-direction: column;
            align-items: flex-start;
            /*border-bottom: 2px solid #fbbc14;*/
        }

        .plan-item h2 {
            font-size: 2rem;
            color: white;
        }

        .plan-item button {
            border: none;
            color: #fbbc14 !important;
            background: transparent !important;
            text-transform: none;
            width: auto;
            padding: 0;
            margin-top: 10px;
        }

        .plan-table {
            width: 100%;
        }

        .plan-table thead {
            background-color: #268d94;
        }

        .plan-table thead th {
            border: none;
            color: #ffffff;
        }

        .plan-table tbody td {
            border-bottom: 1px solid #268c94;
        }
    </style>
    @yield('page-styles')
</head>
<body>
<!-- Content -->
<div class="wrapper">
    @include('includes.customer.header')
    <div class="inside_wrap accountmainpage">
        <div class="content">
            @include('includes.customer.sidebar')
            @yield('content')
        </div>
    </div>
    @include('includes.landing.footer')
</div>

<!-- Page Scripts -->

@include('includes.general.chat-widget')

<script type="text/javascript">
    var rawDisplayedNumbers = $('.num-format');

    rawDisplayedNumbers.each(function () {
        var elem = $(this)
        var numberValue = elem.text();
        var formattedNumber = numberFormatter(+numberValue);
        elem.text(formattedNumber);
    })

    function numberFormatter(num) {
        const numberFormatter = Intl.NumberFormat('en-US');
        return numberFormatter.format(num);
    }
</script>
@yield('page-scripts')
</body>
</html>
