var editable = false;

$('#btnEdit').on('click', function () {
    editable = !editable;

    editable === true
        ? $(this).html('Cancel<i class="fas fa-user-times ml-2"></i>')
        : $(this).html('Edit<i class="fas fa-user-edit ml-2"></i>');
    // : ;

    $('.form-control').each(function () {
        editable == true
            ? $(this).prop('disabled', false)
            : $(this).prop('disabled', true);
    });

    editable === true
        ? $('#submit').prop('disabled', false)
        : $('#submit').prop('disabled', true);

    editable === true
        ? $('#changePassword').prop('disabled', false)
        : $('#changePassword').prop('disabled', true);
});

//AJAX Setup
// $.ajaxSetup({
//     headers: {
//         'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
//     }
// });

$('#inputGroupFile01').on('change', function (event) {
    var obj = $(this)[0].files;
    var filePath = obj.length > 1 ? obj.files[obj.length - 1] : obj[0];

    $('#imgElem').attr('src', URL.createObjectURL(filePath));

    $('#photoPreview')
        .removeClass('d-none')
        .addClass('d-block');
});

$('#upload').on('click', function (event) {
    event.preventDefault();
    event.stopPropagation();

    // Extract form data
    var formData = new FormData();
    var files = $('#inputGroupFile01')[0].files;
    var file_path = files.length > 1 ? files[files.length - 1] : files[0];

    // Check if files selected
    if (files.length == 0) {
        showToast('warning', 'You have not selected any files.');
        return;
    }

    if (files[0]['type'] === 'image/png' || files[0]['type'] === 'image/jpeg') {

        formData.append('file', file_path);
        // console.log('FORM DATA: ', formData);

        $.ajax({
            url: '/admin/profile/upload-avatar',
            type: 'POST',
            processData: false,
            contentType: false,
            data: formData,
            success: function (resp, _, __) {
                console.log('Upload successful: ', resp);
                if (resp.success) {
                    showToast(resp.alert, resp.message);
                    $('#picture-modal').modal('hide');
                    location.reload(true);
                } else {
                    showToast(resp.alert, resp.message);
                }
            },
            error: function (_, __, errorMessage) {
                console.log('Upload failed: ', errorMessage);
                showToast('error', errorMessage);
            }
        });

        return;
    }

    showToast('error', 'File type not supported');
});

$('#update').on('click', function (event) {
    event.preventDefault();

    var formData = new FormData();

    //Validate the form
    $('form#form-profile input[type=password]').each(function (index) {
        // console.log('Input ' + index + ': ' + $(this).val());
        if ($(this).val().length < 10) {
            showToast('error', 'The passwords must be up to 8 character.');
            return;
        }

        if ($(this).val() === '' || $(this).val() === null) {
            showToast('error', 'Passwords cannot be empty');
            return;
        }
    });

    if ($('#new_password').val()) {
        formData.append('password', $('#new_password').val());
    }
    if ($('#confirm_password').val()) {
        formData.append('password_confirmation', $('#confirm_password').val());
    }

    //Make an ajax call
    $.ajax({
        url: '/admin/profile/update-password',
        type: 'POST',
        data: formData,
        processData: false,
        contentType: false,
        success: function (resp, status, xhr) {
            if (resp.success) {
                showToast(resp.alert, resp.message);
                $('#password-modal').modal('hide');
                setTimeout(function (args) {
                    location.reload(true);
                }, 3000);
            }
            showToast(resp.alert, resp.message);
        },
        error: function (_, __, errorMessage) {
            showToast('error', errorMessage);
        }
    });
})



