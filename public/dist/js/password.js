$('#autoPassword').on('click', function(){
    $('#password').val(generateRandomPassword());
});

function generateRandomPassword() {
    var stringSet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTVWXYZ1234567890!@#$^*';
    var outPutString = ''
    var passwordLenght = 12;
    var counter;
    for(counter = 0; counter < passwordLenght; counter++) {
        var index = Math.floor(Math.random() * Math.floor(stringSet.length + 1));
        outPutString += stringSet.charAt(index);
    }

    return outPutString;
}
