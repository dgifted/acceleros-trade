//AJAX Setup
$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

$('#submit').on('click', function (event) {
    event.preventDefault();

    //Chech if user has provided wallet.
    var userWallet = $('#user-wallet').val();
    if (!userWallet) {
        showToast('warning', 'Please enter your wallet address before proceeding with withdrawal');
        return;
    }

    var amount = $('#amount').val();
    var profits = $('#profits').val();

    $(this).prop('disabled', true);
    $(this).append('&nbsp;<img src="/images/loader.gif" width="24" height="24" class="loader-img">');

    // Make an ajax call
    $.ajax({
        url: '/investor/withdraw',
        type: 'POST',
        data: {
            'amount': +amount,
            'profits': +profits,
        },
        success: function (resp, status, xhr) {
            console.log('Request success');
            $('#submit').html('proceed');
            $('#submit').prop('disabled', false);
            if (resp.success) {
                $('#withdrawal').hide();
                $('#withdrawal-modal').show('slow');
                showToast(resp.alert, resp.message);
            } else {
                showToast(resp.alert, resp.message);
            }

        },
        error: function (jqXhr, textStatus, errorMessage) {
            $('#submit').html('proceed');
            $('#submit').prop('disabled', false);
            showToast('error', errorMessage);
        }
    });

    // $('#withdrawal').addClass('d-none');
    // $('#withdrawal-modal').removeClass('d-none');
});

$('#confirm-password').on('click', function (event) {
    event.preventDefault();

    var password = $('#password').val();

    if (password === null || password === '') {
        showToast('error', 'Please enter your password');
        return;
    }

    $.ajax({
        url: '/investor/withdraw-confirm-password',
        type: 'POST',
        data: {
            'password': password,
        },
        success: function (resp, status, xhr) {
            if (resp.success) {
                showToast(resp.alert, resp.message);
                $('#static-modal').hide();
                $('#withdrawal').show();
            } else {
                showToast('error', resp.message);
                $('#password-error').show();
            }
        },
        error: function (_, __, errorMessage) {
            showToast('error', errorMessage);
        }
    });
});

$('#amount').keyup(function () {
    var target = $(this);
    var button = $('#submit');
    var withdrawalLimit = +button.data('withdrawable');

    if (+target.val() > withdrawalLimit || +target.val() <= 0) {
        button.prop('disabled', true);
    }

    var max = +target.data('max');
    var min = +target.data('min');


    if (+target.val() < min) {
        target.val(min)
    }

    if (+target.val() > max) {
        target.val(max)
    }
});
