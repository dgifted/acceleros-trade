// Investment controller

//Global payment option
var selectedOption;
var availableOptions = $("#paymentOptionsHolder").val();
availableOptions = JSON.parse(availableOptions);
var walletAddress = "";

function setSelectedOption(optionId) {
    for (var i = 0; i < availableOptions.length; i++) {
        if (+availableOptions[i].id === +optionId)
            selectedOption = availableOptions[i];
    }
}

function feedPageData() {
    if (selectedOption) {
        walletAddress = selectedOption.wallets[0].address;
    }

    $("#address").val(walletAddress);

    $(".refferal-info").append(`
    <button class="refferal-copy copy-clipboard"
                            id="copy-button"
                            data-clipboard-text="${walletAddress}">
                        <em class="ti ti-files"></em></button>
`);

    $("#qrcode").qrcode({
        minVersion: 1,
        maxVersion: 40,
        ecLevel: "L",
        fill: "#000",
        background: null,
        radius: 0,
        quiet: 0,
        mode: 0,
        size: 100,
        text: walletAddress
    });
}

// Trigger button
$("#submit").on("click", function(event) {
    event.preventDefault();
    $(this).prop("disabled", true);

    //Grab the data
    var amount = $("#amount").val();
    $(this).append(
        '&nbsp;<img src="/images/loader.gif" width="24" height="24" class="loader-img" alt="">'
    );
    var pay_option = $("input[name=pay_option]:checked").val();

    //Prompt the user to select a payment option if not selected
    if (!pay_option) {
        alert("Please select a payment method before proceeding");
        return;
    }

    setSelectedOption(pay_option);
    feedPageData();

    //AJAX Setup
    $.ajaxSetup({
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content")
        }
    });

    //Send an ajax request to the server
    $.ajax({
        url: "/investor/investment-store",
        type: "POST",
        data: {
            amount: amount,
            pay_option: pay_option
        },
        success: function(resp) {
            if (resp.success) {
                $("#cancel-order").attr(
                    "href",
                    "/investor/investor-cancel_investment/" + resp.data.ref_id
                );
                $("#modal_1").hide();
                $("#modal_2").show("slow");
                showToast(resp.alert, resp.message);
            } else {
                showToast(resp.alert, resp.message);
            }
        },
        error: function(jqXhr, textStatus, errorMessage) {
            $("#submit").html("complete process");
            $("#submit").prop("disabled", false);
            showToast("error", errorMessage);
        }
    });
});

$("#cancel-order").on("click", function(event) {
    event.preventDefault();
    var cancelOrderButton = $(this);
    cancelOrderButton.html("CANCELLED");
    cancelOrderButton.addClass("disabled");

    //AJAX Setup
    $.ajaxSetup({
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content")
        }
    });

    var url = $(this).attr("href");

    $.ajax({
        url: url,
        type: "GET",
        success: function(resp, status, xhr) {
            if (resp.success) {
                showToast(resp.alert, resp.message);
            }
            if (!resp.success) {
                showToast(resp.alert, resp.message);
            }
        },
        error: function(jqXhr, textStatus, errorMessage) {
            showToast("Error", errorMessage);
            cancelOrderButton.html("CANCEL");
            cancelOrderButton.removeClass("disabled");
        }
    });
});
