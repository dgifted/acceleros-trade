
$('#resend-email').on('click', function(event) {
    event.preventDefault();

    //AJAX Setup
    $.ajaxSetup({
        headers : {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $.ajax({
        url: '/investor/resend-verification',
        type: 'GET',
        success: function(resp, status, xhr) {
            //Hide the alert
            $('#confirm-email').hide();
            //Notify the user
            showToast(resp.alert, resp.message);
        },
        error: function(_, _, errorMessage) {
            showToast('error', errorMessage);
        }
    });

});
