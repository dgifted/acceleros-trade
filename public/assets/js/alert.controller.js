function showToast(type, message) {
    if(typeof type !== 'string') {
        type = 'info';
    }

    var _type = type.charAt(0).toUpperCase() + type.slice(1);

    var toastOptions = {
        'toastClass':'toast',
        'showMethod':'fadeIn',
        "hideMethod": "fadeOut",
        "hideEasing": "linear",
        'hideDuration': 1000,
        'iconClass':'toast-' + type,
        'showDuration': 300,
        'positionClass':'toast-top-right',
        'timeOut': 3000,
        'titleClass':'toast-title',
        'messageClass':'toast-message',
        'progressBar':true
    }

    switch(type) {
        case 'success':
            toastr.success(message, _type, toastOptions);
            break;
        case 'warning':
            toastr.warning(message, _type, toastOptions);
            break;
        case 'info':
            toastr.info(message, _type, toastOptions);
            break;
        default:
            toastr.error(message, _type, toastOptions);
            break;
    }

}
