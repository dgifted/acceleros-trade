//AJAX Setup
$.ajaxSetup({
    headers : {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

$('#withdraw').on('click', function(event) {
    event.preventDefault();
    var amount = $(this).data('amount');
    $(this).prop('disabled', true);
    $(this).append('&nbsp;<img src="/images/loader.gif" width="24" height="24" class="loader-img">');

    $.ajax({
        url: '/investor/withdraw_referral-bonus',
        type: 'POST',
        data: {
            'amount': amount,
        },
        success: function(resp, status, xhr) {
            $('#withdraw').html('Withdraw Bonus');
            $('#withdraw').prop('disabled', false);

            $('.bonus-amount').each(function() {
                $(this).html('$ 0');
            });

            if (resp.success) {
                $('#withdraw').hide();
                showToast(resp.alert, resp.message);
            } else {
                showToast(resp.alert, resp.message);
            }
        },
        error: function(_, __, errorMessage) {
            $('#withdraw').html('Withdraw Bonus');
            $('#withdraw').prop('disabled', false);

            showToast('error', errorMessage);
        },
    });
});
