//AJAX Setup
$.ajaxSetup({
    headers : {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
})

$('#activitylog').on('click', function(event) {
    event.preventDefault();

    //Make ajax call
    $.ajax({
        url: '/investor/save_activity_log',
        type: 'GET',
        success: function(resp, status, xhr) {
            var state = resp.state == 'on' ? true : false;

            console.log('Response: ', resp);
            $('#activitylog').prop('checked', state);
            showToast(resp.alert, resp.message);
        },
        error: function(jqXhr, textStatus, errorMessage) {
            showToast('error', errorMessage);
        }
    });
});

$('#passchange').on('click', function(event) {
    event.preventDefault();

    //Make ajax call
    $.ajax({
        url: '/investor/email_before_password_change',
        type: 'GET',
        success: function(resp, status, xhr) {
            var state = resp.state == 'on' ? true : false;

            console.log('Response: ', resp);
            $('#passchange').prop('checked', state);
            showToast(resp.alert, resp.message);
        },
        error: function(jqXhr, textStatus, errorMessage) {
            showToast('error', errorMessage);
        }
    });
});

$('#tokwith').on('click', function(event) {
    event.preventDefault();

    //Make ajax call
    $.ajax({
        url: '/investor/password_before_profit_withdraw',
        type: 'GET',
        success: function(resp, status, xhr) {
            var state = resp.state == 'on' ? true : false;

            console.log('Response: ', resp);
            $('#tokwith').prop('checked', state);
            showToast(resp.alert, resp.message);
        },
        error: function(jqXhr, textStatus, errorMessage) {
            showToast('error', errorMessage);
        }
    });
});


$('#toggleVisibility').on('click', function(event) {


    $(this).text(function(index) {
        if ($(this).text() === 'Make fields visible') {
            console.log('TRUE');
            $('form#passwords input[type=password]').each(function() {
                $(this).attr('type', 'text');
            });
            return 'Hide fields';
        } else {
            console.log('FALSE');
            $('form#passwords input[type=password]').each(function() {
                $(this).attr('type', 'password');
            });
            return 'Make fields visible';
        }
    });

    event.preventDefault();
});

// $('#toggleVisibility').on('click', function(event) {
//     event.preventDefault();

//     console.log(hidden);

//     label = hidden == true ? 'Hide fields' : 'Make fields visible';
//     $(this).html(label);

//     $('form#passwords input[type=password]').each(function() {
//         hidden == true
//             ? $(this).prop('type', 'text')
//             : $(this).prop('type', 'password');
//     });

//     hidden = !hidden;
// });
